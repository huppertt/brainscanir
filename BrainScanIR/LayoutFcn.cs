﻿using System;
using Gtk;

namespace BrainScanIR
{
    public partial class MainWindow : Gtk.Window
    {

        //*********************************************************
        void setGUIhandles()
        {
            handles.windowdata_checkbox = this.check_windowdata;
            handles.windatatime = this.windatatime_edit;
            handles.showstim = this.showstim_checkbox;
            handles.statusbardevice = this.statusbar1;
            handles.showlegend = this.showlegend_checkbox;
            handles.SDGwin = this.drawingareaSDG;
            handles.rtpanel = this.crealtimepanel1;
            handles.laserpanels = new CLaserPanel[16];
            handles.laserlabels = new Label[16];
            handles.laserframes = new Alignment[16];
            handles.combobox_wh = this.combobox_whichdisplay;
            handles.scantypes = this.comboboxscantypes;
            handles.stimtypes = this.combobox_whichstim;
            handles.texteventlistener = this.textview_eventlistener;

            handles.button_LasersOnOff=this.button_LasersOnOff;
            handles.button_LasersOnOff2=this.button_LasersOnOff2;
            handles.buttonlaser_autoadjust = buttonlaser_autoadjust;
            handles.imagelaserssafety = this.imagelaserssafety;
            handles.checkbuttonlinksrcs = this.checkbuttonlinksrcs;

            handles.checkbuttonlinksrcs = this.checkbuttonlinksrcs;
            handles.checkbutton_useeventlistener = this.checkbutton_useeventlistener;
            handles.expander1 = this.expander1;
            handles.buttonautoadjustgain = this.buttonautoadjustgain;
            handles.ShowDataQuality = this.ShowDataQuality;
            handles.expander_controls = this.expander_controls;
            handles.togglebutton_markstim = this.togglebutton_markstim;
            handles.button_markstim = this.button_markstim;
            handles.startstop_button = this.startstop_button;
            handles.statswinview = this.statswindow;

			handles.laserpanels[0] = this.claserpanelA;
            handles.laserpanels[1] = this.claserpanelB;
            handles.laserpanels[2] = this.claserpanelC;
            handles.laserpanels[3] = this.claserpanelD;
            handles.laserpanels[4] = this.claserpanelE;
            handles.laserpanels[5] = this.claserpanelF;
            handles.laserpanels[6] = this.claserpanelG;
            handles.laserpanels[7] = this.claserpanelH;
            handles.laserpanels[8] = this.claserpanelA1;
            handles.laserpanels[9] = this.claserpanelB1;
            handles.laserpanels[10] = this.claserpanelC1;
            handles.laserpanels[11] = this.claserpanelD1;
            handles.laserpanels[12] = this.claserpanelE1;
            handles.laserpanels[13] = this.claserpanelF1;
            handles.laserpanels[14] = this.claserpanelG1;
            handles.laserpanels[15] = this.claserpanelH1;

            handles.laserframes[0] = this.SourceA;
            handles.laserframes[1] = this.SourceB;
            handles.laserframes[2] = this.SourceC;
            handles.laserframes[3] = this.SourceD;
            handles.laserframes[4] = this.SourceE;
            handles.laserframes[5] = this.SourceF;
            handles.laserframes[6] = this.SourceG;
            handles.laserframes[7] = this.SourceH;
            handles.laserframes[8] = this.SourceA1;
            handles.laserframes[9] = this.SourceB1;
            handles.laserframes[10] = this.SourceC1;
            handles.laserframes[11] = this.SourceD1;
            handles.laserframes[12] = this.SourceE1;
            handles.laserframes[13] = this.SourceF1;
            handles.laserframes[14] = this.SourceG1;
            handles.laserframes[15] = this.SourceH1;

            handles.laserlabels[0] = this.SourceLabelA;
            handles.laserlabels[1] = this.SourceLabelB;
            handles.laserlabels[2] = this.SourceLabelC;
            handles.laserlabels[3] = this.SourceLabelD;
            handles.laserlabels[4] = this.SourceLabelE;
            handles.laserlabels[5] = this.SourceLabelF;
            handles.laserlabels[6] = this.SourceLabelG;
            handles.laserlabels[7] = this.SourceLabelH;
            handles.laserlabels[8] = this.SourceLabelA1;
            handles.laserlabels[9] = this.SourceLabelB1;
            handles.laserlabels[10] = this.SourceLabelC1;
            handles.laserlabels[11] = this.SourceLabelD1;
            handles.laserlabels[12] = this.SourceLabelE1;
            handles.laserlabels[13] = this.SourceLabelF1;
            handles.laserlabels[14] = this.SourceLabelG1;
            handles.laserlabels[15] = this.SourceLabelH1;

            handles.headrefgmenu = this.ProbeRegistrationAction;

            ActivateGUI(new Cprobe(), false);
        }
        //*********************************************************

        public void SetLaserLayout(int numlasers, int numlambda)
        {
            handles.laserbuttons = new Button[numlasers];
            handles.laserspinners = new SpinButton[numlasers];
            handles.laserlabels2 = new Label[numlasers];


            int cnt = 0;
            for (int i = 0; i < 16; i++)
            {
                if (i >= numlasers / numlambda)
                {
                    handles.laserpanels[i].Destroy();
                    handles.laserlabels[i].Destroy();
                    handles.laserframes[i].Destroy();

                }
                else
                {
                    handles.laserpanels[i].Sensitive = false;
                    handles.laserlabels[i].Sensitive = false;
                    handles.laserframes[i].Sensitive = false;
                    for (int j = 0; j < numlambda; j++)
                    {

                        handles.laserpanels[i].laserlabels[j].Text = MainClass.SystemControler.SystemInfo.wavelengths[j].ToString() + "nm";
                        handles.laserpanels[i].laserspinner[j].Value = 0;
                        handles.laserpanels[i].laserspinner[j].SetRange(0, MainClass.SystemControler.SystemInfo.maxlaserpwr);
                        handles.laserpanels[i].laserbutton[j].Label = "Laser-" + (cnt + 1).ToString();

                        handles.laserlabels2[cnt] = handles.laserpanels[i].laserlabels[j];
                        handles.laserbuttons[cnt] = handles.laserpanels[i].laserbutton[j];
                        handles.laserspinners[cnt] = handles.laserpanels[i].laserspinner[j];

                        handles.laserpanels[i].linklasers = true;
                        handles.laserpanels[i].laserindices[j] = cnt;
                        handles.laserpanels[i].laserinuse[j] = true;
                        handles.laserpanels[i].setlaser(j, false);

                        cnt++;
                        if (MainClass.SystemControler.SystemInfo.laser_adjustable)
                        {
                            handles.laserpanels[i].laserspinner[j].SetRange(0, MainClass.SystemControler.SystemInfo.maxlaserpwr);
                        }
                        else
                        {
                            handles.laserpanels[i].laserspinner[j].Destroy();
                        }
                    }
                    for (int j = numlambda; j < 4; j++)
                    {
                        handles.laserpanels[i].laserlabels[j].Destroy();
                        handles.laserpanels[i].laserspinner[j].Destroy();
                        handles.laserpanels[i].laserbutton[j].Destroy();
                        handles.laserpanels[i].laserinuse[j] = false;
                    }
                }

            }


			var buffer = System.IO.File.ReadAllBytes(@"surf/laser-hazard-safety.png");
			var pixbuf = new Gdk.Pixbuf(buffer);

			int w, h;
			h = pixbuf.Height;
			w = pixbuf.Width;
            this.imagelaserssafety.Pixbuf = pixbuf;

            // TODO- this probably doesn't work for >2 wavelengths
            double numsrc = numlasers / numlambda;
            for (int i = (int)(Math.Ceiling(numsrc / 8)); i < this.notebook3.NPages; i++)
            {
                this.notebook3.RemovePage(i);
            }


            handles.combobox_wh.Clear();

            CellRendererText cell = new CellRendererText();
            handles.combobox_wh.PackStart(cell, false);
            handles.combobox_wh.AddAttribute(cell, "text", 0);
            ListStore store = new ListStore(typeof(string));
            handles.combobox_wh.Model = store;

            for (int i = 0; i < MainClass.SystemControler.SystemInfo.numwavelengths; i++)
            {
                handles.combobox_wh.AppendText(String.Format("Raw {0}nm", MainClass.SystemControler.SystemInfo.wavelengths[i]));
            }
            for (int i = 0; i < MainClass.SystemControler.SystemInfo.numwavelengths; i++)
            {
                handles.combobox_wh.AppendText(String.Format("dOD {0}nm", MainClass.SystemControler.SystemInfo.wavelengths[i]));
            }

            if (handles.rtpanel.useMBLL())
            {
                handles.combobox_wh.AppendText("Oxy-Hemoglobin");
                handles.combobox_wh.AppendText("Deoxy-Hemoglobin");
            }
            handles.combobox_wh.Active = 0;
        }
        //*********************************************************
        public void SetDetectorLayout(int numdetectors)
        {

            // This function destroys any unused detector controls
            int numdetpanels = 4;
            handles.detpanels = new CDetectorsTab[numdetpanels];

            handles.detpanels[0] = this.cdetectorstab1;
            handles.detpanels[1] = this.cdetectorstab2;
            handles.detpanels[2] = this.cdetectorstab3;
            handles.detpanels[3] = this.cdetectorstab4;

            Label[] dl = new Label[numdetpanels];
            dl[0] = this.detpanellabel1;
            dl[1] = this.detpanellabel2;
            dl[2] = this.detpanellabel3;
            dl[3] = this.detpanellabel4;

            handles.detimages = new ColorButton[numdetpanels * 8];
            handles.detlabels = new Label[numdetpanels * 8];
            handles.detscales = new VScale[numdetpanels * 8];

            int cnt = 0;
            for (int i = 0; i < numdetpanels; i++)
            {
                for (int j = 0; j < handles.detpanels[i].dimage.Length; j++)
                {
                    handles.detimages[cnt] = handles.detpanels[i].dimage[j];
                    handles.detlabels[cnt] = handles.detpanels[i].dlabels[j];
                    handles.detscales[cnt] = handles.detpanels[i].dscales[j];
                    handles.detscales[cnt].Value = 0;
                    handles.detscales[cnt].Inverted = true;
                    handles.detscales[cnt].SetRange(0, MainClass.SystemControler.SystemInfo.maxdetectgain);
                    handles.detlabels[cnt].Text = String.Format("Det-{0}", cnt + 1);
                    handles.detimages[cnt].Sensitive = false;
                   cnt++;
                }
                string tabl = String.Format("Detectors {0}-{1}", 8 * (i) + 1, 8 * (i + 1));
                dl[i].Text = tabl;

            }
            for (int i = numdetectors; i < numdetpanels * 8; i++)
            {
                handles.detimages[i].Sensitive = false;
                handles.detscales[i].Sensitive = false;
                handles.detlabels[i].Sensitive = false;
            }

        }

        //*********************************************************
        public void SetDataFileLayout()
        {

            this.notebook4.Page = 0; // set main display to time-series plot

            // Set up the treeview to allow edits of collected data files
            this.treeviewfiles.HeadersVisible = true;
            this.treeviewfiles.EnableGridLines = TreeViewGridLines.Both;


            TreeViewColumn col = new TreeViewColumn();
            CellRendererText colt = new CellRendererText();
            col.Title = "Scan Number";
            col.PackStart(colt, true);
            col.AddAttribute(colt, "text", 0);
            this.treeviewfiles.AppendColumn(col);

            col = new TreeViewColumn();
            CellRendererToggle colb = new CellRendererToggle();
            col.Title = "Include";
            col.PackStart(colb, true);
            colb.Activatable = true;
            colb.Active = true;
            col.AddAttribute(colb, "include", 1);
            this.treeviewfiles.AppendColumn(col);


            col = new TreeViewColumn();
            CellRendererCombo colc = new CellRendererCombo();
            col.Title = "Type";
            col.PackStart(colc, true);
            colc.TextColumn = 0;
            colc.Model = new ListStore(typeof(string));
            colc.HasEntry = true;
            colc.Editable = true;

            Gtk.ComboBox cbox = new ComboBox();
            cbox.Model = new ListStore(typeof(string));
            for (int j = 0; j < 4; j++)
            {
                cbox.AppendText(String.Format("Option-{0}", j));
            }
            colc.Model = cbox.Model;


            this.treeviewfiles.AppendColumn(col);


            ListStore store2 = new ListStore(typeof(string), typeof(ToggleButton), typeof(ComboBox));
            this.treeviewfiles.Model = store2;

            handles.FileTreeIter = new TreeIter[0];
        }

        public void SetStimTableLayout()
        {
            // Stimulus table
            //this.treeviewStim;
            this.treeviewstim.HeadersVisible = true;
            this.treeviewstim.EnableGridLines = TreeViewGridLines.Both;

            TreeViewColumn[] col2 = new TreeViewColumn[4];
            CellRendererText[] col2t = new CellRendererText[4];
            col2t[0] = new CellRendererText();
            col2[0] = new TreeViewColumn();
            col2t[0].Editable = true;
            col2[0].Title = "Condition";
            col2[0].PackStart(col2t[0], true);
            col2[0].AddAttribute(col2t[0], "text", 0);
            this.treeviewstim.AppendColumn(col2[0]);

            col2t[1] = new CellRendererText();
            col2[1] = new TreeViewColumn();
            col2t[1].Editable = true;
            col2[1].Title = "Onset";
            col2[1].PackStart(col2t[1], true);
            col2[1].AddAttribute(col2t[1], "text", 1);
            col2t[1].Edited += ChangedStimOnset;

            this.treeviewstim.AppendColumn(col2[1]);

            col2t[2] = new CellRendererText();
            col2[2] = new TreeViewColumn();
            col2t[2].Editable = true;
            col2[2].Title = "Duration";
            col2[2].PackStart(col2t[2], true);
            col2[2].AddAttribute(col2t[2], "text", 2);
            col2t[2].Edited += ChangedStimDuration;

            this.treeviewstim.AppendColumn(col2[2]);

            col2t[3] = new CellRendererText();
            col2[3] = new TreeViewColumn();
            col2t[3].Editable = true;
            col2[3].Title = "Amplitude";
            col2[3].PackStart(col2t[3], true);
            col2[3].AddAttribute(col2t[3], "text", 3);
            col2t[3].Edited += ChangedStimAmp;
            this.treeviewstim.AppendColumn(col2[3]);

            handles.StimStore = new ListStore(typeof(string), typeof(string), typeof(string), typeof(string));
            this.treeviewstim.Model = handles.StimStore;
            handles.StimTreeIter = new TreeIter[0];
        }

        public void ActivateGUI(Cprobe tmpprobe, bool flag)
        {
            // This enables/disables all the GUI controls
            handles.windowdata_checkbox.Sensitive = flag;
            handles.windatatime.Sensitive = flag;
            handles.showstim.Sensitive = flag;
            handles.showlegend.Sensitive = flag;
            handles.combobox_wh.Sensitive = flag;
            //handles.FileTreeIter.Sensitive = flag;
            //handles.StimTreeIter.Sensitive = flag;
           // handles.rtpanel.Sensitive = flag;
            //handles.StimStore.Sensitive = flag;

            handles.button_LasersOnOff.Sensitive = flag;
			handles.button_LasersOnOff2.Sensitive = flag;
			handles.buttonlaser_autoadjust.Sensitive = flag;

            handles.checkbuttonlinksrcs.Sensitive = flag;
			handles.checkbutton_useeventlistener.Sensitive = flag;
			handles.expander1.Sensitive = flag;
			handles.buttonautoadjustgain.Sensitive = flag;
			handles.ShowDataQuality.Sensitive = flag;
			handles.expander_controls.Sensitive = flag;
			handles.togglebutton_markstim.Sensitive = flag;
			handles.button_markstim.Sensitive = flag;
			handles.startstop_button.Sensitive = flag;
			handles.scantypes.Sensitive = flag;
			handles.stimtypes.Sensitive = flag;

            handles.imagelaserssafety.Sensitive = false; // use the sensitivity to show whan any laser is on


            if (flag)
            {
				for (int i = 0; i < handles.laserpanels.Length; i++)
					handles.laserpanels[i].Sensitive = flag;
				for (int i = 0; i < handles.laserframes.Length; i++)
					handles.laserframes[i].Sensitive = flag;
				for (int i = 0; i < handles.laserlabels.Length; i++)
					handles.laserlabels[i].Sensitive = flag;
                
                for (int i = 0; i < tmpprobe.numsrc; i++)
                {
                    for (int j = 0; j < tmpprobe.numlambda; j++)
                    {
                        handles.laserpanels[i].laserinuse[j] = flag;
                        handles.laserpanels[i].laserbutton[j].Sensitive = true;
                        handles.laserpanels[i].laserlabels[j].Sensitive = true;
                        handles.laserpanels[i].laserspinner[j].Sensitive = true;
                        handles.laserpanels[i].laserbutton[j].Label = "Laser-" + (tmpprobe.lasermap[i, j] + 1).ToString();
                        handles.laserpanels[i].laserindices[j] = tmpprobe.lasermap[i, j];
                    }
                }
                for (int i = tmpprobe.numsrc; i < handles.laserpanels.Length; i++)
                {
                    handles.laserpanels[i].Sensitive = false;
                    handles.laserpanels[i].Visible = false;
                }

                for (int i = 0; i < tmpprobe.numdet; i++){
                    handles.detscales[i].Sensitive = flag;
                    handles.detlabels[i].Sensitive = flag;
                }
                for (int i = tmpprobe.numdet; i < handles.detscales.Length; i++){
                    handles.detscales[i].Sensitive = false;
                    handles.detlabels[i].Sensitive = false;;
                }

            }
        }
    }
}