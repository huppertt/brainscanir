﻿using System;
using Microsoft.Xna.Framework;
//using OpenTK;
using System.Xml;
using System.Collections.Generic;
using System.IO;
using zlib;

namespace BrainScanIR
{
	public class Gifti
	{
		public VertexPositionNormalColor[] vertices;
		public short[] indices;
		public Vector3[] facenormals;
		public short[,] faces;
		public int nvertices, nindices, nfaces;
		public Vector3 boundingboxMin;
		public Vector3 boundingboxMax;
		public Double[] cdata;

		public Gifti()
		{
			nvertices = -1;
			nindices = -1;
			nfaces = -1;
			indices = new short[0];
			faces = new short[0, 0];
			vertices = new VertexPositionNormalColor[0];
			cdata = new double[0];
		}

		public void ReadGifti(string filename)
		{

			//Load the GIFTI file
			XmlDocument doc = new XmlDocument();
			XmlDocument doc2 = new XmlDocument();
			XmlReaderSettings settings = new XmlReaderSettings();
			settings.XmlResolver = null;
			settings.DtdProcessing = DtdProcessing.Ignore;

			XmlReader doc3 = XmlReader.Create(filename, settings);
			doc.Load(doc3);
			XmlNodeList elemList;
			XmlNodeList elemList2;
			double[] d;
			d = new double[0];
			elemList = doc.GetElementsByTagName("DataArray");
			for (int i = 0; i < elemList.Count; i++)
			{
				string DataType = elemList[i].Attributes["DataType"].Value;
				string Encoding = elemList[i].Attributes["Encoding"].Value;
				string Intent = elemList[i].Attributes["Intent"].Value;
				int nrows = Convert.ToInt32(elemList[i].Attributes["Dim0"].Value);
				int ncols = 0;
				try
				{
					ncols = Convert.ToInt32(elemList[i].Attributes["Dim1"].Value);
				}
				catch
				{
					ncols = 0;
				}

				//	bool flag = System.BitConverter.IsLittleEndian;
				doc2 = new XmlDocument();
				doc2.LoadXml("<root>" + elemList[i].InnerXml + "</root>");
				elemList2 = doc2.GetElementsByTagName("Data");
				string data = elemList2[0].InnerXml;

				if (Encoding.Equals("GZipBase64Binary"))
				{
					byte[] gtmp = Convert.FromBase64String(data);
					byte[] tmp;
					DecompressData(gtmp, out tmp);

					d = new double[tmp.Length / 4];
					for (int j = 0; j < tmp.Length / 4; j++)
					{
						if (DataType.Equals("NIFTI_TYPE_INT32"))
						{
							d[j] = BitConverter.ToInt32(tmp, j * 4);
						}
						else if (DataType.Equals("NIFTI_TYPE_FLOAT32"))
						{
							d[j] = BitConverter.ToSingle(tmp, j * 4);
						}
					}
				}
				boundingboxMin = new Vector3(999f, 999f, 999f);
				boundingboxMax = new Vector3(-999f, -999f, -999f);

				if (Intent.Equals("NIFTI_INTENT_POINTSET"))
				{
					// Set vertices
					vertices = new VertexPositionNormalColor[nrows];
					nvertices = nrows;
					for (int j = 0; j < nrows; j++)
					{
						Vector3 point = new Vector3();
                        point.X = (float)d[j];
						point.Y = (float)d[j+nrows];
                        point.Z = (float)d[j+2*nrows];

						vertices[j] = new VertexPositionNormalColor();
						vertices[j].Position = point;
						vertices[j].Color = OpenTK.Graphics.Color4.LightGray;

                   
                              
						boundingboxMin.X = Math.Min(boundingboxMin.X, point.X);
						boundingboxMin.Y = Math.Min(boundingboxMin.Y, point.Y);
						boundingboxMin.Z = Math.Min(boundingboxMin.Z, point.Z);

						boundingboxMax.X = Math.Max(boundingboxMax.X, point.X);
						boundingboxMax.Y = Math.Max(boundingboxMax.Y, point.Y);
						boundingboxMax.Z = Math.Max(boundingboxMax.Z, point.Z);
					}
				}
				else if (Intent.Equals("NIFTI_INTENT_TRIANGLE"))
				{
					nindices = nrows * ncols;
					nfaces = nrows;
					indices = new short[nrows * ncols];
					faces = new short[nrows, ncols];
					int cnt = 0;
					for (int k = 0; k < ncols; k++)
					{
						for (int j = 0; j < nrows; j++)
						{

							faces[j, k] = (short)d[cnt];
							indices[cnt] = (short)d[cnt];
							cnt++;
						}
					}


				}
				else if (Intent.Equals("NIFTI_INTENT_NONE"))
				{
					cdata = new double[nrows];
					for (int j = 0; j < nrows; j++)
					{
						cdata[j] = (double)d[j];

					}
				}

			}
			// Calculate vertex normals
			facenormals = new Vector3[nfaces];
			List<int>[] list = new List<int>[nvertices];
			Vector3 center = new Vector3();
            center.X = 0f; center.Y = 0f; center.Z = 0f;
			for (int i = 0; i < nvertices; i++)
			{
				center.X += vertices[i].Position.X;
                center.Y += vertices[i].Position.Y;
                center.Z += vertices[i].Position.Z;
				list[i] = new List<int>();
			}
			center.X = center.X / nvertices;
            center.Y = center.Y / nvertices;
            center.Z = center.Z / nvertices;

			for (int i = 0; i < nfaces; i++)
			{

                Vector3 a = new Vector3();
                a.X = vertices[faces[i, 0]].Position.X - vertices[faces[i, 1]].Position.X;
                a.Y = vertices[faces[i, 0]].Position.Y - vertices[faces[i, 1]].Position.Y;
                a.Z = vertices[faces[i, 0]].Position.Z - vertices[faces[i, 1]].Position.Z;

				Vector3 b = new Vector3();
                b.X = vertices[faces[i, 0]].Position.X - vertices[faces[i, 2]].Position.X;
                b.Y = vertices[faces[i, 0]].Position.Y - vertices[faces[i, 2]].Position.Y;
                b.Z = vertices[faces[i, 0]].Position.Z - vertices[faces[i, 2]].Position.Z;

                //facenormals[i] = Vector3.Cross(a, b);
                facenormals[i].X = a.Y * b.Z - a.Z * b.Y;
                facenormals[i].Y = a.Z * b.X - a.X * b.Z;
				facenormals[i].Z = a.X * b.Y - a.Y * b.X;
                //facenormals[i].Normalize();
                float s = (float)Math.Sqrt(facenormals[i].X * facenormals[i].X + facenormals[i].Y * facenormals[i].Y + facenormals[i].Z * facenormals[i].Z);
                facenormals[i].X = facenormals[i].X / s;
                facenormals[i].Y = facenormals[i].Y / s;
                facenormals[i].Z = facenormals[i].Z / s;

				Vector3 c = new Vector3();
                c.X = vertices[faces[i, 0]].Position.X - center.X;
                c.Y = vertices[faces[i, 0]].Position.Y - center.Y;
                c.Z = vertices[faces[i, 0]].Position.Z - center.Z;
                s = (float)Math.Sqrt(c.X * c.X + c.Y * c.Y + c.Z * c.Z);
                c.X = c.X / s;
                c.Y = c.Y / s;
                c.Z = c.Z / s;

                double dot = facenormals[i].X * c.X + facenormals[i].Y * c.Y + facenormals[i].Z * c.Z;
                double angle = Math.Acos(dot);
				if (Math.Abs(angle) < MathHelper.PiOver2)
				{
                    facenormals[i].X = -facenormals[i].X;
                    facenormals[i].Y = -facenormals[i].Y;
                    facenormals[i].Z = -facenormals[i].Z;
				}
				list[faces[i, 0]].Add(i);
				list[faces[i, 1]].Add(i);
				list[faces[i, 2]].Add(i);

			}
			// now convert to vertex norms
			for (int i = 0; i < nvertices; i++)
			{
				vertices[i].Normal = new Vector3();
                vertices[i].Normal.X = 0f; vertices[i].Normal.Y = 0f; vertices[i].Normal.Z = 0f;
                for (int j = 0; j < list[i].Count; j++)
                {
                    vertices[i].Normal.X += facenormals[list[i][j]].X;
                    vertices[i].Normal.Y += facenormals[list[i][j]].Y;
                    vertices[i].Normal.Z += facenormals[list[i][j]].Z;
                }

				vertices[i].Normal.X = vertices[i].Normal.X / list[i].Count;
                vertices[i].Normal.Y = vertices[i].Normal.Y / list[i].Count;
                vertices[i].Normal.Z = vertices[i].Normal.Z / list[i].Count;
			}

		}
		public static void DecompressData(byte[] inData, out byte[] outData)
		{
			using (MemoryStream outMemoryStream = new MemoryStream())
			using (ZOutputStream outZStream = new ZOutputStream(outMemoryStream))
			using (Stream inMemoryStream = new MemoryStream(inData))
			{
				CopyStream(inMemoryStream, outZStream);
				outZStream.finish();
				outData = outMemoryStream.ToArray();
			}
		}

		public static void CopyStream(System.IO.Stream input, System.IO.Stream output)
		{
			byte[] buffer = new byte[2000];
			int len;
			while ((len = input.Read(buffer, 0, 2000)) > 0)
			{
				output.Write(buffer, 0, len);
			}
			output.Flush();
		}


        public void setcolors(OpenTK.Graphics.Color4[] cdata)
		{
			for (int i = 0; i < cdata.Length; i++)
			{
				vertices[i].Color = cdata[i];

			}

		}

		public double[,] set2surface(double[,] pos, int num)
		{
			double[,] posout = new double[num, 3];

			Vector3 com = new Vector3();
            com.X = vertices[0].Position.X;
            com.Y = vertices[0].Position.Y;
            com.Z = vertices[0].Position.Z;

			for (int i = 1; i < nvertices; i++)
			{
				com.X += vertices[i].Position.X;
                com.Y += vertices[i].Position.Y;
                com.Z += vertices[i].Position.Z;
			}
			com.X = com.X / nvertices;
            com.Y = com.Y / nvertices;
            com.Z = com.Z / nvertices;



			for (int i = 0; i < num; i++)
			{
                Vector3 thispos = new Vector3();
                thispos.X = (float)pos[i, 0];
                thispos.Y = (float)pos[i, 1];
                thispos.Z = (float)pos[i, 2];

				Vector3 vec = new Vector3(); 
                vec.X=thispos.X - com.X;
                vec.Y = thispos.Y - com.Y;
                vec.Z = thispos.Z - com.Z;

                float s = (float)Math.Sqrt(vec.X * vec.X + vec.Y * vec.Y + vec.Z * vec.Z);
                vec.X = vec.X / s;
                vec.Y = vec.Y / s;
                vec.Z = vec.Z / s;


				float mindist = 9999;
				Vector3 bestfit = new Vector3();
				for (int j = 0; j < 200; j ++)
				{
					thispos.X = thispos.X + vec.X * .1f;
                    thispos.Y = thispos.Y + vec.Y * .1f;
                    thispos.Z = thispos.Z + vec.Z * .1f;

					for (int k = 0; k < nvertices; k++)
					{
                        float dist = (float)Math.Sqrt((thispos.X - vertices[k].Position.X) * (thispos.X - vertices[k].Position.X) +
                                                      (thispos.Y - vertices[k].Position.Y) * (thispos.Y - vertices[k].Position.Y) + 
                                                      (thispos.Z - vertices[k].Position.Z) * (thispos.Z - vertices[k].Position.Z));
						if (dist < mindist)
						{
							mindist = dist;
							bestfit.X = thispos.X;
							bestfit.Y = thispos.Y;
							bestfit.Z = thispos.Z;

						}
					}

				}

				posout[i, 0] = bestfit.X;
				posout[i, 1] = bestfit.Y;
				posout[i, 2] = bestfit.Z;


			}

			return posout;
		}

	}
}
