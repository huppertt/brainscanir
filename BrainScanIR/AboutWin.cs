﻿using System;

namespace PhotonViewer
{
	public partial class AboutWin : Gtk.Dialog
	{
		public AboutWin ()
		{
			this.Build ();
		}

		protected void ClickOk (object sender, EventArgs e)
		{
			this.Destroy();
		}
	}
}

