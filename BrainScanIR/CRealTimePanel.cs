﻿using System;
using MathNet.Numerics;
using MathNet.Filtering;
using System.Collections;
using Gtk;
using MathNet.Filtering.Kalman;
using LSL;

namespace BrainScanIR
{
    [System.ComponentModel.ToolboxItem(true)]
    public partial class CRealTimePanel : Gtk.Bin
    {
		MathNet.Filtering.OnlineFilter[] OnlineFIRFiltersBPF;
		MathNet.Filtering.Kalman.DiscreteKalmanFilter[] MocoKalman;
		MathNet.Filtering.Kalman.DiscreteKalmanFilter[] GLMKalman;
        LSL.liblsl.StreamOutlet LSLoutlet; //(info);

        public CRealTimePanel()
        {
            this.Build();
        }
    

	public bool useBPF()
	{
		return this.checkbuttonBPF.Active;
	}
	public bool useMOCO()
	{
		return this.checkbuttonMOCO.Active;
	}
	public bool useMBLL()
	{
		return this.checkbuttonMBLL.Active;
	}
	public bool useGLM()
	{
		return this.checkbuttonRTGLM.Active;
	}

        public void InitializeRealtimeProcess()
        {

            double fs = MainClass.SystemControler.SystemInfo.samplerate;
            MathNet.Filtering.OnlineFilter filter = OnlineFilter.CreateDenoise();

            int nmeas = MainClass.SystemControler.SubjectData.data[0].SDGRef.nummeas;

            // TODO fix this outside of mono
            // LSL.liblsl.StreamInfo LSLinfo= new LSL.liblsl.StreamInfo("BrainScanIR","NIRS",nmeas);
			// LSLoutlet=new LSL.liblsl.StreamOutlet(LSLinfo);

			//if (this.useBPF ()) {
			bool uselpf = false;
            bool usehpf = false;
            double lpf = .5;
            double hpf = .016;


            try
            {
                lpf = Convert.ToDouble(this.entrylpf.Text);
                uselpf = true;
            }
            catch
            {
                uselpf = false;
            }
            try
            {
                hpf = Convert.ToDouble(this.entryhpf.Text);
                usehpf = true;
            }
            catch
            {
                usehpf = false;
            }

            if (uselpf & usehpf)
            {
                filter = OnlineFilter.CreateBandpass(ImpulseResponse.Finite, fs, hpf, lpf);
            }
            else if (!uselpf & usehpf)
            {
                filter = OnlineFilter.CreateHighpass(ImpulseResponse.Finite, fs, hpf);
            }
            else if (uselpf & !usehpf)
            {
                filter = OnlineFilter.CreateLowpass(ImpulseResponse.Finite, fs, lpf);
            }
            else
            {
                this.checkbuttonBPF.Active = false;  // Turn off no valid BPF
            }
           
            int cnt = MainClass.SystemControler.SubjectData.SDGprobe.nummeas *
                               MainClass.SystemControler.SubjectData.SDGprobe.numlambda;
            OnlineFIRFiltersBPF = new MathNet.Filtering.OnlineFilter[cnt];
            for (int i = 0; i < cnt; i++)
            {
                OnlineFIRFiltersBPF[i] = filter;
                OnlineFIRFiltersBPF[i].Reset();
            }



            MocoKalman = new MathNet.Filtering.Kalman.DiscreteKalmanFilter[cnt];
            int order = 5;
            var x0 = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense(order + 1, 1, 0);
            var P0 = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.DenseIdentity(order + 1);

            for (int i = 0; i < cnt; i++)
            {
                MocoKalman[i] = new MathNet.Filtering.Kalman.DiscreteKalmanFilter(x0, P0);
            }

          

                // Real-time GLM model

                GLMKalman = new MathNet.Filtering.Kalman.DiscreteKalmanFilter[cnt];
                int ncond = 5;  /// max number of stimulus conditions
                var x01 = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense(ncond + 1, 1, 0);
                var P01 = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.DenseIdentity(ncond + 1);
                for (int i = 0; i < cnt; i++)
                {
                    GLMKalman[i] = new MathNet.Filtering.Kalman.DiscreteKalmanFilter(x01, P01);
                }

                //initialize an empty window
                int dof = 1;
                Cprobe probe = new Cprobe();
                probe.numdet = MainClass.SystemControler.SubjectData.data[0].SDGRef.numdet;
                probe.numsrc = MainClass.SystemControler.SubjectData.data[0].SDGRef.numsrc;
                probe.nummeas = MainClass.SystemControler.SubjectData.data[0].SDGRef.nummeas;
                probe.numlambda = MainClass.SystemControler.SubjectData.data[0].SDGRef.numlambda;
                probe.detpos = (int[,])MainClass.SystemControler.SubjectData.data[0].SDGRef.detpos.Clone();
                probe.srcpos = (int[,])MainClass.SystemControler.SubjectData.data[0].SDGRef.srcpos.Clone();
                probe.measlist = (int[,])MainClass.SystemControler.SubjectData.data[0].SDGRef.measlist.Clone();
                probe.measlistAct = (bool[])MainClass.SystemControler.SubjectData.data[0].SDGRef.measlistAct.Clone();
                probe.colormap = (Gdk.Color[])MainClass.SystemControler.SubjectData.data[0].SDGRef.colormap.Clone();

                string[] cond = new string[5];

                MathNet.Numerics.LinearAlgebra.Matrix<double>[] C = new MathNet.Numerics.LinearAlgebra.Matrix<double>[cnt];
                for (int i = 0; i < cnt; i++)
                {
                    C[i] = P0.Clone();
                }
                var X = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense(order + 1, cnt, 0);

            MainClass.AppWindow.handles.statswinview.update(X, C, cond, dof, probe);




        }
		public CNIRSdata[] UpdateRealtimeProcess(CNIRSdata[] data)
		{
			int samples = data[0].samples;
			int oldsamples = data[1].samples;
			int nm = data[0].SDGRef.nummeas * data[0].SDGRef.numlambda;




			// Conversion to dOD (and bandpass filter)
			for (int i = oldsamples + 1; i < samples; i++)
			{
				double[] d = new double[nm];
				double t = data[0].time[i];
				for (int j = 0; j < nm; j++)
				{
					d[j] = -Math.Log(data[0].data[j, i]) + Math.Log(data[0].data[j, 0]);

					if (this.useMOCO())
					{
						int order = 5;
						if (i > order)
						{
							var F = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.DenseIdentity(order + 1);
							var Q = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense(order + 1, order + 1, 0);
							var H = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense(1, order + 1);
							var R = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense(1, 1, .001);
							var z = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense(1, 1, d[j]);

							for (int k = 0; k < order; k++)
							{
								H[0, k] = -Math.Log(data[0].data[j, i - k - 1]) + Math.Log(data[0].data[j, 0]);
								//data [1].data [j, i - k - 1];
							}

							MocoKalman[j].Predict(F, Q);
							MocoKalman[j].Update(z, H, R);

							var ressid = H * MocoKalman[j].State;
							d[j] = ressid[0, 0];
						}

					}



					if (this.useBPF())
					{
						d[j] = OnlineFIRFiltersBPF[j].ProcessSample(d[j]);
					}

				}
				data[1].adddata(d, t);
				data[1].SDGRef = data[0].SDGRef;
				data[1].StimInfo = data[0].StimInfo;

			}
			int lastID = 1;
			if (this.useMBLL())
			{
				// MBLL
				// TODO 
				double E11, E12, E21, E22;
				int sIdx1, sIdx2;

				int numml = data[0].SDGRef.nummeas;
				for (int i = oldsamples + 1; i < samples; i++)
				{
					double[] d = new double[numml * 2];
					double t = data[0].time[i];
					for (int j = 0; j < numml; j++)
					{
						sIdx1 = data[0].SDGRef.measlist[j, 3];
                        E11 = MainClass.SystemControler.SystemInfo.laserarray[sIdx1].ExtHbO;
                        E12 = MainClass.SystemControler.SystemInfo.laserarray[sIdx1].ExtHbR;
						sIdx2 = data[0].SDGRef.measlist[j + numml, 3];
                        E21 = MainClass.SystemControler.SystemInfo.laserarray[sIdx2].ExtHbO;
                        E22 = MainClass.SystemControler.SystemInfo.laserarray[sIdx2].ExtHbR;

						double L = data[0].SDGRef.distances[j];
						d[j] = 1000000 * 1 / L * (E22 * data[1].data[j, i] - E12 * data[1].data[j + numml, i]) / (E11 * E11 - E12 * E21);
						d[j + numml] = 1000000 * 1 / L * (E21 * data[1].data[j + numml, i] - E11 * data[1].data[j, i]) / (E11 * E11 - E12 * E21);

					}
					data[2].adddata(d, t);
				}
				data[2].SDGRef = data[0].SDGRef;
				data[2].StimInfo = data[0].StimInfo;
				lastID = 2;
			}

			if (useGLM())
			{
				for (int i = oldsamples + 1; i < samples; i++)
				{
					double q = hscaleKalmanQ.Value;
					double[] d = new double[nm];
					double[] t = new double[1];
					t[0] = data[0].time[i];

					var x = data[0].StimInfo.GetDesignMtx(t);

					int order = 5;
					var H = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense(1, order + 1);
					var F = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.DenseIdentity(order + 1);
					var Q = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense(order + 1, order + 1, q);

					for (int j = 0; j < x.ColumnCount - 1; j++)
					{  //DC regressor is always end
						H[0, j] = x[0, j];
					}
					for (int j = x.ColumnCount - 1; j < order + 1; j++)
					{
						H[0, j] = 0;
					}
					H[0, order] = 1; // DC regressor at the end

					var B = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense(order + 1, nm);
					MathNet.Numerics.LinearAlgebra.Matrix<double>[] C = new MathNet.Numerics.LinearAlgebra.Matrix<double>[nm];
					var R = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense(1, 1, .001);
					for (int j = 0; j < nm; j++)
					{
						d[j] = data[lastID].data[j, i];

						var z = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense(1, 1, d[j]);

						GLMKalman[j].Predict(F, Q);
						GLMKalman[j].Update(z, H, R);

						var b = GLMKalman[j].State;
						for (int k = 0; k < order + 1; k++)
						{
							B[k, j] = b[k, 0];
						}
						C[j] = GLMKalman[j].Cov;

					}
					MainClass.AppWindow.handles.statswinview.update(B, C, data[2].StimInfo.conditions, samples);

				}
			}


			if (this.checkbuttonDataXfer.Active)
			{
                if(entryRTfileName.Active==0){
                    string dataout = "";
				    for (int i = oldsamples + 1; i < samples; i++)
					    {
						    dataout += String.Format("{0}", data[this.comboboxwhichXfer.Active].time[i]);
						    for (int j = 0; j < nm; j++)
						    {
							    dataout += String.Format("\t{0}", data[this.comboboxwhichXfer.Active].data[j, i]);
						    }
						    dataout += "\r\n";
					    }

                        // Transfer the data in real-time to a file
                        string file = entryRTfileName.ActiveText;
                        string rtXferfile = System.IO.Path.Combine(MainClass.SystemControler.SubjectData.outfolder, file);
				        System.IO.File.AppendAllText(rtXferfile, dataout);
                    }else{
                    // Lab Streaming layer

                    double[] sample = new double[nm+1];
					for (int i = oldsamples + 1; i < samples; i++)
					{
						sample[0]=data[this.comboboxwhichXfer.Active].time[i];
						for (int j = 0; j < nm; j++)
						{
							sample[1+j] =data[this.comboboxwhichXfer.Active].data[j, i];
						}
					 LSLoutlet.push_sample(sample);
					}
                   
                }
			}
            

			return data;

		}
		protected void BPFchanged(object sender, EventArgs e)
		{
			bool uselpf = false;
			bool usehpf = false;
			double lpf = .5;
			double hpf = .016;

            double fs = MainClass.SystemControler.SystemInfo.samplerate;
			MathNet.Filtering.OnlineFilter filter = OnlineFilter.CreateDenoise();

			try
			{
				lpf = Convert.ToDouble(this.entrylpf.Text);
				uselpf = true;
			}
			catch
			{
				uselpf = false;
			}
			try
			{
				hpf = Convert.ToDouble(this.entryhpf.Text);
				usehpf = true;
			}
			catch
			{
				usehpf = false;
			}

			if (uselpf & usehpf)
			{
				filter = OnlineFilter.CreateBandpass(ImpulseResponse.Finite, fs, hpf, lpf);
			}
			else if (!uselpf & usehpf)
			{
				filter = OnlineFilter.CreateHighpass(ImpulseResponse.Finite, fs, hpf);
			}
			else if (uselpf & !usehpf)
			{
				filter = OnlineFilter.CreateLowpass(ImpulseResponse.Finite, fs, lpf);
			}
			else
			{
				this.checkbuttonBPF.Active = false;  // Turn off no valid BPF
			}
			//}
			//if (this.useBPF ()) {
            int cnt = MainClass.SystemControler.SubjectData.SDGprobe.nummeas *
                               MainClass.SystemControler.SubjectData.SDGprobe.numlambda;
			OnlineFIRFiltersBPF = new OnlineFilter[cnt];
			for (int i = 0; i < cnt; i++)
			{
				OnlineFIRFiltersBPF[i] = filter;
				OnlineFIRFiltersBPF[i].Reset();
			}
		}

		protected void UseMBLLChanged(object sender, EventArgs e)
		{
            int act = MainClass.AppWindow.handles.combobox_wh.Active;
			MainClass.AppWindow.handles.combobox_wh.Clear();

			CellRendererText cell = new CellRendererText();
			MainClass.AppWindow.handles.combobox_wh.PackStart(cell, false);
			MainClass.AppWindow.handles.combobox_wh.AddAttribute(cell, "text", 0);
			ListStore store = new ListStore(typeof(string));
			MainClass.AppWindow.handles.combobox_wh.Model = store;

            for (int i = 0; i < MainClass.SystemControler.SystemInfo.numwavelengths; i++)
			{
                MainClass.AppWindow.handles.combobox_wh.AppendText(String.Format("Raw {0}nm", MainClass.SystemControler.SystemInfo.wavelengths[i]));
			}
            for (int i = 0; i < MainClass.SystemControler.SystemInfo.numwavelengths; i++)
			{
                MainClass.AppWindow.handles.combobox_wh.AppendText(String.Format("dOD {0}nm", MainClass.SystemControler.SystemInfo.wavelengths[i]));
			}
			if (this.useMBLL())
			{
				MainClass.AppWindow.handles.combobox_wh.AppendText("Oxy-Hemoglobin");
				MainClass.AppWindow.handles.combobox_wh.AppendText("Deoxy-Hemoglobin");
			}
			else
			{
				if (act > 3)
					act = 3;
			}

            MainClass.AppWindow.handles.combobox_wh.Active = act;


		}

		protected void WirelessDarkNoiseClicked(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		protected void WirelessLPFClicked(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		
	}

}
