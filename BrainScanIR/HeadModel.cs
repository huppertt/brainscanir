﻿using System;
using Gtk;
using System.Threading;
using System.Xml;
using System.Linq;
using System.IO;


namespace BrainScanIR
{


    public partial class MainWindow : Gtk.Window
    {
		// These hold the head/brain and 10-20 labels for the Colin27 atlas.
		public static Gifti[] headmodel;
		public static Gifti sphereGifti;
		public static TenTwenty c1020;
		public static MathNet.Numerics.LinearAlgebra.Matrix<double> Smoother;

        public bool show1020;
        public bool showkin;
        public bool showprobe;

        public void LoadHeadModel()
        {
            headmodel = new Gifti[2];
            headmodel[0] = new Gifti();
            headmodel[0].ReadGifti(@"surf/Skin.gii");

            headmodel[1] = new Gifti();
            headmodel[1].ReadGifti(@"surf/Pial.gii");
            sphereGifti = new Gifti();
            sphereGifti.ReadGifti(@"surf/Sphere-ico3.gii");
            c1020 = new TenTwenty(@"surf/pts1020.xml");
            for (int i = 0; i < c1020.points.Length; i++)
            {
                double[,] p = new double[1, 3];
                p[0, 0] = (double)c1020.points[i].X;
                p[0, 1] = (double)c1020.points[i].Y;
                p[0, 2] = (double)c1020.points[i].Z;
                p = headmodel[0].set2surface(p, 1);
                c1020.points[i].X = (float)p[0, 0];
                c1020.points[i].Y = (float)p[0, 1];
                c1020.points[i].Z = (float)p[0, 2];

            }


			show1020=true;
        showkin = false;
        showprobe = false;


            double[,] Sm = new double[headmodel[1].nvertices * 2, headmodel[1].nvertices * 2];
            using (BinaryReader b = new BinaryReader(File.Open(@"surf/GaussianSmooth.bin", FileMode.Open)))
            {

                int pos = 0;
                int length = (int)b.BaseStream.Length;
                while (pos < length)
                {
                    int i = (int)b.ReadSingle();
                    int j = (int)b.ReadSingle();
                    float v = b.ReadSingle();
                    Sm[j - 1, i - 1] = (double)v;
                    Sm[j - 1 + headmodel[1].nvertices, i - 1 + headmodel[1].nvertices] = (double)v;
                    // 4.
                    // Advance our position variable.
                    pos += sizeof(float);
                    pos += sizeof(float);
                    pos += sizeof(float);
                }
            }
            Smoother = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.SparseOfArray(Sm);
        }
    }
}
