﻿using System;
using System.Collections;
using System.Threading;
using System.Diagnostics;
using System.IO.Ports;

namespace BrainScanIR
{

	public class CTechEnCw6
	{
		public int[] gain { get; set; }
		public bool[] lason { get; set; }
        public int[] laserpower { get; set; }
		protected bool[] measlist;
		public static bool isrunning;
		public static int samplesavaliable;
		public static int samplerate;
		static Queue[] dataqueue;
		static int maxmeas;
		private int maxdet;
		private int maxsrc;
		static int maxaux;
		public static Thread newthread;
		public static FTD2XX_NET.FTDI ftdi;
		bool useserial;
		static SerialPort wirelessserial;

		public bool getisrunning()
		{
			return isrunning;
		}

		public int getsamplesavaliable()
		{
			return samplesavaliable;
		}

		public CTechEnCw6()
		{
			maxdet = 32;
			maxsrc = 32;
			maxaux = 8;
			maxmeas = 1024;
			samplerate = 20;
			dataqueue = new Queue[maxmeas + maxaux];
			useserial = false;

			gain = new int[maxdet];
			lason = new bool[maxsrc];
			//	laspow = new int[maxsrc];

			for (int i = 0; i < maxdet; i++)
			{
				gain[i] = 0;
			}
			for (int i = 0; i < maxsrc; i++)
			{
				lason[i] = false;

			}
			isrunning = false;
			samplesavaliable = 0;

			for (int i = 0; i < maxmeas + maxaux; i++)
			{
				dataqueue[i] = new Queue();
			}

			if (useserial)
			{

				string[] ports = SerialPort.GetPortNames();

				int found = 1;
				for (int i = 0; i < ports.Length; i++)
				{
					if (ports[i].Contains("/dev/tty.usbserial"))
						found = i;

				}
				// TODO fix this
				try
				{
					wirelessserial = new SerialPort(ports[found], 230400, Parity.None, 8);  //TODO fix this.
					wirelessserial.StopBits = StopBits.One;
					wirelessserial.Handshake = Handshake.None;
					wirelessserial.Open();
				}
				catch
				{
					Gtk.MessageDialog msg = new Gtk.MessageDialog(null, Gtk.DialogFlags.Modal, Gtk.MessageType.Error, Gtk.ButtonsType.Ok, "Failed to load system");
					msg.Run();
					Process.GetCurrentProcess().CloseMainWindow();
				}

			}
			else {
				ftdi = new FTD2XX_NET.FTDI();
				initialize();
			}

			alloff();
			SendSerial("STOP");

			for (int i = 0; i < maxdet; i++)
			{
				setgain(i, gain[i]);
			}
			for (int i = 0; i < maxsrc; i++)
			{
				setlaser(i, lason[i]);
				//setlaserpower(i, laspow[i]);
			}

		}

		~CTechEnCw6()
		{
			alloff();
			SendSerial("STOP");
			if (useserial)
			{
				wirelessserial.Close();
				wirelessserial.Dispose();
			}
		}




		public static void adddata()
		{
			// TODO
			int wait;
			wait = (500 / samplerate);
			Debug.WriteLine("Running thread");
			char[] line = new char[121];
			while (isrunning)
			{
				uint cnt = 0;
				ftdi.GetRxBytesAvailable(ref cnt);

				while (cnt > 122)
				{
					int ch = 0;
					while (ch != 36)
					{
						//ch = ftdi.Read(
					}
					wirelessserial.Read(line, 0, 121);

					for (int i = 0; i < maxmeas + maxaux; i++)
					{
						double value = 0;
						value += 16 * 16 * 16 * char2val(line[10 + i * 5]);
						value += 16 * 16 * char2val(line[10 + i * 5 + 1]);
						value += 16 * char2val(line[10 + i * 5 + 2]);
						value += char2val(line[10 + i * 5 + 3]);
						dataqueue[i].Enqueue(value);
					}
					samplesavaliable += 1;

					ftdi.GetRxBytesAvailable(ref cnt);
				}
				System.Threading.Thread.Sleep(wait);
			}


		}

		static double char2val(char a)
		{
			switch (a.ToString())
			{
				case "0":
					return 0;
				case "1":
					return 1;
				case "2":
					return 2;
				case "3":
					return 3;
				case "4":
					return 4;
				case "5":
					return 5;
				case "6":
					return 6;
				case "7":
					return 7;
				case "8":
					return 8;
				case "9":
					return 9;
				case "A":
					return 10;
				case "B":
					return 11;
				case "C":
					return 12;
				case "D":
					return 13;
				case "E":
					return 14;
				case "F":
					return 15;
				default:
					return 0;
			}

		}

		static string val2char(int a)
		{
			switch (a)
			{
				case 0:
					return "0";
				case 1:
					return "1";
				case 2:
					return "2";
				case 3:
					return "3";
				case 4:
					return "4";
				case 5:
					return "5";
				case 6:
					return "6";
				case 7:
					return "7";
				case 8:
					return "8";
				case 9:
					return "9";
				case 10:
					return "A";
				case 11:
					return "B";
				case 12:
					return "C";
				case 13:
					return "D";
				case 14:
					return "E";
				case 15:
					return "F";
				default:
					return "0";
			}

		}


		public Cprobe initialize(Cprobe probe)
		{
			initialize();
			// Send measurement info to the device
			bool[] mlall = new bool[maxmeas];
			for (int i = 0; i < maxmeas; i++)
			{
				mlall[i] = false;
			}

			//[1 9 2 10 3 11 4 12 5 13 6 14 7 15 8 16 17 25 18 26 19 27 20 28 21 29 22 30 23 31 24 32];
			int[] FreqList = {1, 3, 5, 7, 9, 11, 13, 15, 2, 4, 6, 8, 10, 12, 14, 16,
				17, 19, 21, 23, 25, 27, 29, 31, 18, 20, 22, 24, 26, 28, 30, 32};

			for (int i = 0; i < probe.nummeas; i++)
			{
				int idx = maxdet*probe.measlist[i, 4] + FreqList[probe.measlist[i, 3]]; // 3= src & 4=det
				mlall[idx] = true;
			}

			bool[] DetMask=new bool[maxdet];
			bool[] SrcMask=new bool[maxsrc];
			for (int i = 0; i < maxdet; i++)
			{
				DetMask[i] = false;
			}
			for (int i = 0; i < maxsrc; i++)
			{
				SrcMask[i] = false;
			}

			for (int i = 0; i < probe.nummeas; i++)
			{
				SrcMask[probe.measlist[i, 3]] = true;
				DetMask[probe.measlist[i, 4]] = true;
				int idx = maxsrc * probe.measlist[i, 3] + probe.measlist[i, 4];
				int cnt = 0;
				for (int j = 0; j < idx; j++)
				{
					if (mlall[j])
						cnt++;
				}
				probe.measlist[i, 5] = cnt;
			}

			// Send the measurement mask to the system
			int c = 0;
			for (int idx = 0; idx < maxdet; idx++)
			{
				for (int srcidx = 0; srcidx < maxsrc; srcidx++)
				{
					SrcMask[srcidx] = mlall[c];
					if (SrcMask[srcidx])
						DetMask[idx] = true;

					c++;
				}
				string msg = bool2hex(SrcMask);

				string srcmsg = "DSRC " + msg;
				SendSerial(srcmsg);
			}
			string msg2 = bool2hex(DetMask);
			string detmsg = "DSEL " + msg2;
			SendSerial(detmsg);
		

			return probe;
		}

		public bool initialize()
		{
			for (int i = 0; i < maxmeas; i++)
			{
				dataqueue[i].Clear();
			}
			isrunning = false;
			samplesavaliable = 0;

			if (!useserial)
			{
				// TODO
				FTD2XX_NET.FTDI.FT_STATUS ftstatus;


				UInt32 devcount = 0;
				ftstatus = ftdi.GetNumberOfDevices(ref devcount);
				string msg = String.Format("Found {0} FTDI devices", devcount);
				//PhotonViewer.AppWindow.paneldb.debugmsg (msg);

				FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE[] devicelist;
				devicelist = new FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE[devcount];
				// Instantiate the array elements as FT_DEVICE_INFO_NODE
				for (UInt32 i = 0; i < devcount; i++)
				{
					devicelist[i] = new FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE();
				}

				ftstatus = ftdi.GetDeviceList(devicelist);
				for (UInt32 i = 0; i < devcount; i++)
				{
					msg = String.Format("Device {0}: Serial Number {1}", i, devicelist[i].SerialNumber);
					//		PhotonViewer.AppWindow.paneldb.debugmsg (msg);
					msg = String.Format("COM {0}", devicelist[i].LocId);
					//		PhotonViewer.AppWindow.paneldb.debugmsg (msg);
				}

				if (!ftdi.IsOpen)
				{
					ftstatus = ftdi.OpenByIndex((uint)0);

					if (ftstatus == FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
					{
						//msg = "Connected to Cw6";
						//PhotonViewer.AppWindow.paneldb.debugmsg(msg);

						//uint contextID;
						//contextID = AppWindow.statusbardevice.GetContextId(msg);
						//AppWindow.statusbardevice.Push(contextID, msg);

					}
				}
				ftdi.ResetDevice();

				ftdi.SetDataCharacteristics(FTD2XX_NET.FTDI.FT_DATA_BITS.FT_BITS_8,
					FTD2XX_NET.FTDI.FT_STOP_BITS.FT_STOP_BITS_1,
				     FTD2XX_NET.FTDI.FT_PARITY.FT_PARITY_ODD);
				ftdi.SetBaudRate((UInt32)9600);
				ftdi.SetFlowControl(FTD2XX_NET.FTDI.FT_FLOW_CONTROL.FT_FLOW_NONE, 1, 1);

				return (ftstatus == FTD2XX_NET.FTDI.FT_STATUS.FT_OK);
			}
			else
				return true;

		}




		public bool setML(int src,int det,bool state){
			int fnd = 0;
			int cnt = 0;
			for (int i = 0; i < maxsrc; i++) {
				for (int j = 0; j < maxdet; j++) {
					if(i==src & j==det)
						fnd=cnt;
					cnt++;
				}
			}
			measlist [fnd] = state;
			return true;
		}


		public bool setTDML(bool state){
			if(state)
				SendSerial("SEQ1 1");
			else
				SendSerial("SEQ1 0");
			return true;
		}

		public bool setgain(int indx,int g){
			gain [indx] = g;
			// TODO- send signal to Cw6

			string msg = String.Format ("SETG {0} {1}", indx + 1, g);
			bool ok = SendSerial (msg);
			return ok;
		}
		public bool setlaserpower(int indx,int g)
		{
            laserpower[indx] = g;
			// TODO- send signal to Cw6

			string msg = String.Format("SETL {0} {1}", indx + 1, g);
			bool ok = SendSerial(msg);
            return ok;
		}
		public int getlaserpower(int indx)
		{
			return laserpower[indx];
		}

		public int getgain(int indx){
			return gain[indx];
		}



		public bool setlaser(int indx,bool state){
			lason [indx] = state;
			// TODO- send signal to Cw6
			string msg=bool2hex(lason);

			msg = "LASR " + msg;
			bool ok = SendSerial (msg);
			return ok;
		}


		public bool getlaser(int indx){
			return lason[indx];
		}

		public double[] getdata()
		{
			double[] thisdata = new double[maxmeas + maxaux];
			for (int i = 0; i < maxmeas + maxaux; i++)
			{
				thisdata[i] = (double)dataqueue[i].Dequeue();
			}
			samplesavaliable -= 1;
			return thisdata;
		}



		public bool start()
		{
			//initialize ();

			// Send start command

			Debug.WriteLine("Started function");
			isrunning = true;
			newthread = new Thread(adddata);
			newthread.Start();

			if (useserial)
			{
				wirelessserial.DiscardInBuffer();
				wirelessserial.DiscardOutBuffer();
			}
			else {
				ftdi.Purge(FTD2XX_NET.FTDI.FT_PURGE.FT_PURGE_RX);
				ftdi.Purge(FTD2XX_NET.FTDI.FT_PURGE.FT_PURGE_TX);
			}

			bool ok = SendSerial("run");


			return ok;
		}


		public bool stop(){

			// Send stop command
			bool ok = SendSerial("STOP");
			isrunning=false;
			newthread.Abort ();
			return ok;
		}

		public bool alloff(){
			for (int i = 0; i < maxsrc; i++) {
				lason[i] = false;
			}

			SendSerial("LASR 00000000");
			return true;
		}

		public bool setrate(int rate){
			samplerate = rate;

			// TODO- send signal to Cw6
			string msg = "RATE ";
			msg += String.Format ("{0}", rate);
			bool ok=SendSerial (msg);
			return ok;
		}

		public bool sendML2device(){
			bool[] DetMask=new bool[maxdet];
			bool[] SrcMask=new bool[maxsrc];

			//first mask all the detectors
			for(int idx=0; idx<maxdet; idx++){
				DetMask[idx]=false;
			}

			int cnt=0;


			string lsrmsg;
			string msg;

			//Laser frq mapping
			//[1 9 2 10 3 11 4 12 5 13 6 14 7 15 8 16 17 25 18 26 19 27 20 28 21 29 22 30 23 31 24 32];
			bool ok;
			for(int idx=0; idx<maxdet; idx++){
				for(int srcidx=0; srcidx<maxsrc; srcidx++){
					SrcMask[srcidx]=measlist[cnt];
					if(SrcMask[srcidx])
						DetMask[idx]=true;

					cnt++;
				}
				msg=bool2hex(SrcMask);

				lsrmsg=String.Format("DSRC {0} ",idx+1)+msg;
				//AfxMessageBox(lsrmsg);
				ok=SendSerial(lsrmsg);
				System.Threading.Thread.Sleep(50);
			}


			msg=bool2hex(DetMask);
			lsrmsg="DSEL "+msg;
			ok=SendSerial(lsrmsg);
			System.Threading.Thread.Sleep(50);

			return ok;
		}

		public bool SendSerial(string msg){
			msg += "\r\n";
			uint BytesWritten=0;
			int BuffSize;
			BuffSize = msg.Length;

			FTD2XX_NET.FTDI.FT_STATUS ftstatus;
			ftstatus = ftdi.Write(msg,BuffSize,ref BytesWritten);
			System.Threading.Thread.Sleep (50);
			return (ftstatus==FTD2XX_NET.FTDI.FT_STATUS.FT_OK);
		}


		public string bool2hex(bool[] valueIn){


			//string hex = BitConverter.ToString(valueIn);
			//return hex.Replace("-","");

			char[] temp = new char[8];
			string tmp2;

			int cnt;
			cnt=0;
			for(int idx=0; idx<valueIn.Length; idx++){
				if(valueIn[idx])
					cnt+=(int)(Math.Pow(2,idx));
			}


			int val;
			for(int idx=7; idx>-1; idx=idx-1){
				val=(int)(cnt/Math.Pow(16,idx));
				//val=cnt/pow(double(16),idx);

				tmp2=val2char(val);
				temp[idx]=tmp2[0];
				cnt=(int)cnt % (int)(Math.Pow(16,idx));
			}

			string valueOut;
			valueOut="";
			for(int idx=7; idx>-1; idx=idx-1){
				valueOut+=temp[idx];
			}

			return valueOut;
		}

		public void autoadjust_gain()
		{
            //TODO
			Random rnd = new Random();

			// in the simulator just make it random
			for (int i = 0; i < maxdet; i++)
			{
				gain[i] = rnd.Next(9);
			}
			return;
		}
		public void autoadjust_lasers()
		{
			//TODO
			Random rnd = new Random();

			// in the simulator just make it random
			for (int i = 0; i < maxsrc; i++)
			{
				laserpower[i] = rnd.Next(9);
			}
			return;
		}

		

	}


}