﻿using System;
using System.Xml;
//using System.Collections.Generic;
//using System.Xml.Serialization;
//using System.IO;
using Gdk;
//using Gtk;

namespace BrainScanIR
{
	public class Cprobe
	// Class to hold the probe/SDG object
	{
		public int numsrc;
		public int numdet;
		public int nummeas;
		public int numlambda;

		public int [,] srcpos;
		public int [,] detpos;

		public double[,] srcpos3D;
		public double[,] detpos3D;

		public int[,] laserpos;
		public int[,] measlist;
		public bool[] measlistAct;

		public int[,] lasermap;
		public int[] detmap;
		public string[] laserrename;
		public string filename;
		public double[] distances;
		public bool isregistered;


		// This specifies the colors used for the probe and comes from the DefaultConfig.xml file
		public Gdk.Color[] colormap;
		public Gdk.Color[] colormapstats;

		public Cprobe ()
		{
			isregistered = false;
			filename = "";
			// Load the colormaps from the XML file
			XmlDocument doc = new XmlDocument();
			doc.Load(@"DefaultConfig.xml");
			XmlNodeList elemList;
			XmlNodeList elemListAll;
			XmlNodeList elemListsub;

			elemListAll = doc.GetElementsByTagName("colormap");
			doc = new XmlDocument();
			doc.LoadXml ("<root>" + elemListAll [0].InnerXml + "</root>");
			elemList = doc.GetElementsByTagName("color");

			colormap =new Gdk.Color[elemList.Count];
			for (int i=0; i < elemList.Count; i++){
				XmlDocument doc2 = new XmlDocument();
				doc2.LoadXml ("<root>" + elemList [i].InnerXml + "</root>");
				elemListsub = doc2.GetElementsByTagName("R");
				byte r = Convert.ToByte(elemListsub [0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("G");
				byte g = Convert.ToByte(elemListsub [0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("B");
				byte b = Convert.ToByte(elemListsub [0].InnerXml);
				colormap[i]=new Gdk.Color(r,g,b);
			}

			doc = new XmlDocument();
			doc.LoadXml("<root>" + elemListAll[1].InnerXml + "</root>");
			elemList = doc.GetElementsByTagName("color");

			colormapstats = new Gdk.Color[elemList.Count];
			for (int i = 0; i < elemList.Count; i++)
			{
				XmlDocument doc2 = new XmlDocument();
				doc2.LoadXml("<root>" + elemList[i].InnerXml + "</root>");
				elemListsub = doc2.GetElementsByTagName("R");
				byte r = Convert.ToByte(elemListsub[0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("G");
				byte g = Convert.ToByte(elemListsub[0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("B");
				byte b = Convert.ToByte(elemListsub[0].InnerXml);
				colormapstats[i] = new Gdk.Color(r, g, b);
			}

		//	headmodel = new Gifti[0];	
		}

		public void loadxmlprobe(string file)
		{  // This function loads a probe from an xml file

			XmlDocument doc = new XmlDocument();
			XmlDocument doc2;
			doc.Load(file);
			XmlNodeList elemList;
			XmlNodeList elemListsub;

			this.numlambda = 2;
			this.filename = file;


			// Source-positions
			elemList = doc.GetElementsByTagName("srcpos");
			this.numsrc=elemList.Count;
			srcpos = new int[this.numsrc,2];
			for (int i=0; i < elemList.Count; i++)
			{   
				doc2 = new XmlDocument();
				doc2.LoadXml ("<root>" + elemList [i].InnerXml + "</root>");
				elemListsub = doc2.GetElementsByTagName("index");
				int index = Convert.ToInt32(elemListsub [0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("x");
				srcpos[index,0]= Convert.ToInt32(elemListsub [0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("y");
				srcpos[index,1]= Convert.ToInt32(elemListsub [0].InnerXml);

			}  

			// Detector-positions
			elemList = doc.GetElementsByTagName("detpos");
			this.numdet=elemList.Count;
			detpos = new int[this.numdet,2];
			for (int i=0; i < elemList.Count; i++)
			{   
				doc2 = new XmlDocument();
				doc2.LoadXml ("<root>" + elemList [i].InnerXml + "</root>");
				elemListsub = doc2.GetElementsByTagName("index");
				int index = Convert.ToInt32(elemListsub [0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("x");
				detpos[index,0]= Convert.ToInt32(elemListsub [0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("y");
				detpos[index,1]= Convert.ToInt32(elemListsub [0].InnerXml);

			}  

			// 3D corrdinates

			// Source-positions
			elemList = doc.GetElementsByTagName("srcpos3D");
			srcpos3D = new double[this.numsrc, 3];
			for (int i = 0; i < elemList.Count; i++)
			{
				doc2 = new XmlDocument();
				doc2.LoadXml("<root>" + elemList[i].InnerXml + "</root>");
				elemListsub = doc2.GetElementsByTagName("index");
				int index = Convert.ToInt32(elemListsub[0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("x");
				srcpos3D[index, 0] = Convert.ToDouble(elemListsub[0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("y");
				srcpos3D[index, 1] = Convert.ToDouble(elemListsub[0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("z");
				srcpos3D[index, 2] = Convert.ToDouble(elemListsub[0].InnerXml);
				isregistered = true;

			}

			// Detector-positions
			elemList = doc.GetElementsByTagName("detpos3D");
			detpos3D = new double[this.numdet, 3];
			for (int i = 0; i < elemList.Count; i++)
			{
				doc2 = new XmlDocument();
				doc2.LoadXml("<root>" + elemList[i].InnerXml + "</root>");
				elemListsub = doc2.GetElementsByTagName("index");
				int index = Convert.ToInt32(elemListsub[0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("x");
				detpos3D[index, 0] = Convert.ToDouble(elemListsub[0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("y");
				detpos3D[index, 1] = Convert.ToDouble(elemListsub[0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("z");
				detpos3D[index, 2] = Convert.ToDouble(elemListsub[0].InnerXml);

			}


			// Set the default Laser->SrcPos mapping
			// TODO - read this from the file to replace the defaults
			lasermap= new int[this.numsrc,this.numlambda];
			laserrename = new string[this.numsrc*this.numlambda];
			for (int i = 0; i < this.numsrc; i++) {
				for (int j = 0; j < this.numlambda; j++) {
					lasermap [i, j] = i * 2 + j;

						if(MainClass.DEBUG){
						laserrename[i * 2 + j]=String.Format("Laser {0}",i * 2 + j);
						}else{
                        double lambda =MainClass.SystemControler.SystemInfo.wavelengths[j];  // TODO
						laserrename[i * 2 + j] = String.Format("Source {0} [{1}nm]",i,lambda);
						}

				}
			}
			detmap = new int[this.numdet];
			for (int i=0; i<this.numdet; i++){
				detmap[i]=i;
			}

			// Measurement list
			elemList = doc.GetElementsByTagName("ml");
			this.nummeas=elemList.Count;
			measlist = new int[this.nummeas*this.numlambda,6];
			distances = new double[this.nummeas];
			for (int j = 0; j < this.numlambda; j++) {
				for (int i=0; i < elemList.Count; i++)
				{   
					doc2 = new XmlDocument ();
					doc2.LoadXml ("<root>" + elemList [i].InnerXml + "</root>");
					int ii = this.nummeas * j + i;
					elemListsub = doc2.GetElementsByTagName ("src");
					measlist [ii, 0] = Convert.ToInt16 (elemListsub [0].InnerXml);
					elemListsub = doc2.GetElementsByTagName ("det");
					measlist [ii, 1] = Convert.ToInt16 (elemListsub [0].InnerXml);
					measlist [ii, 2] = j;
					measlist [ii, 3] = lasermap [measlist [ii, 0], j];
					measlist [ii, 4] = detmap [measlist [ii, 1]];
					measlist[ii, 5] = ii;
					distances [i] = Math.Sqrt (((this.srcpos [measlist [ii, 0], 0] - this.detpos [measlist [ii, 1], 0]) *
					(this.srcpos [measlist [ii, 0], 0] - this.detpos [measlist [ii, 1], 0])) +
					((this.srcpos [measlist [ii, 0], 1] - this.detpos [measlist [ii, 1], 1]) *
							(this.srcpos [measlist [ii, 0], 1] - this.detpos [measlist [ii, 1],1])));
				}

			}

			this.measlistAct = new bool[this.nummeas];
			for (int i = 0; i < this.nummeas; i++) {
				this.measlistAct [i] = true;
			}
			return;

		}
		public void drawsdg(Gdk.Drawable da){
			// This function draws the SDG probe


		
			int width, height;
			da.GetSize (out width, out height);

			float dx, dy;
			dx = 20;
			dy = 20;
			width = width -2*(int)dy;
			height = height -2*(int)dx;

			float maxX, minX, maxY, minY;
			maxX = -999; maxY = -999;
			minX = 999; minY = 999;
			for (int i = 0; i < this.numdet; i++) {
				if (maxX < this.detpos [i, 0])
					maxX = this.detpos [i, 0];
				if (maxY < this.detpos [i, 1])
					maxY = this.detpos [i, 1];
				if (minX > this.detpos [i, 0])
					minX = this.detpos [i, 0];
				if (minY > this.detpos [i, 1])
					minY = this.detpos [i, 1];
			}
			for (int i = 0; i < this.numsrc; i++) {
				
					if (maxX < this.srcpos [i, 0])
						maxX = this.srcpos [i, 0];
					if (maxY < this.srcpos [i, 1])
						maxY = this.srcpos [i, 1];
					if (minX > this.srcpos [i, 0])
						minX = this.srcpos [i, 0];
					if (minY > this.srcpos [i, 1])
						minY = this.srcpos [i, 1];
				}
			float rangeX = maxX - minX;
			float rangeY = maxY - minY;

			Gdk.GC gc = new Gdk.GC (da);

			gc.RgbBgColor = new Gdk.Color (0, 0, 0);
			gc.RgbFgColor = new Gdk.Color (0,0,0);
			Rectangle rarea = new Rectangle();
			rarea.Height = height+2 * (int)dy;
			rarea.Width = width+2 * (int)dx;
			da.DrawRectangle (gc, true, rarea);

			gc.RgbBgColor = new Gdk.Color (0, 0, 0);
			gc.RgbFgColor = new Gdk.Color (255, 255, 255);
			rarea = new Rectangle();
			rarea.Height = height-2+2* (int)dx; ;
			rarea.Width = width-2+2 * (int)dx; ;
			rarea.X = 1;
			rarea.Y = 1;

			da.DrawRectangle (gc, true, rarea);

			int sz = 10;


			gc.RgbFgColor = new Gdk.Color (0, 0, 0);
			gc.SetLineAttributes(3, LineStyle.Solid, CapStyle.Projecting, JoinStyle.Round);
			for (int i = 0; i < this.nummeas; i++) {
					if (this.measlistAct [i]) {
					gc.RgbFgColor = colormap[i]; //new Gdk.Color (0, 0, 0);
					}
					else {
						gc.RgbFgColor = new Gdk.Color (230, 230, 230);
					}
					int si = this.measlist [i, 0];
					int di = this.measlist [i, 1];

					float x1 = (this.detpos[di,0]-minX)/rangeX*width+dx;
					float y1 = (this.detpos[di,1]-minY)/rangeY*height+dy;
					float x2 = (this.srcpos[si,0]-minX)/rangeX*width+dx;
					float y2 = (this.srcpos[si,1]-minY)/rangeY*height+dy;
					da.DrawLine(gc,(int)x1,(int)y1,(int)x2,(int)y2);
					//pts[cnt]=new Gdk.Point((int)x,(int)y);

			}


			gc.RgbFgColor = new Gdk.Color (0,255, 0);
			gc.SetLineAttributes (3, LineStyle.Solid, CapStyle.Round, JoinStyle.Round);
		//	Gdk.Point[] pts = new Gdk.Point[this.numdet+this.numsrc];
			for (int i = 0; i < this.numdet; i++) {
				float x = (this.detpos[i,0]-minX)/rangeX*width+dx-sz/2;
				float y = (this.detpos[i,1]-minY)/rangeY*height+dy-sz/2;
				da.DrawArc (gc, true, (int)x, (int)y, sz, sz, 0, 360 * 64);
				//pts[cnt]=new Gdk.Point((int)x,(int)y);
			}

			gc.RgbBgColor = new Gdk.Color (0, 255, 0);
			gc.RgbFgColor = new Gdk.Color (255, 0, 0);
			gc.SetLineAttributes (3, LineStyle.Solid, CapStyle.Round, JoinStyle.Round);
			for (int i = 0; i < this.numsrc; i++) {
				float x = (this.srcpos[i,0]-minX)/rangeX*width+dx-sz/2;
				float y = (this.srcpos[i,1]-minY)/rangeY*height+dy-sz/2;
				da.DrawArc (gc, true, (int)x, (int)y, sz, sz, 0, 360 * 64);
			}


		
			return;
		}
		public double[,] project1020(double[,] pts3d,int numpts,out double headcirc)
		{
			// Clarke far-side general prospective azumuthal projection
			//	r = -2.4;
			//	R = sqrt(sum(pts.^ 2, 2));
			//	x = r * R.* (pts(:, 1)./ abs(pts(:, 3) - r * R));
			//	y = r * R.* (pts(:, 2)./ abs(pts(:, 3) - r * R));

			double[,] pts2d = new double[numpts, 2];

			double r = -2.4f;
			double Rall=0;;
			for (int i = 0; i < numpts; i++)
			{
				double R = Math.Sqrt(pts3d[i, 0] * pts3d[i, 0] + pts3d[i, 1] * pts3d[i, 1] +
									 pts3d[i, 2] * pts3d[i, 2]);
				Rall += R;
				double x = r * R * (pts3d[i,0] / Math.Abs(pts3d[i,2] - r * R));
				double y = r * R * (pts3d[i,1] / Math.Abs(pts3d[i,2] - r * R));
				pts2d[i,0] = x;
				pts2d[i, 1] = y;
			}

			headcirc = Rall / numpts;

			return pts2d;
		}


		public void drawsdg1020(Gdk.Drawable da)
		{
			if (!this.isregistered){
				drawsdg(da);
				return;
			}

			double headcirc = 0;
			double[,] lsrcpos = project1020(this.srcpos3D, this.numsrc,out headcirc);
			double[,] ldetpos = project1020(this.detpos3D, this.numdet,out headcirc);

			int width, height;
			da.GetSize(out width, out height);

			double dx, dy;
			dx = 10;
			dy = 10;
			width = width - 20;
			height = height - 20;

			double maxX = headcirc*1.2;
			double minX = -1*headcirc* 1.2;
			double maxY = headcirc* 1.2;
			double minY = -1*headcirc* 1.2;
		
			double rangeX = maxX - minX;
			double rangeY = maxY - minY;

			Gdk.GC gc = new Gdk.GC(da);

			gc.RgbBgColor = new Gdk.Color(0, 0, 0);
			gc.RgbFgColor = new Gdk.Color(0, 0, 0);
			Rectangle rarea = new Rectangle();
			rarea.Height = height + 20;
			rarea.Width = width + 20;
			da.DrawRectangle(gc, true, rarea);

			gc.RgbBgColor = new Gdk.Color(0, 0, 0);
			gc.RgbFgColor = new Gdk.Color(255, 255, 255);
			rarea = new Rectangle();
			rarea.Height = height + 18;
			rarea.Width = width + 18;
			rarea.X = 1;
			rarea.Y = 1;

			da.DrawRectangle(gc, true, rarea);

			int sz = 10;


			gc.RgbFgColor = new Gdk.Color(0, 0, 0);
			gc.SetLineAttributes(3, LineStyle.Solid, CapStyle.Projecting, JoinStyle.Round);
			for (int i = 0; i < this.nummeas; i++)
			{
				if (this.measlistAct[i])
				{
					gc.RgbFgColor = colormap[i]; //new Gdk.Color (0, 0, 0);
				}
				else {
					gc.RgbFgColor = new Gdk.Color(230, 230, 230);
				}
				int si = this.measlist[i, 0];
				int di = this.measlist[i, 1];

				double x1 = (ldetpos[di, 0] - minX) / rangeX * width + dx;
				double y1 = (ldetpos[di, 1] - minY) / rangeY * height + dy;
				double x2 = (lsrcpos[si, 0] - minX) / rangeX * width + dx;
				double y2 = (lsrcpos[si, 1] - minY) / rangeY * height + dy;
				da.DrawLine(gc, (int)x1, (int)y1, (int)x2, (int)y2);
				//pts[cnt]=new Gdk.Point((int)x,(int)y);

			}


			gc.RgbFgColor = new Gdk.Color(0, 255, 0);
			gc.SetLineAttributes(3, LineStyle.Solid, CapStyle.Round, JoinStyle.Round);
			//	Gdk.Point[] pts = new Gdk.Point[this.numdet+this.numsrc];
			for (int i = 0; i < this.numdet; i++)
			{
				double x = (ldetpos[i, 0] - minX) / rangeX * width + dx - sz / 2;
				double y = (ldetpos[i, 1] - minY) / rangeY * height + dy - sz / 2;
				da.DrawArc(gc, true, (int)x, (int)y, sz, sz, 0, 360 * 64);
				//pts[cnt]=new Gdk.Point((int)x,(int)y);
			}

			gc.RgbBgColor = new Gdk.Color(0, 255, 0);
			gc.RgbFgColor = new Gdk.Color(255, 0, 0);
			gc.SetLineAttributes(3, LineStyle.Solid, CapStyle.Round, JoinStyle.Round);
			for (int i = 0; i < this.numsrc; i++)
			{
				double x = (lsrcpos[i, 0] - minX) / rangeX * width + dx - sz / 2;
				double y = (lsrcpos[i, 1] - minY) / rangeY * height + dy - sz / 2;
				da.DrawArc(gc, true, (int)x, (int)y, sz, sz, 0, 360 * 64);
			}

			gc.RgbBgColor = new Gdk.Color(0, 0, 0);
			gc.RgbFgColor = new Gdk.Color(0, 0, 0);
			double xx = (0) / rangeX * width + dx;
			double yy = (0) / rangeY * height + dy;
			da.DrawArc(gc, false, (int)xx, (int)yy, (int)(width), (int)(height), 0, 360 * 64); 
		}


		public void updateML (int DAx, int DAy,bool reset,int DAwidth,int DAheight){
		// This function is called whrn the user clicks on the SDG and used to update the MeasurementList



			int sz = 10;
			float dx, dy;
			dx = 10;
			dy = 10;
			DAwidth = DAwidth -20;
			DAheight = DAheight -20;

			float maxX, minX, maxY, minY;
			maxX = -999; maxY = -999;
			minX = 999; minY = 999;
			for (int i = 0; i < this.numdet; i++) {
				if (maxX < this.detpos [i, 0])
					maxX = this.detpos [i, 0];
				if (maxY < this.detpos [i, 1])
					maxY = this.detpos [i, 1];
				if (minX > this.detpos [i, 0])
					minX = this.detpos [i, 0];
				if (minY > this.detpos [i, 1])
					minY = this.detpos [i, 1];
			}
			for (int i = 0; i < this.numsrc; i++) {

				if (maxX < this.srcpos [i, 0])
					maxX = this.srcpos [i, 0];
				if (maxY < this.srcpos [i, 1])
					maxY = this.srcpos [i, 1];
				if (minX > this.srcpos [i, 0])
					minX = this.srcpos [i, 0];
				if (minY > this.srcpos [i, 1])
					minY = this.srcpos [i, 1];
			}
			float rangeX = maxX - minX;
			float rangeY = maxY - minY;
		
			float distance;
			float cutoff = 5;
			float cutoff2 = 2;


			if (reset)
			{
				for (int j = 0; j < this.nummeas; j++)
				{
					this.measlistAct[j] = false;
				}
			}

			// Do lines
			for (int i = 0; i < this.nummeas; i++) {
				int si = this.measlist [i, 0];
				int di = this.measlist [i, 1];

				float x1 = (this.detpos[di,0]-minX)/rangeX*DAwidth+dx;
				float y1 = (this.detpos[di,1]-minY)/rangeY*DAheight+dy;
				float x2 = (this.srcpos[si,0]-minX)/rangeX*DAwidth+dx;
				float y2 = (this.srcpos[si,1]-minY)/rangeY*DAheight+dy;

				float dist2S, dist2D, distSD;
				distSD = (float)Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
				dist2D = (float)Math.Sqrt((DAx - x1) * (DAx - x1) + (DAy - y1) * (DAy - y1));
				dist2S = (float)Math.Sqrt((DAx - x2) * (DAx - x2) + (DAy - y2) * (DAy - y2));

				if (dist2D + dist2S - distSD < cutoff2) {

					this.measlistAct [i] = !this.measlistAct [i];
				}


			}

			for (int i = 0; i < this.numdet; i++) {
				float x = (this.detpos [i, 0] - minX) / rangeX * DAwidth + dx - sz / 2;
				float y = (this.detpos [i, 1] - minY) / rangeY * DAheight + dy - sz / 2;
				distance = (DAx - x) * (DAx - x) + (DAy - y) * (DAy - y);

				if (distance < cutoff * cutoff) {
					// Selected this detector
					if (reset) {
						for (int j = 0; j < this.nummeas; j++) {
							this.measlistAct [j] = false;
						}
					}
					for (int j = 0; j < this.nummeas; j++) {
						if (this.measlist [j, 1] == i) {
							this.measlistAct [j] = true;
						}
					}
				}

			}
			for (int i = 0; i < this.numsrc; i++) {
					float x = (this.srcpos[i,0]-minX)/rangeX*DAwidth+dx-sz/2;
					float y = (this.srcpos[i,1]-minY)/rangeY*DAheight+dy-sz/2;
				distance  = (DAx - x) * (DAx - x) + (DAy - y) * (DAy - y);

				if (distance < cutoff * cutoff) {
					if (reset) {
						for (int j = 0; j < this.nummeas; j++) {
							this.measlistAct [j] = false;
						}
					}
					// Selected this detector
					for (int j = 0; j < this.nummeas; j++) {
						if (this.measlist [j, 0] == i) {
							this.measlistAct [j] = true;
						}
					}
				}

			}




			return;

		}

	}
}

