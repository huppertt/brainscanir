
// This file has been generated by the GUI designer. Do not modify.
namespace BrainScanIR
{
	public partial class GLview
	{
		private global::Gtk.DrawingArea drawingarea1;

		protected virtual void Build()
		{
			global::Stetic.Gui.Initialize(this);
			// Widget BrainScanIR.GLview
			global::Stetic.BinContainer.Attach(this);
			this.WidthRequest = 300;
			this.HeightRequest = 350;
			this.Name = "BrainScanIR.GLview";
			// Container child BrainScanIR.GLview.Gtk.Container+ContainerChild
			this.drawingarea1 = new global::Gtk.DrawingArea();
			this.drawingarea1.Name = "drawingarea1";
			this.Add(this.drawingarea1);
			if ((this.Child != null))
			{
				this.Child.ShowAll();
			}
			this.Hide();
		}
	}
}
