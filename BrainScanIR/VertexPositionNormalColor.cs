﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace BrainScanIR
{
	public struct VertexPositionNormalColor : IVertexType
	{
		public Vector3 Position;
        public OpenTK.Graphics.Color4 Color;
		public Vector3 Normal;

		public readonly static VertexDeclaration VertexDeclaration
			= new VertexDeclaration(
				new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
				new VertexElement(sizeof(float) * 3, VertexElementFormat.Color, VertexElementUsage.Color, 0),
				new VertexElement(sizeof(float) * 3 + 4, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0)
				);

		public VertexPositionNormalColor(Vector3 pos, OpenTK.Graphics.Color4 c, Vector3 n)
		{
			Position = pos;
			Color = c;
			Normal = n;
		}
		public VertexPositionNormalColor(Vector3 pos, OpenTK.Graphics.Color4 c)
		{
			Position = pos;
			Color = c;
			Normal = new Vector3();
		}


		VertexDeclaration IVertexType.VertexDeclaration
		{
			get { return VertexDeclaration; }
		}
	}
}
