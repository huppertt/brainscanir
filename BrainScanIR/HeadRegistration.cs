﻿using System;
using Gtk;
namespace BrainScanIR
{
    public partial class HeadRegistration : Gtk.Window
    {
        Cprobe probe;
        public HeadRegistration(Cprobe SDG) :
                base(Gtk.WindowType.Toplevel)
        {
            this.Build();
            this.Title = "Probe Registration";
            probe = SDG;

            this.drawingarea1.ExposeEvent += drawprobe;

            drawprobe(null, null);

            this.combobox3.Clear();
           
			CellRendererText cell = new CellRendererText();
			this.combobox3.PackStart(cell, false);
			this.combobox3.AddAttribute(cell, "text", 0);
			ListStore store = new ListStore(typeof(string));
			this.combobox3.Model = store;
            for (int i = 0; i < probe.nummeas; i++){
                this.combobox3.AppendText(String.Format("Src{0} - Det{1}", probe.measlist[i, 0]+1, probe.measlist[i, 1]+1));
			}
            this.combobox3.Active = 0;
            this.colorbutton1.Color = probe.colormap[this.combobox3.Active];

            if (MainClass.SystemControler.SystemInfo.loadheadmodel)
            {
                this.DeleteEvent += stopev;
                this.glview3.startdrawGL();
				this.show1020.Active = MainClass.AppWindow.show1020;
				this.showskin.Active = MainClass.AppWindow.showkin;
				MainClass.AppWindow.showprobe = true;
            }else{
                this.glview3.Destroy();
                
                this.showskin.Destroy();
                this.show1020.Destroy();
                //this.frame3.Destroy();
            }


            this.ShowAll();
          
        }

        protected void stopev(object o, Gtk.DeleteEventArgs e)
		{
            this.glview3.stopdrawGL();
        }

        protected void drawprobe(object sender, EventArgs e)
		{
            if (this.combobox1.Active ==1 & probe.isregistered)
                probe.drawsdg1020(this.drawingarea1.GdkWindow);
            else
                probe.drawsdg(this.drawingarea1.GdkWindow);
                    
            this.drawingarea1.QueueDraw();

        }

        protected void Recolorprobe(object sender, EventArgs e)
        {
            probe.colormap[this.combobox3.Active] = this.colorbutton1.Color;
            MainClass.SystemControler.SubjectData.SDGprobe.colormap[this.combobox3.Active] = this.colorbutton1.Color;
                    this.drawingarea1.QueueDraw();
            MainClass.AppWindow.handles.SDGwin.QueueDraw();


        }

        protected void updatecolor(object sender, EventArgs e)
        {
           this.colorbutton1.Color = probe.colormap[this.combobox3.Active];
            this.ShowAll();
        }

        protected void setshowskin(object sender, EventArgs e)
        {
            MainClass.AppWindow.showkin = this.showskin.Active;
        }

        protected void setshow1020(object sender, EventArgs e)
        {
            MainClass.AppWindow.show1020 = this.show1020.Active;
        }

        protected void ResizeRequested(object o, SizeRequestedArgs args)
        {
        }

        protected override bool OnDestroyEvent(Gdk.Event evnt)
        {
            if(this.glview3!=null)
                this.glview3.stopdrawGL();

            return base.OnDestroyEvent(evnt);
        }
       

    }
}
