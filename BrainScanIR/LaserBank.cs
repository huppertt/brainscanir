﻿using System;
using Gtk; 
namespace PhotonViewer
{
	[System.ComponentModel.ToolboxItem (true)]
	public partial class LaserBank : Gtk.Bin
	{
		public Gtk.ToggleButton[] lasertogglehandles;
		public Gtk.SpinButton[] laserspinhandles;

		public LaserBank ()
		{
			this.Build ();
			laserspinhandles = new Gtk.SpinButton[8];
			lasertogglehandles = new Gtk.ToggleButton[8];

			Gdk.Color c = new Gdk.Color (200, 100, 100);

			toggle1.ModifyBg (StateType.Active,c);
			toggle2.ModifyBg (StateType.Active,c);
			toggle3.ModifyBg (StateType.Active,c);
			toggle4.ModifyBg (StateType.Active,c);
			toggle5.ModifyBg (StateType.Active,c);
			toggle6.ModifyBg (StateType.Active,c);
			toggle7.ModifyBg (StateType.Active,c);
			toggle8.ModifyBg (StateType.Active,c);

			laserspinhandles [0] = spinbutton1;
			laserspinhandles [1] = spinbutton2;
			laserspinhandles [2] = spinbutton3;
			laserspinhandles [3] = spinbutton4;
			laserspinhandles [4] = spinbutton5;
			laserspinhandles [5] = spinbutton6;
			laserspinhandles [6] = spinbutton7;
			laserspinhandles [7] = spinbutton8;

			lasertogglehandles [0] = toggle1;
			lasertogglehandles [1] = toggle2;
			lasertogglehandles [2] = toggle3;
			lasertogglehandles [3] = toggle4;
			lasertogglehandles [4] = toggle5;
			lasertogglehandles [5] = toggle6;
			lasertogglehandles [6] = toggle7;
			lasertogglehandles [7] = toggle8;


		}

		protected void LaserButtonClicked (object sender, EventArgs e)
		{
			Gtk.ToggleButton thisbtn = (Gtk.ToggleButton)sender;
			int index = Int16.Parse (thisbtn.Name);
			PhotonViewer.AppWindow.SystemController.systemInfo.SetLaser (index, thisbtn.Active);

			if (thisbtn.Active) {
				PhotonViewer.AppWindow.SystemController.allonbuttonhandle.State = StateType.Active;
				PhotonViewer.AppWindow.SystemController.allonbuttonhandle.Label = "All Lasers Off";
			}


		}

		protected void LaserIntenChanged (object sender, EventArgs e)
		{
			Gtk.SpinButton thisbtn = (Gtk.SpinButton)sender;
			int index = Int16.Parse (thisbtn.Name);
			int value = thisbtn.ValueAsInt;
			PhotonViewer.AppWindow.SystemController.systemInfo.SetLaserInten (index, value);
		}

	
	}
}

