﻿using csmatio.io;
using csmatio.types;
using System.Collections.Generic;

// Class for holding subject data including probe, data, and demographics
namespace BrainScanIR
{
    public class Csubject
	{
		public Cprobe SDGprobe { get; set; }
		public string outfolder;
		public string fileroot;
		public int scanIdx { get; set; }

		public class demographics{
			public string subjID { get; set; }
			//public enum gender { get; set; }
			//public int age { get; set; }
		}
		public CNIRSdata[] data;

		public Csubject ()
		{
			SDGprobe = new Cprobe ();

			data = new CNIRSdata[2];

			data[0] = new CNIRSdata (SDGprobe);
			data [0].type = "Raw";
			data[1] = new CNIRSdata (SDGprobe);
			data [1].type = "delta-OD";

			scanIdx = 1;
		}
			
		public void newscan(){
			// Initialize data for a new scan

			int cnt = 4;  //always prepare for the max to allow the user to toggle the options on during runtime  

//            if(MainClass.AppWindow.handles.rtpanel.useMOCO())
//               cnt++;
//			if(MainClass.AppWindow.handles.rtpanel.useMBLL())
//				cnt++;

			data = new CNIRSdata[cnt];

			for (int i=0; i<cnt; i++){
				data[i] = new CNIRSdata (SDGprobe);
				data[i].type = "Raw";
				if (i > 0)
					data[i].numaux=0;
			}
				
		}




		public void save2matlab(string filename){

			// Store the data into the *.nirs matlab format

			int numsamples = data[0].samples+1;
			int numch = data[0].nummeas;

			// save the structure as mat file using MatFileWriter
			List<MLArray> mlList = new List<MLArray>(); 

			double[][] d = new double[numsamples][];
			double[][] aux = new double[numsamples][];
			double[][] t = new double[numsamples][];

			for (int j = 0; j < numsamples; j++) {
				double[] dloc = new double[numch];
				double[] auxloc = new double[data[0].numaux];
				for (int i = 0; i < data[0].numaux; i++) {
					auxloc[i] = data[0].aux[i, j];
				}
				for (int i = 0; i < numch; i++)
				{
					dloc[i] = data[0].data[i, j];
				}
				double[] tt = new double[1];
				tt[0] = data[0].time[j]/1000;
				t[j] = tt;
				d [j] = dloc;
				aux[j] = auxloc;

			}

			MLDouble mldata = new MLDouble("d", d);
			mlList.Add(mldata);
			MLDouble mlaux = new MLDouble("aux", aux);
			mlList.Add(mlaux);
			MLDouble mltime = new MLDouble("t",t);
			mlList.Add(mltime); 

			// Probe 
			MLStructure mlSD = new MLStructure("SD", new int[] { 1, 1 });

            double[] lambda = (double[])MainClass.SystemControler.SystemInfo.wavelengths.Clone();

			double[][] srcpos = new double[SDGprobe.numsrc][];
			for (int j = 0; j < SDGprobe.numsrc; j++) {
				double[] slo = new double[3];
				for (int i = 0; i < 2; i++) {
					slo [i] = SDGprobe.srcpos [j,i];
				}
				slo[2] = 0;
				srcpos [j] = slo;
			}


			double[][] detpos = new double[SDGprobe.numdet][];
			for (int j = 0; j < SDGprobe.numdet; j++) {
				double[] dlo = new double[3];
				for (int i = 0; i < 2; i++) {
					dlo [i] = SDGprobe.detpos [j, i];
				}
				dlo[2] = 0;
				detpos [j] = dlo;
			}

			mlSD["NumDet",0] = new MLDouble("",new double[] { SDGprobe.numdet },1);
			mlSD["NumSrc",0] = new MLDouble("",new double[] {SDGprobe.numsrc},1);
			mlSD["Lambda",0] = new MLDouble("", lambda,1);  
			mlSD["SrcPos",0] = new MLDouble("", srcpos);
			mlSD["DetPos",0] = new MLDouble("", detpos);

			mlList.Add(mlSD); 


			double[][] ml = new double[SDGprobe.nummeas * SDGprobe.numlambda][];
			for (int i = 0; i < SDGprobe.nummeas * SDGprobe.numlambda; i++)
			{
				double[] m = new double[4];
				m[0] = SDGprobe.measlist[i, 0]+1;
				m[1] = SDGprobe.measlist[i, 1]+1;
				m[2] = 0;
				m[3] = SDGprobe.measlist[i, 2]+1;
				ml[i] = m;
			}
			MLDouble mlml = new MLDouble("ml", ml);
			mlList.Add(mlml);

			MLStructure mlStim = new MLStructure("StimDesign", new int[] {this.data[0].StimInfo.numconditions, 1 });
			for (int i = 0; i < this.data[0].StimInfo.numconditions; i++)
			{
				mlStim["name", i] = new MLChar("",this.data[0].StimInfo.conditions[i]);
				stim localstim = this.data[0].StimInfo.GetEvents(i);
				double[] onset = localstim.onset.ToArray();
				double[] dur = localstim.duration.ToArray();
				double[] amp = localstim.amplitude.ToArray();
				mlStim["onset", i] = new MLDouble("", onset,1);
				mlStim["dur", i] = new MLDouble("", dur,1);
				mlStim["amp", i] = new MLDouble("", amp,1);
			}
			mlList.Add(mlStim);

			new MatFileWriter(filename, mlList, false);

		}

	}
}

