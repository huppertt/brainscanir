﻿
using System;
using Gtk;
using System.Threading;
using System.Xml;
using System.Linq;
using System.IO;

 /*
 * AppWindow Class
 * 		- This is the main GUI interface
 * 		- SystemControlClass - structure above
 * 		- headmodel - GIFTI format of head/brain model from Colin27 atlas
 * 		- c1020 - 10-20 labels for Colin27 atlas
 * */

/*
namespace PhotonViewer
{

	public class SystemControlClass
	{
		// Method to hold all the objects for the control software
		public Csystem systemInfo;    // class to hold info on instrument
		public Csubject subjectData;  // Class to hold subject and data info
		public DrawingArea SDGwin;  // reference to SDG window (used by call to RegisterSubject on close)

		// handles to widgets so I can toggle the visibility from function calls
		public ToggleButton[] laserhandles;
		public SpinButton[] laserspinhandles;
		public VScale[] detectorhandles;
		public Button allonbuttonhandle;
		public Button startbuttonhandle;
		public ComboBox whichdisphandle;

		public bool laseralltogglestate;   // remembers state of all on/off since there are multiple referneces of this button

		public string[] scantypes;   // These hold the types of scans and stim-types associated with
		public string[][] stimtypes; // each type of scan.   
		public double[][] durtypes;  // this holds the default duration (in seconds) for stim-types

		// Constructor
		public SystemControlClass ()
		{
			
			laseralltogglestate = false;
			systemInfo = new Csystem();     // The constructor for this reads the XML file and sets the instrument specs
			subjectData = new Csubject();   // Initialize the subject class
		}


		public void ReadXMLStims(string file)
		{
			// This function reads the default stim information from an XML file
			// This is called after the probe is loaded via the register subject menu
			XmlDocument doc = new XmlDocument();
			XmlDocument doc2 = new XmlDocument();
			XmlDocument doc3 = new XmlDocument();

			doc.Load(file); // opens the XML stream
			XmlNodeList elemList, elemListsub, elemListsubsub;

			// Scan types and stimulus information
			elemList = doc.GetElementsByTagName("scan");

			// Add 4 default types (setup, resting, --, --) to allow real-time editing
			scantypes = new string[elemList.Count+4];
			stimtypes = new string[elemList.Count+4][];
			durtypes = new double[elemList.Count+4][];

			// First entry will be the "setup" 
			string[] stims = new string[4];
			double[] dur = new double[4];
			stims[0] = "Mark #1";  stims[1] = "Mark #2"; stims[2] = "Mark #3"; stims[3] = "Mark #4";
			dur[0] = 1; dur[1] = 1; dur[2] = 1; dur[3] = 1;
			scantypes[0] = "Setup";
			stimtypes[0] = stims;
			durtypes[0] = dur;

			// Now load all the scan types from the XML file
			for (int i = 0; i < elemList.Count; i++)
			{
				doc2 = new XmlDocument();
				doc2.LoadXml("<root>" + elemList[i].InnerXml + "</root>");
				elemListsub = doc2.GetElementsByTagName("name");
				scantypes[i+1] = elemListsub[0].InnerXml;

				elemListsub = doc2.GetElementsByTagName("condition");
				stims = new string[elemListsub.Count];
				dur = new double[elemListsub.Count];
				for (int j = 0; j < elemListsub.Count; j++)
				{
					doc3 = new XmlDocument();
					doc3.LoadXml("<root>" + elemListsub[j].InnerXml + "</root>");
					elemListsubsub = doc3.GetElementsByTagName("name");
					stims[j] = elemListsubsub[0].InnerXml;
					elemListsubsub = doc3.GetElementsByTagName("name");
					stims[j] = elemListsubsub[0].InnerXml;
					elemListsubsub = doc3.GetElementsByTagName("duration");
					dur[j] = Convert.ToDouble(elemListsubsub[0].InnerXml);

					//TODO add trigger types from XML file for stim detection off aux channels

				}
				stimtypes[i+1] = stims;
				durtypes[i+1] = dur;
			}


			// Add "Resting" condition
			stims = new string[4];
			dur = new double[4];
			stims[0] = "Mark #1"; stims[1] = "Mark #2"; stims[2] = "Mark #3"; stims[3] = "Mark #4";
			dur[0] = 1; dur[1] = 1; dur[2] = 1; dur[3] = 1;
			scantypes[elemList.Count + 1] = "Resting";
			stimtypes[elemList.Count+1] = stims;
			durtypes[elemList.Count+1] = dur;


			// Add blank (editable) condition #1
			stims = new string[4];
			dur = new double[4];
			stims[0] = "Mark #1"; stims[1] = "Mark #2"; stims[2] = "Mark #3"; stims[3] = "Mark #4";
			dur[0] = 1; dur[1] = 1; dur[2] = 1; dur[3] = 1;
			scantypes[elemList.Count + 2] = " ----  ";
			stimtypes[elemList.Count + 2] = stims;
			durtypes[elemList.Count + 2] = dur;


			// Add blank (editable) condition #2
			stims = new string[4];
			dur = new double[4];
			stims[0] = "Mark #1"; stims[1] = "Mark #2"; stims[2] = "Mark #3"; stims[3] = "Mark #4";
			dur[0] = 1; dur[1] = 1; dur[2] = 1; dur[3] = 1;
			scantypes[elemList.Count + 3] = " ----  ";
			stimtypes[elemList.Count + 3] = stims;
			durtypes[elemList.Count + 3] = dur;

			return; // End of ReadXMLStims
		}	

	} // End of SystemControlClass definition

//-----------------------------------------------------------------------
	public partial class AppWindow : Gtk.Window
	{
		
		// Load all the system control info from config.xml
		public static SystemControlClass SystemController;   // Defined in lines 10-22 above
		public static Thread maindisplaythread;  // Timing thread to handle drawing during running aquistion

		// handles to the control widgets
		public static DebugPanel paneldb; 
		public static RealtimePanel rtpanel;
		public static ComboBox combobox_wh;
		public static Statusbar statusbardevice;
		public static CheckButton showstim;
		public static CheckButton showlegend;
		public static CheckButton winddata;
		public static Entry windatatime;
		public static ListStore StimStore;
		public static CRegressionStats stat;
		public static TreeIter[] StimTreeIter;
		public static TreeIter[] FileTreeIter;
		public static ComboBoxEntry scantypes;
		public static ComboBoxEntry stimtypes;

		// These hold the head/brain and 10-20 labels for the Colin27 atlas.
		public static Gifti[] headmodel;
		public static Gifti sphereGifti;
		public static TenTwenty c1020;
		public static MathNet.Numerics.LinearAlgebra.Matrix<double> Smoother;

		public AppWindow() :
			base(Gtk.WindowType.Toplevel)
		{
			this.Build();
			this.Hide();

			SystemController = new SystemControlClass();   // Initialize the control class.  
														   // This gets the instrument info from the XML file
			SystemController.SDGwin = this.drawingareaSDG; // Create a reference to the drawing window to invoke in the RegisterSubject code

			// dynamically modifiy the win to match config.xml
			// Update the GUI to match the settings in the config.xml file
			Notebook nb = new Notebook();
			nb.TabPos = PositionType.Left;
			nb.SetSizeRequest(this.fixedDetpanel.WidthRequest, this.fixedDetpanel.HeightRequest);

			// Detector PANEL page
			// Add the detector panels to the GUI based on the MAXDET number from the XML file.
			this.fixedDetpanel.HeightRequest = this.controlnotebook.HeightRequest;
			int numpanels = SystemController.systemInfo.maxdetectors;
			numpanels = (numpanels - 1) / 8;
			SystemController.detectorhandles = new Gtk.VScale[SystemController.systemInfo.maxdetectors];
			int cnt = 0;
			for (int i = 0; i < numpanels + 1; i++)
			{
				string label = String.Format("{0}-{1}", 8 * (i) + 1, 8 * (i + 1));
				PhotonViewer.DetectorPanel panel = new PhotonViewer.DetectorPanel(i, SystemController);
				panel.HeightRequest = this.fixedDetpanel.HeightRequest;
				nb.AppendPage(panel, new Label(label));
				panel.ResizeChildren();
				for (int j = 0; j < 8; j++)
				{
					if (cnt < SystemController.systemInfo.maxdetectors)
					{
						SystemController.detectorhandles[cnt] = panel.dethandle[j];
						SystemController.detectorhandles[cnt].Sensitive = false;
						cnt++;
					}
					else {
						panel.dethandle[j].Destroy();
						// TODO - destroy the labels and color LEDs
					}
				}
			}
			this.fixedDetpanel.Add(nb);

			// Set the initial tab to the "Detectors" page
			this.controlnotebook.CurrentPage = 0;
			// end of Detector Panel definitions 



			// Add the source panel  
			this.fixedsrcs.HeightRequest = this.controlnotebook.HeightRequest;

			SystemController.laserhandles = new Gtk.ToggleButton[SystemController.systemInfo.maxsources];
			SystemController.laserspinhandles = new Gtk.SpinButton[SystemController.systemInfo.maxsources];

			LaserPanel panellas = new LaserPanel();
			this.fixedsrcs.Add(panellas);
			cnt = 0;
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					if (cnt < SystemController.systemInfo.maxsources)
					{
						string thisname = String.Format("Laser {0}", cnt + 1);
						int k;
						k = cnt;
						SystemController.laserspinhandles[k] = panellas.laserbankhandles[i].laserspinhandles[j];
						SystemController.laserhandles[k] = panellas.laserbankhandles[i].lasertogglehandles[j];
						SystemController.laserhandles[k].Label = thisname;
						SystemController.laserhandles[k].Name = Convert.ToString(cnt);
						SystemController.laserspinhandles[k].Name = Convert.ToString(cnt);

						SystemController.laserspinhandles[k].Sensitive = false;
						SystemController.laserhandles[k].Sensitive = false;
						if (!SystemController.systemInfo.laser_adjustable)
						{
							SystemController.laserspinhandles[k].Destroy();
						}

						cnt++;
					}
					else {
						panellas.laserbankhandles[i].laserspinhandles[j].Destroy();
						panellas.laserbankhandles[i].lasertogglehandles[j].Destroy();
					}
				}
			}

			rtpanel = new RealtimePanel();
			this.fixedrtpanel.Add(rtpanel);
			if (MainClass.SYSTEM != 2)
			{
				rtpanel.wirelesscontrols.Destroy();
			}



			// Turn off all the buttons until you register a subject/probe
			AllLasers.Sensitive = false;
			StartStop.Sensitive = false;
			combobox_whichdisplay.Sensitive = false;



			SystemController.startbuttonhandle = StartStop;
			SystemController.whichdisphandle = combobox_whichdisplay;
			SystemController.allonbuttonhandle = AllLasers;

			// If in debug mode add a tab to show messages
			paneldb = new DebugPanel();
			if (MainClass.DEBUG)
			{
				Gtk.Label label = new Gtk.Label();
				label.Text = "Debug Info";
				this.controlnotebook.AppendPage(paneldb, label);
			}

			combobox_wh = this.combobox_whichdisplay;

			AppWindow.combobox_wh.Clear();

			CellRendererText cell = new CellRendererText();
			AppWindow.combobox_wh.PackStart(cell, false);
			AppWindow.combobox_wh.AddAttribute(cell, "text", 0);
			ListStore store = new ListStore(typeof(string));
			AppWindow.combobox_wh.Model = store;

			for (int i = 0; i < AppWindow.SystemController.systemInfo.numwavelengths; i++)
			{
				AppWindow.combobox_wh.AppendText(String.Format("Raw {0}nm", AppWindow.SystemController.systemInfo.wavelengths[i]));
			}
			for (int i = 0; i < AppWindow.SystemController.systemInfo.numwavelengths; i++)
			{
				AppWindow.combobox_wh.AppendText(String.Format("dOD {0}nm", AppWindow.SystemController.systemInfo.wavelengths[i]));
			}
			if (rtpanel.useMBLL())
			{
				AppWindow.combobox_wh.AppendText("Oxy-Hemoglobin");
				AppWindow.combobox_wh.AppendText("Deoxy-Hemoglobin");
			}
			AppWindow.combobox_wh.Active = 0;

			// Add the event handlers to control clicking on the SDG window
			this.drawingareaSDG.ExposeEvent += sdgdraw;
			this.drawingareaMainData.ExposeEvent += datadraw;
			this.drawingareaSDG.AddEvents((int)Gdk.EventMask.ButtonPressMask);
			this.drawingareaSDG.AddEvents((int)Gdk.EventMask.ButtonReleaseMask);

			// Set up the treeview to allow edits of collected data files
			this.treeviewfiles.HeadersVisible = true;
			this.treeviewfiles.EnableGridLines = Gtk.TreeViewGridLines.Both;


			TreeViewColumn col = new TreeViewColumn();
			CellRendererText colt = new CellRendererText();
			col.Title = "Scan Number";
			col.PackStart(colt, true);
			col.AddAttribute(colt, "text", 0);
			this.treeviewfiles.AppendColumn(col);

			col = new TreeViewColumn();
			CellRendererToggle colb = new CellRendererToggle();
			col.Title = "Include";
			col.PackStart(colb, true);
			colb.Activatable = true;
			colb.Active = true;
			col.AddAttribute(colb, "include", 1);
			this.treeviewfiles.AppendColumn(col);


			col = new TreeViewColumn();
			CellRendererCombo colc = new CellRendererCombo();
			col.Title = "Type";
			col.PackStart(colc, true);
			colc.TextColumn = 0;
			colc.Model = new Gtk.ListStore(typeof(string));
			colc.HasEntry = true;
			colc.Editable = true;


			// TODO
			Gtk.ComboBox cbox = new Gtk.ComboBox();
			cbox.Model = new Gtk.ListStore(typeof(string));
			for (int j = 0; j < 4; j++)
			{
				cbox.AppendText(String.Format("Option-{0}", j));
			}
			colc.Model = cbox.Model;

			this.treeviewfiles.AppendColumn(col);

			ListStore store2 = new ListStore(typeof(string), typeof(Gtk.ToggleButton), typeof(Gtk.ComboBox));
			this.treeviewfiles.Model = store2;

			FileTreeIter = new TreeIter[0];

			// Stimulus table
			//this.treeviewStim;
			this.treeviewStim.HeadersVisible = true;
			this.treeviewStim.EnableGridLines = Gtk.TreeViewGridLines.Both;

			TreeViewColumn[] col2 = new TreeViewColumn[4];
			CellRendererText[] col2t = new CellRendererText[4];
			col2t[0] = new CellRendererText();
			col2[0] = new TreeViewColumn();
			col2t[0].Editable = true;
			col2[0].Title = "Condition";
			col2[0].PackStart(col2t[0], true);
			col2[0].AddAttribute(col2t[0], "text", 0);
			this.treeviewStim.AppendColumn(col2[0]);

			col2t[1] = new CellRendererText();
			col2[1] = new TreeViewColumn();
			col2t[1].Editable = true;
			col2[1].Title = "Onset";
			col2[1].PackStart(col2t[1], true);
			col2[1].AddAttribute(col2t[1], "text", 1);
			col2t[1].Edited += ChangedStimOnset;

			this.treeviewStim.AppendColumn(col2[1]);

			col2t[2] = new CellRendererText();
			col2[2] = new TreeViewColumn();
			col2t[2].Editable = true;
			col2[2].Title = "Duration";
			col2[2].PackStart(col2t[2], true);
			col2[2].AddAttribute(col2t[2], "text", 2);
			col2t[2].Edited += ChangedStimDuration;

			this.treeviewStim.AppendColumn(col2[2]);

			col2t[3] = new CellRendererText();
			col2[3] = new TreeViewColumn();
			col2t[3].Editable = true;
			col2[3].Title = "Amplitude";
			col2[3].PackStart(col2t[3], true);
			col2[3].AddAttribute(col2t[3], "text", 3);
			col2t[3].Edited += ChangedStimAmp;
			this.treeviewStim.AppendColumn(col2[3]);

			StimStore = new ListStore(typeof(string), typeof(string), typeof(string), typeof(string));
			this.treeviewStim.Model = StimStore;
			StimTreeIter = new TreeIter[0];

			statusbardevice = this.statusbar_connected;
			showstim = this.checkbuttonShowStim;
			showlegend = this.checkbuttonShowStimLegend;
			winddata = this.checkbuttonwindata;
			windatatime = this.entrywindata;
			scantypes = this.comboboxscantypes;
			stimtypes = this.combobox_stim;

			// Show the the application initialized
			string msg = "Application loaded";
			dispstatus(msg);
			PhotonViewer.AppWindow.paneldb.debugmsg(msg);

			if (!MainClass.DEBUG)
			{
				this.progressbar_buffer.Visible = false;
			}

			if (MainClass.SYSTEM == 0)
			{
				msg = "Connected to: SIMULATOR";
			}
			else if (PhotonViewer.MainClass.SYSTEM == 1)
			{
				msg = "Connected to: CW6";
			}
			else if (PhotonViewer.MainClass.SYSTEM == 2)
			{
				msg = "Connected to: WIRELESS";
			}
			PhotonViewer.AppWindow.paneldb.debugmsg (msg);
		
			headmodel = new Gifti[2];
			headmodel[0] = new Gifti();
			headmodel[0].ReadGifti(@"surf/Skin.gii");
		
			headmodel[1] = new Gifti();
			headmodel[1].ReadGifti(@"surf/Pial.gii");
			sphereGifti = new Gifti();
			sphereGifti.ReadGifti(@"surf/Sphere-ico3.gii");
			c1020 = new TenTwenty(@"surf/pts1020.xml");
			for (int i = 0; i < c1020.points.Length; i++)
			{
				double[,] p = new double[1, 3];
				p[0, 0] = (double)c1020.points[i].X;
				p[0, 1] = (double)c1020.points[i].Y;
				p[0, 2] = (double)c1020.points[i].Z;
				p = AppWindow.headmodel[0].set2surface(p, 1);
				c1020.points[i].X = (float)p[0, 0];
				c1020.points[i].Y = (float)p[0, 1];
				c1020.points[i].Z = (float)p[0, 2];

			}


			double[,] Sm = new double[headmodel[1].nvertices * 2, headmodel[1].nvertices * 2];
			using (BinaryReader b = new BinaryReader(File.Open(@"surf/GaussianSmooth.bin", FileMode.Open)))
			{

				int pos = 0;
				int length = (int)b.BaseStream.Length;
				while (pos < length)
				{
					int i = (int)b.ReadSingle();
					int j = (int)b.ReadSingle();
					float v = b.ReadSingle();
					Sm[j - 1, i - 1] = (double)v;
					Sm[j - 1 + headmodel[1].nvertices, i - 1 + headmodel[1].nvertices] = (double)v;
					// 4.
					// Advance our position variable.
					pos += sizeof(float);
					pos += sizeof(float);
					pos += sizeof(float);
				}
			}
			Smoother = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.SparseOfArray(Sm);



		}


		protected void ChangedStimOnset(object sender, EditedArgs e){
			TreeSelection selection = treeviewStim.Selection;	
			TreeIter iter;

			if (!selection.GetSelected (out iter)) {
				return;
			}
			Gtk.TreeModel store = treeviewStim.Model;

			stim oldev= new stim();
			oldev.onset.Add(Convert.ToDouble(store.GetValue(iter,1)));
			oldev.duration.Add(Convert.ToDouble(store.GetValue(iter,2)));
			oldev.amplitude.Add(Convert.ToDouble(store.GetValue(iter,3)));
			string oldname = (string)store.GetValue (iter, 0);

			store.SetValue (iter, 1, e.NewText);

			stim newev= new stim();
			newev.onset.Add(Convert.ToDouble(store.GetValue(iter,1)));
			newev.duration.Add(Convert.ToDouble(store.GetValue(iter,2)));
			newev.amplitude.Add(Convert.ToDouble(store.GetValue(iter,3)));
			string newname = (string)store.GetValue (iter, 0);
			SystemController.subjectData.data [0].StimInfo.ChangeStimEvents (oldname,oldev,newname, newev);
			this.drawingareaMainData.QueueDraw ();
		}
		protected void ChangedStimDuration(object sender, EditedArgs e){
			TreeSelection selection = treeviewStim.Selection;	
			TreeIter iter;

			if (!selection.GetSelected (out iter)) {
				return;
			}
			Gtk.TreeModel store = treeviewStim.Model;

			stim oldev= new stim();
			oldev.onset.Add(Convert.ToDouble(store.GetValue(iter,1)));
			oldev.duration.Add(Convert.ToDouble(store.GetValue(iter,2)));
			oldev.amplitude.Add(Convert.ToDouble(store.GetValue(iter,3)));
			string oldname = (string)store.GetValue (iter, 0);

			store.SetValue (iter, 2, e.NewText);

			stim newev= new stim();
			newev.onset.Add(Convert.ToDouble(store.GetValue(iter,1)));
			newev.duration.Add(Convert.ToDouble(store.GetValue(iter,2)));
			newev.amplitude.Add(Convert.ToDouble(store.GetValue(iter,3)));
			string newname = (string)store.GetValue (iter, 0);
			SystemController.subjectData.data [0].StimInfo.ChangeStimEvents (oldname,oldev,newname, newev);
			this.drawingareaMainData.QueueDraw ();
		}

		protected void ChangedStimAmp(object sender, EditedArgs e){
			TreeSelection selection = treeviewStim.Selection;	
			TreeIter iter;

			if (!selection.GetSelected (out iter)) {
				return;
			}
			Gtk.TreeModel store = treeviewStim.Model;

			stim oldev= new stim();
			oldev.onset.Add(Convert.ToDouble(store.GetValue(iter,1)));
			oldev.duration.Add(Convert.ToDouble(store.GetValue(iter,2)));
			oldev.amplitude.Add(Convert.ToDouble(store.GetValue(iter,3)));
			string oldname = (string)store.GetValue (iter, 0);

			store.SetValue (iter, 3, e.NewText);

			stim newev= new stim();
			newev.onset.Add(Convert.ToDouble(store.GetValue(iter,1)));
			newev.duration.Add(Convert.ToDouble(store.GetValue(iter,2)));
			newev.amplitude.Add(Convert.ToDouble(store.GetValue(iter,3)));
			string newname = (string)store.GetValue (iter, 0);
			SystemController.subjectData.data [0].StimInfo.ChangeStimEvents (oldname,oldev,newname, newev);
			this.drawingareaMainData.QueueDraw ();
		}

	//-----------------------------------------------------------------------
		public void dispstatus(string msg){
			// Function to handle updates to the status bar in the main GUI
			uint contextID;
			contextID = this.statusbar1.GetContextId (msg);
			contextID=this.statusbar1.Push (contextID, msg);

		}

	//-----------------------------------------------------------------------
		protected void OnRegisterNewSubjectActionActivated (object sender, EventArgs e)
		{
			// Activated from pull-down menu to launch Subject Registration GUI
			RegisterSubject win = new RegisterSubject (SystemController);
			win.Show ();
			Application.Run ();
		}

	//-----------------------------------------------------------------------
		protected void OnAboutActionActivated (object sender, EventArgs e)
		{
			// Show the "About" window 
			AboutWin win = new AboutWin ();
			win.Show();

		}

	//-----------------------------------------------------------------------
		protected void OnToggleHidecontrolsClicked (object sender, EventArgs e)
		{
			// This function toggles the display of the control panel to make the controls disapear when actual data collection is run
				
				if (this.toggle_hidecontrols.Active) {
					this.controlnotebook.Visible = false;
					this.controlnotebook.HeightRequest = 0;
					this.drawingareaMainData.HeightRequest = 600;
					this.toggle_hidecontrols.Label = "Show Controls";
				} else {
					this.controlnotebook.Visible = true;
				this.controlnotebook.HeightRequest = 226;
				this.drawingareaMainData.HeightRequest = 232;
					this.toggle_hidecontrols.Label = "Hide Controls";
				}
		}

	//-----------------------------------------------------------------------
		protected void sdgdraw(object sender, EventArgs e)
		{
			// This is evoked on exposure of the SDG window to draw the probe
			// drawing is handled by the CProbe class and is reused by several GUIs in the code
			SystemController.subjectData.SDGprobe.drawsdg (this.drawingareaSDG.GdkWindow);
			this.drawingareaSDG.QueueDraw ();
			return ;
		}

	//-----------------------------------------------------------------------
		public void ClickSDG (object o, ButtonReleaseEventArgs args)
		{
			// This is evoked when someone clicks on the SDG window to update the active measurement list of the probe.
			// The actual update is handled by the CProbe class allowing it to be reused

			double x = args.Event.X;
			double y = args.Event.Y;

			int width, height;
			this.drawingareaSDG.GdkWindow.GetSize (out width, out height);
		
			SystemController.subjectData.SDGprobe.updateML ((int)x, (int)y,true,width,height);  // update the active measurement list
			SystemController.subjectData.data[0].SDGRef = SystemController.subjectData.SDGprobe;  // restore the probe reference with the updated version

			// update the probe and the data on the the next cycle
			this.drawingareaSDG.QueueDraw ();
			this.drawingareaMainData.QueueDraw ();
		}

		//-----------------------------------------------------------------------
		protected void datadraw(object sender, EventArgs e)
		{
			int whichdata, whichdata2;

			whichdata = (int)Math.Floor ((double)(this.combobox_whichdisplay.Active /AppWindow.SystemController.systemInfo.numwavelengths));
			whichdata2= (int)this.combobox_whichdisplay.Active % (int)AppWindow.SystemController.systemInfo.numwavelengths;


			// This is evoked on exposure of the main data window to update the drawing
			SystemController.subjectData.data[whichdata].drawdata(this.drawingareaMainData.GdkWindow,whichdata2);
			return ;
		}

		//-----------------------------------------------------------------------
		protected void ChangeWhichData (object sender, EventArgs e)
		{
			// User changed which data (690nm, HbO, etc) to display
			this.drawingareaMainData.QueueDraw ();
		}

		//-----------------------------------------------------------------------
		protected void ClickedStartDAQ (object sender, EventArgs e)
		{
			// This handles both the start and stop aquistion

			bool flag = SystemController.systemInfo.start();  // this is the call to the instrument 

			// TODO- add the icon to the button.  Currently disapears after first push
			if (flag) {
				// If instrument said it is now running
				SystemController.subjectData.newscan();
				rtpanel.InitializeRealtimeProcess ();

				StartStop.Label = "Stop Aquistion";
				maindisplaythread = new Thread (updatedata);

				// TODO - adjust the priority of the thread

				maindisplaythread.Start (); // start the timing thread to update the display

				string msg = "Running";
				dispstatus (msg);
				PhotonViewer.AppWindow.paneldb.debugmsg (msg);
			} else {
				StartStop.Label = "Start Aquistion";
				maindisplaythread.Abort ();


				// TODO - Auto save the data and tell the user about it
				string msg = "Aquistion stopped";
				dispstatus (msg);
				PhotonViewer.AppWindow.paneldb.debugmsg (msg);

				// save data
				SystemController.subjectData.scanIdx += 1;
				string filename = System.IO.Path.Combine (SystemController.subjectData.outfolder,
					                  SystemController.subjectData.fileroot +
							String.Format("{0}",SystemController.subjectData.scanIdx)+".nirs");
				SystemController.subjectData.save2matlab(filename);

				msg = "Saved data as: " + filename;
				PhotonViewer.AppWindow.paneldb.debugmsg (msg);

				string name = "Scan-" +
					String.Format("{0}",SystemController.subjectData.scanIdx)+".nirs";

				Gtk.ToggleButton tgl = new Gtk.ToggleButton ();
				tgl.Active = true;

				Gtk.ComboBox cbox = new Gtk.ComboBox ();

				// TODO Fix this part
				StimTreeIter = new TreeIter[FileTreeIter.Length];
				FileTreeIter.CopyTo(StimTreeIter, 0);
				TreeIter[] temp = new TreeIter[FileTreeIter.Length + 1];
				for (int i = 0; i < StimTreeIter.Length; i++) {
					temp [i] = FileTreeIter [i];
				}

				ListStore store = (ListStore)this.treeviewfiles.Model;
				temp[FileTreeIter.Length]=store.AppendValues (name,tgl,cbox);

				FileTreeIter = temp;
			

			}
		}


		//-----------------------------------------------------------------------
		protected void updatedata(){
			// This loop is evoked with the maindisplaythread is started and updates the data display

			float cnt = 0;
			while (maindisplaythread.IsAlive){
				System.Threading.Thread.Sleep (PhotonViewer.MainClass.UPDATETIME);  // update rate (default 500ms)

				// Get data from the instrument
				SystemController.subjectData.data[0] = SystemController.systemInfo.getnewdata (SystemController.subjectData.data[0]);
				SystemController.subjectData.data=rtpanel.UpdateRealtimeProcess (SystemController.subjectData.data);
				this.drawingareaMainData.QueueDraw ();

				// TODO - replace this with a tic/toc from the system so I can compare to the timing report from the instrument thread
				// TODO - add feedback about buffer overload and loss data in DEBUG mode
				cnt += 1;
				string msg=string.Format("Running: Time Elapsed: {0:0.0}",cnt/2);
				dispstatus (msg);

				// spin the progress bar... 
				// TODO- make this not look so stupid
				this.progressbar1.Pulse ();

				if (PhotonViewer.MainClass.DEBUG) {
					float fract = SystemController.systemInfo.getbufferfill ();
					this.progressbar_buffer.Fraction = fract;
				}
			}
		}
				
		protected void AllLasersButtonCallback (object sender, EventArgs e)
		{
			AllLasers.ModifyBg (StateType.Active, new Gdk.Color (200, 100, 100));
			SystemController.laseralltogglestate = !SystemController.laseralltogglestate;

			if (SystemController.laseralltogglestate) {
				AllLasers.State = StateType.Active;
				AllLasers.Label = "All Lasers Off";

			} else {
				AllLasers.State = StateType.Normal;
				AllLasers.Label = "All Lasers On";
			}

			for (int i = 0; i < SystemController.laserhandles.Length; i++) {
				SystemController.laserhandles [i].Active = SystemController.laseralltogglestate;
				SystemController.systemInfo.SetLaser (i, SystemController.laseralltogglestate,false);
			}
			SystemController.systemInfo.SetLaser(0, SystemController.laseralltogglestate);


		}

	
		protected void ClickMarkStim (object sender, EventArgs e)
		{
			int i = SystemController.subjectData.data [0].samples;
			if (i < 0)
				return;

			if(maindisplaythread.IsAlive){

				
				
				double time = SystemController.subjectData.data [0].time [i];

				string cond = combobox_stim.ActiveText;
				double d = SystemController.durtypes[comboboxscantypes.Active][combobox_stim.Active];



				stim ev = new stim ();
				ev.onset.Add (time/1000);
				ev.duration.Add (d);
				ev.amplitude.Add (1);

				SystemController.subjectData.data [0].StimInfo.SetStimEvents (cond, ev);

				ev = SystemController.subjectData.data [0].StimInfo.GetEvents (cond);
				this.labelStimMarks.Text= String.Format ("Marks: {0}", ev.amplitude.Count);


			}
		


		}

		protected void ToggleStimClicked (object sender, EventArgs e)
		{
			if (maindisplaythread.IsAlive) {
				int i = SystemController.subjectData.data [0].samples;
				double time = SystemController.subjectData.data [0].time [i];

				string cond = combobox_stim.ActiveText;

				stim ev = new stim ();
				ev.onset.Add (time/1000);
				ev.duration.Add (999);
				ev.amplitude.Add (1);

				if(togglebuttonstim.Active)
					SystemController.subjectData.data [0].StimInfo.EndStimEvent (cond, time);
				else
					SystemController.subjectData.data [0].StimInfo.SetStimEvents (cond, ev);

			}
		}

	

		protected void PostProcStats (object sender, EventArgs e)
		{
			// TODO launch the stats GUI

			int nummeas = SystemController.subjectData.data [0].nummeas;
			int numtpts = SystemController.subjectData.data [0].samples;
			var Y = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense (numtpts, nummeas, 0);
			var H = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense (numtpts, nummeas, 0);

			double[] time = new double[numtpts];
			for (int i = 0; i < numtpts; i++) {
				time[i]=SystemController.subjectData.data [0].time[i];
				for (int j = 0; j < nummeas; j++) {
					Y [i, j] = SystemController.subjectData.data [0].data [j,i];
				}

			}
			var X = SystemController.subjectData.data[0].StimInfo.GetDesignMtx(time);
		//	var mX = X.ColumnSums();

			// Convert to optical density
			var mY = Y.ColumnSums();
			for (int i = 0; i < numtpts; i++) {
				for (int j = 0; j < nummeas; j++) {
					Y [i, j] = -Math.Log(Y[i,j]) + Math.Log(mY[j]/numtpts);
				}

			}

			// Convert to Hemoglobin
			double E11, E12, E21, E22;
			int sIdx1, sIdx2;
			int numml = SystemController.subjectData.data [0].SDGRef.nummeas;
			for (int i = 0; i < numtpts; i++) {
				for (int j = 0; j < numml; j++) {
					sIdx1 = SystemController.subjectData.data [0].SDGRef.measlist [j, 3];
					E11 = AppWindow.SystemController.systemInfo.laserarray [sIdx1].ExtHbO;
					E12 = AppWindow.SystemController.systemInfo.laserarray [sIdx1].ExtHbR;
					sIdx2 = SystemController.subjectData.data [0].SDGRef.measlist [j+ numml, 3];
					E21 = AppWindow.SystemController.systemInfo.laserarray [sIdx2].ExtHbO;
					E22 = AppWindow.SystemController.systemInfo.laserarray [sIdx2].ExtHbR;

					double L = SystemController.subjectData.data [0].SDGRef.distances [j];
					H [i,j] = 1000000*1/L*(E22 * Y[i,j] - E12 * Y[i,j + numml]) / (E11 * E11 - E12 * E21);
					H [i,j + numml] = 1000000*1/L*(E21 * Y[i,j + numml] - E11 * Y[i,j]) / (E11 * E11 - E12 * E21);
				}
			}

			MathNet.Numerics.LinearAlgebra.Matrix<double>[] C = new MathNet.Numerics.LinearAlgebra.Matrix<double>[nummeas];


			var Beta = (X.Transpose () * X).Inverse () * X.Transpose () * H;
			var res = H - X * Beta;
			var CovBeta = (X.Transpose () * X).Inverse ();


			var mres = res.ColumnSums ();
			for (int i = 0; i < numtpts; i++) {
				for (int j = 0; j < nummeas; j++) {
					res [i, j] = (res[i,j]-mres[j]/numtpts);
				}
			}

			var Sigma2 = res.Transpose()*res/ numtpts;

			for (int i = 0; i < nummeas; i++) {
				C [i] = CovBeta.Clone () * Sigma2 [i, i];
			}


			//var beta = MathNet.Numerics.LinearRegression.MultipleRegression.NormalEquations (X, Y);

			int dof = numtpts - X.ColumnCount;
			Cprobe probe = new Cprobe ();
			probe.numdet = SystemController.subjectData.data [0].SDGRef.numdet;
			probe.numsrc = SystemController.subjectData.data [0].SDGRef.numsrc;
			probe.nummeas = SystemController.subjectData.data [0].SDGRef.nummeas;
			probe.numlambda = SystemController.subjectData.data [0].SDGRef.numlambda;
			probe.detpos = (int[,])SystemController.subjectData.data [0].SDGRef.detpos.Clone();
			probe.srcpos= (int[,])SystemController.subjectData.data [0].SDGRef.srcpos.Clone();
			probe.measlist = (int[,])SystemController.subjectData.data [0].SDGRef.measlist.Clone();
			probe.measlistAct = (bool[])SystemController.subjectData.data [0].SDGRef.measlistAct.Clone();
			probe.colormap = (Gdk.Color[])SystemController.subjectData.data [0].SDGRef.colormap.Clone ();
			string[] cond = SystemController.subjectData.data [0].StimInfo.conditions;
			stat = new CRegressionStats (Beta,C,cond,dof,probe);

			stat.Show ();

		}

		protected void StimComboBoxChanged (object sender, EventArgs e)
		{

			stim ev = SystemController.subjectData.data [0].StimInfo.GetEvents (this.combobox_stim.ActiveText);
			this.labelStimMarks.Text= String.Format ("Marks: {0}", ev.amplitude.Count);

		}

		protected void ShowLegend (object sender, EventArgs e)
		{
			this.drawingareaMainData.QueueDraw ();
		}

		protected void ShowStim (object sender, EventArgs e)
		{
			this.drawingareaMainData.QueueDraw ();
		}

		protected void changesscantype(object sender, EventArgs e)
		{
			int cnt = scantypes.Active;
			ListStore lst2 = new ListStore(typeof(string));
			for (int i = 0; i < SystemController.stimtypes[cnt].Length; i++)
			{
				lst2.AppendValues(SystemController.stimtypes[cnt][i]);
			}
			stimtypes.Model = lst2;
			stimtypes.Active = 0;


		}

		protected void Show3DRegistation(object sender, EventArgs e)
		{
		//	DrawBrain BrainWin = new DrawBrain();
		//	BrainWin.Show();

		


		}

	
	}

}
*/
