﻿using System;

namespace PhotonViewer
{
	[System.ComponentModel.ToolboxItem (true)]
	public partial class LaserPanel : Gtk.Bin
	{
		public LaserBank[] laserbankhandles;

		public LaserPanel ()
		{
			this.Build ();
			laserbankhandles = new LaserBank[4];
			laserbankhandles[0] = this.laserbank1;
			laserbankhandles[1] = this.laserbank2;
			laserbankhandles[2] = this.laserbank3;
			laserbankhandles[3] = this.laserbank4;

			if (!AppWindow.SystemController.systemInfo.laser_adjustable)
			{
				this.entryAllLaserPower.Destroy();
				this.label1.Destroy();
				this.checkbuttonadjlasers.Destroy();
				this.buttonOptimLasersOnly.Destroy();

			}

		}

		protected void SetAllLaserPowers(object sender, EventArgs e)
		{
			try
			{
				int val = Convert.ToInt32(entryAllLaserPower.Text);
				for (int i = 0; i < PhotonViewer.AppWindow.SystemController.laserhandles.Length; i++)
				{
					PhotonViewer.AppWindow.SystemController.systemInfo.SetLaserInten(i, val);
					PhotonViewer.AppWindow.SystemController.laserspinhandles[i].Value = val;
				}
			}
			catch
			{
				entryAllLaserPower.Text = "0";
			}
		}

	}
}

