﻿using System;
using Microsoft.Xna.Framework;
using System.Xml;

namespace BrainScanIR
{
	public class TenTwenty
	{
		public Vector3[] points;
		public string[] labels;
		public TenTwenty(string filename)
		{

			XmlDocument doc = new XmlDocument();
			XmlDocument doc2 = new XmlDocument();
			doc.Load(filename);
			XmlNodeList elemList;
			XmlNodeList elemList2;

			elemList = doc.GetElementsByTagName("pt");

			labels = new string[elemList.Count];
			points = new Vector3[elemList.Count];
			for (int i = 0; i < elemList.Count; i++)
			{
				doc2 = new XmlDocument();
				doc2.LoadXml("<root>" + elemList[i].InnerXml + "</root>");
				elemList2 = doc2.GetElementsByTagName("name");
				labels[i] = elemList2[0].InnerXml;

				double x, y, z;
				elemList2 = doc2.GetElementsByTagName("x");
				x = Convert.ToDouble(elemList2[0].InnerXml);
				elemList2 = doc2.GetElementsByTagName("y");
				y = Convert.ToDouble(elemList2[0].InnerXml);
				elemList2 = doc2.GetElementsByTagName("z");
				z = Convert.ToDouble(elemList2[0].InnerXml);
                points[i] = new Vector3();
                points[i].X = (float)x;
                points[i].Y = (float)y;
                points[i].Z = (float)z;
			}
		}
	}
}
