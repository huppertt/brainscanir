﻿using System;
namespace BrainScanIR
{
    public partial class CShowDataQuality : Gtk.Window
    {
        double[] SNI;
        double[] SNR;
        double[] Inten;
        double[] Var;
        Cprobe probe;
        Gdk.Color[] SNRCM;
		Gdk.Color[] SNICM;
		Gdk.Color[] IntenCM;
		Gdk.Color[] VarCM;


		public CShowDataQuality(CNIRSdata data) :
                base(Gtk.WindowType.Toplevel)
        {
            this.Build();
            this.drawingarea1.ExposeEvent += drawprobe;
            SNI = new double[data.nummeas];
            SNR = new double[data.nummeas]; 
            Inten = new double[data.nummeas];
            Var = new double[data.nummeas];

            SNRCM = new Gdk.Color[data.nummeas];
            SNICM = new Gdk.Color[data.nummeas];
            IntenCM = new Gdk.Color[data.nummeas];
			VarCM = new Gdk.Color[data.nummeas];


			probe = new Cprobe();
            probe.detpos = data.SDGRef.detpos;
            probe.srcpos = data.SDGRef.srcpos;
            probe.measlist = data.SDGRef.measlist;
            probe.measlistAct = data.SDGRef.measlistAct;
            probe.nummeas = data.SDGRef.nummeas;
            probe.numdet = data.SDGRef.numdet;
            probe.numsrc = data.SDGRef.numsrc;


            for (int i = 0; i < data.nummeas; i++){
                Inten[i] = 0;
                Var[i] = 0;
                for (int j = 0; j < data.samples; j++){
                    Inten[i] += data.data[i, j];
                }
                Inten[i] = Inten[i] / data.samples;
				for (int j = 0; j < data.samples; j++)
				{
                    Var[i] += (data.data[i, j]-Inten[i])*(data.data[i, j] - Inten[i]);
				}
				Var[i] = Var[i] / data.samples;
                Var[i] = Math.Sqrt(Var[i]);
                SNR[i] = Inten[i] / Var[i];
			}
            for (int i = 0; i < data.nummeas; i++){
                byte a = (byte)Math.Round(Math.Min(10 * SNR[i], 255));
                SNRCM[i] = new Gdk.Color((byte)(255 - a), a, 0);
                SNICM[i] = new Gdk.Color((byte)(255 - a), a, 0);
                byte a2 = (byte)Math.Round(Math.Min(Inten[i] / 10, 255));
                IntenCM[i] = new Gdk.Color((byte)(255 - a2), a2, 0);
                byte a3 = (byte)Math.Round(Math.Min(Var[i], 255));
                IntenCM[i] = new Gdk.Color(a2, (byte)(255 - a2), 0);
            }
            probe.colormap = SNRCM;

            probe.drawsdg(this.drawingarea1.GdkWindow);
            this.drawingarea1.QueueDraw();
            ShowAll();

        }

		protected void drawprobe(object sender, EventArgs e)
		{
			probe.drawsdg(this.drawingarea1.GdkWindow);
            this.drawingarea1.QueueDraw();
		}

        protected void ChangeColorMap(object sender, EventArgs e)
        {
            if (this.combobox1.Active == 0)
                probe.colormap = SNICM;
            else if (this.combobox1.Active == 1)
                probe.colormap = SNRCM;
            else if (this.combobox1.Active == 2)
                probe.colormap = IntenCM;
            else if (this.combobox1.Active == 3)
                probe.colormap = VarCM;
			probe.drawsdg(this.drawingarea1.GdkWindow);
			this.drawingarea1.QueueDraw();
            ShowAll();


        }
    }
}
