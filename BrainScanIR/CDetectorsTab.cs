﻿using System;
using Gtk;
using Gdk;

namespace BrainScanIR
{
    [System.ComponentModel.ToolboxItem(true)]
    public partial class CDetectorsTab : Gtk.Bin
    {
        public Label[] dlabels;
        public VScale[] dscales;
        public ColorButton[] dimage;
        public float[] SNR;
        public int whichpanel;

        private float SNRThresLow = 1.2f;
        private float SNRThresHi = 3.0f;
        private Color SNRLowColor = new Color(178,102,255);
        private Color SNRMidColor = new Color(102,255,255);
		private Color SNRHiColor = new Color(102,255,102);
        private Color SaturatedColor = new Color(255, 0, 0);


		public CDetectorsTab()
        {
            this.Build();
            whichpanel = 0;
            dlabels = new Label[8];
            dscales = new VScale[8];
            dimage = new ColorButton[8];
            SNR = new float[8];


            dlabels[0] = this.GtkLabel;
            dlabels[1] = this.GtkLabel1;
            dlabels[2] = this.GtkLabel2;
            dlabels[3] = this.GtkLabel3;
            dlabels[4] = this.GtkLabel4;
            dlabels[5] = this.GtkLabel5;
            dlabels[6] = this.GtkLabel6;
            dlabels[7] = this.GtkLabel7;
            dscales[0] = this.vscale1;
            dscales[1] = this.vscale2;
            dscales[2] = this.vscale3;
            dscales[3] = this.vscale4;
            dscales[4] = this.vscale5;
            dscales[5] = this.vscale6;
            dscales[6] = this.vscale7;
            dscales[7] = this.vscale8;
            dimage[0] = this.colorbutton1;
			dimage[1] = this.colorbutton2;
            dimage[2] = this.colorbutton3;
            dimage[3] = this.colorbutton4;
            dimage[4] = this.colorbutton5;
            dimage[5] = this.colorbutton6;
            dimage[6] = this.colorbutton7;
            dimage[7] = this.colorbutton8;

            for (int i = 0; i < 8; i++) { 
                SNR[i] = 0;
                dscales[i].UpdatePolicy = UpdateType.Delayed;  //This will delay the slider response
            }
            updateSNRcolors();

		}

        protected void DetSliderCallback2(object sender, EventArgs e)
        {
            adjustgain(1, this.vscale2.Value);
        }

        protected void DetSliderCallback3(object sender, EventArgs e)
        {
            adjustgain(2, this.vscale3.Value);
        }

        protected void DetSliderCallback4(object sender, EventArgs e)
        {
            adjustgain(3, this.vscale4.Value);
        }

        protected void DetSliderCallback5(object sender, EventArgs e)
        {
            adjustgain(4, this.vscale5.Value);
        }

        protected void DetSliderCallback6(object sender, EventArgs e)
        {
            adjustgain(5, this.vscale6.Value);
        }

        protected void DetSliderCallback7(object sender, EventArgs e)
        {
            adjustgain(6, this.vscale7.Value);
        }

        protected void DetSliderCallback8(object sender, EventArgs e)
        {
            adjustgain(7, this.vscale8.Value);
        }

        protected void DetSliderCallback1(object sender, EventArgs e)
        {
            adjustgain(0, this.vscale1.Value);
        }

        protected void adjustgain(int idx,double val){

            MainClass.SystemControler.SystemInfo.SetDetector(8*whichpanel+idx,(int)val);
        
        }

        public void updateSNRcolors(){
            Color col = new Color();
            for (int i = 0; i < 8; i++)
            {
                if (SNR[i] < SNRThresLow & SNR[i]>=0)
                    col = SNRLowColor;
                else if (SNR[i] >= SNRThresLow & SNR[i] < SNRThresHi)
                    col = SNRMidColor;
                else if (SNR[i] < 0)
                    col = SaturatedColor;
                else
                    col = SNRHiColor;
                
                dimage[i].Color = col;
            }
        }

    }
}
