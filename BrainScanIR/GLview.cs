﻿using System;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

using System.Threading;

namespace BrainScanIR
{
    [System.ComponentModel.ToolboxItem(true)]
    public partial class GLview : Gtk.Bin
    {
        
        public DrawMesh game;
        Thread task;
        public Gtk.DrawingArea container;
        public GLview()
        {
            this.Build();
	        //this.ParentWindow.KeepBelow = true;
            container = this.drawingarea1;
            container.ExposeEvent += ExposeGL;

		}

    
        public void startdrawGL()
        {
            game = null;
            task = new Thread(() =>
            {
                game = new DrawMesh(this.container);
                game.Run();
            });
            task.Start();
            while (game == null)
                Thread.Yield(); // wait a bit for another thread to init variable if necessary

           
        }

       void ExposeGL(object sender, EventArgs e){
            int x, y, h, w;
            int xp, yp;
            x = this.container.Allocation.X;
			y = this.container.Allocation.Y;

            xp = this.ParentWindow.FrameExtents.X;
            yp = this.ParentWindow.FrameExtents.Y;
            this.ParentWindow.Lower();
           
            w=this.container.Allocation.Width;
            h = this.container.Allocation.Height;

            System.Drawing.Size siz;
            siz.Height = h; 
            siz.Width = w;
            this.game.Size = siz;
            this.game.X = x+xp;
            this.game.Y = y+yp;
        }


		public void stopdrawGL()
        {
            task.Abort();
            game.Exit();
            game.Dispose();
            this.Destroy();

        }

        protected void Resized(object o, Gtk.SelectionRequestEventArgs args)
        {
            ExposeGL(null, null);
        }
		
	}

    //**************************************************************************

    public class DrawMesh : OpenTK.GameWindow
    {
       Vector3 RotAxis;
        double RotAngle;


        public DrawMesh(Gtk.DrawingArea da) :
        base(300,350, GraphicsMode.Default, "Brain View")
        {
            //Vsync = VSyncMode.On;
            this.WindowBorder = WindowBorder.Hidden;
            int x, y;
            int w, h;

            //da.SetPosition(Gtk.WindowPosition.Center);
            h = da.HeightRequest;
            w = da.WidthRequest;
            da.GdkWindow.GetPosition(out x, out y);
            System.Drawing.Point pts;
            pts.X = x;
            pts.Y = y;
            System.Drawing.Size siz;
            siz.Height = h;
            siz.Width = w;

            this.Location = pts;
          //  this.Size = siz;

            RotAngle = 0;
            RotAxis = Vector3.UnitZ;
            da.DestroyEvent += closethis;


        }

        protected void closethis(object o, Gtk.DestroyEventArgs e)
        {
            this.Close();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            GL.ClearColor(1f, 1f, 1f, 0f);
            GL.Enable(EnableCap.DepthTest);
        }


        protected override void OnRenderFrame(FrameEventArgs e)
        {
            Cprobe SDGprobe = MainClass.SystemControler.SubjectData.SDGprobe;
			double[,] detpos3D = MainWindow.headmodel[0].set2surface(SDGprobe.detpos3D, SDGprobe.numdet);
			double[,] srcpos3D = MainWindow.headmodel[0].set2surface(SDGprobe.srcpos3D, SDGprobe.numsrc);


			base.OnRenderFrame(e);
            // Setup buffer
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.MatrixMode(MatrixMode.Projection);

            GL.LoadIdentity();
            GL.Ortho(MainWindow.headmodel[0].boundingboxMin.X * 1.4,
                     MainWindow.headmodel[0].boundingboxMax.X * 1.4,
                     MainWindow.headmodel[0].boundingboxMin.Y * 1.4,
                     MainWindow.headmodel[0].boundingboxMax.Y * 1.4,
                     MainWindow.headmodel[0].boundingboxMin.Z * 1.4,
                     MainWindow.headmodel[0].boundingboxMax.Z * 1.4);

            GL.Material(MaterialFace.Front, MaterialParameter.Diffuse, new Vector4(0.8f, .8f, 0.8f, 1.0f));
            GL.Material(MaterialFace.Front, MaterialParameter.Specular, new Vector4(0f, .8f, .8f, 1.0f));
            GL.Material(MaterialFace.Front, MaterialParameter.Shininess, 50);
            GL.Light(LightName.Light0, LightParameter.Position, new Vector4(-200f, 0f, 0f, 1f));
            GL.Light(LightName.Light0, LightParameter.Diffuse, new float[] { 1.0f, 1.0f, 1.0f, 1.0f });

            GL.Enable(EnableCap.Lighting);
            GL.Enable(EnableCap.Light0);
            GL.Enable(EnableCap.ColorMaterial);
            GL.Enable(EnableCap.DepthTest);

            //GL.PushMatrix();
            GL.Rotate(90,Vector3.UnitX);
            GL.Rotate(180, Vector3.UnitY);
            GL.Rotate((float)RotAngle, RotAxis);
            GL.Begin(PrimitiveType.Triangles);


            int cnt;
            if (MainClass.AppWindow.showkin)
                cnt = 0;
            else
                cnt = 1;

            for (int i = 0; i < MainWindow.headmodel[cnt].nfaces; i++)
            {
                GL.Normal3(MainWindow.headmodel[cnt].facenormals[i].X,
                           MainWindow.headmodel[cnt].facenormals[i].Y,
                           MainWindow.headmodel[cnt].facenormals[i].Z);
                for (int j = 0; j < 3; j++)
                {
                    int ii = MainWindow.headmodel[cnt].faces[i, j];
                    GL.Normal3(MainWindow.headmodel[cnt].vertices[ii].Normal.X,
                               MainWindow.headmodel[cnt].vertices[ii].Normal.Y,
                               MainWindow.headmodel[cnt].vertices[ii].Normal.Z);

                    Vector3 col = new Vector3();
                    col.X = MainWindow.headmodel[cnt].vertices[ii].Color.R;
                    col.Y = MainWindow.headmodel[cnt].vertices[ii].Color.B;
                    col.Z = MainWindow.headmodel[cnt].vertices[ii].Color.G;
                    GL.Color3(col.X, col.Y, col.Z);
                    GL.Vertex3(MainWindow.headmodel[cnt].vertices[ii].Position.X,
                               MainWindow.headmodel[cnt].vertices[ii].Position.Y,
                               MainWindow.headmodel[cnt].vertices[ii].Position.Z);



                }
            }


            if (SDGprobe != null)
            {
                if (SDGprobe.isregistered & MainClass.AppWindow.showprobe)
                {
                    float s = 5f;
                    for (int k = 0; k < SDGprobe.numdet; k++)
                    {

                        float x, y, z;

                        x = (float)detpos3D[k, 0];
                        y = (float)detpos3D[k, 1];
                        z = (float)detpos3D[k, 2];
                        for (int i = 0; i < MainWindow.sphereGifti.nfaces; i++)
                        {
                            for (int j = 0; j < 3; j++)
                            {
                                int ii = MainWindow.sphereGifti.faces[i, j];
                                GL.Normal3(MainWindow.sphereGifti.vertices[ii].Normal.X,
                                        MainWindow.sphereGifti.vertices[ii].Normal.Y,
                                        MainWindow.sphereGifti.vertices[ii].Normal.Z);
                                GL.Color3(1f, 0f, 0f);
                                GL.Vertex3(MainWindow.sphereGifti.vertices[ii].Position.X * s + x,
                                           MainWindow.sphereGifti.vertices[ii].Position.Y * s + y,
                                           MainWindow.sphereGifti.vertices[ii].Position.Z * s + z);

                            }

                        }
                    }
                    for (int k = 0; k < SDGprobe.numsrc; k++)
                    {

                        float x, y, z;
                        x = (float)srcpos3D[k, 0];
                        y = (float)srcpos3D[k, 1];
                        z = (float)srcpos3D[k, 2];
                        for (int i = 0; i < MainWindow.sphereGifti.nfaces; i++)
                        {

                            for (int j = 0; j < 3; j++)
                            {
                                int ii = MainWindow.sphereGifti.faces[i, j];
                                GL.Normal3(MainWindow.sphereGifti.vertices[ii].Normal.X,
                                       MainWindow.sphereGifti.vertices[ii].Normal.Y,
                                       MainWindow.sphereGifti.vertices[ii].Normal.Z);
                                GL.Color3(0f, 0f, 1f);
                                GL.Vertex3(MainWindow.sphereGifti.vertices[ii].Position.X * s + x,
                                           MainWindow.sphereGifti.vertices[ii].Position.Y * s + y,
                                           MainWindow.sphereGifti.vertices[ii].Position.Z * s + z);

                            }

                        }
                    }
                }
            }


            if (MainClass.AppWindow.show1020)
            {
                float s = 3f;
                for (int k = 0; k < MainWindow.c1020.points.Length; k++)
                {

                    float x, y, z;
                    x = MainWindow.c1020.points[k].X;
                    y = MainWindow.c1020.points[k].Y;
                    z = MainWindow.c1020.points[k].Z;
                    for (int i = 0; i < MainWindow.sphereGifti.nfaces; i++)
                    {

                        for (int j = 0; j < 3; j++)
                        {
                            int ii = MainWindow.sphereGifti.faces[i, j];
                            GL.Normal3(MainWindow.sphereGifti.vertices[ii].Normal.X,
                                   MainWindow.sphereGifti.vertices[ii].Normal.Y,
                                   MainWindow.sphereGifti.vertices[ii].Normal.Z);

                            GL.Color3(.3f, 0f, 0f);
                            GL.Vertex3(MainWindow.sphereGifti.vertices[ii].Position.X * s + x,
                                       MainWindow.sphereGifti.vertices[ii].Position.Y * s + y,
                                       MainWindow.sphereGifti.vertices[ii].Position.Z * s + z);

                        }

                    }
                }

            }


            GL.End();
            GL.PopMatrix();

            SwapBuffers();


        }

        /*
        protected void OnMouseDown(Input.MouseButtonEventArgs e)
        {

            dragPointPrev = new Vector3();
            dragPointPrev.X = e.X / (float)this.Width - .5f;
            dragPointPrev.Y = e.Y / (float)this.Height - .5f;
            dragPointPrev.Z = (float)Math.Sqrt(1 - dragPointPrev.X * dragPointPrev.X - dragPointPrev.Y * dragPointPrev.Y);


            base.OnMouseDown(e);

        }
        protected void OnMouseUp(Input.MouseButtonEventArgs e)
        {
            dragPointNew = new Vector3();
            dragPointNew.X = e.X / (float)this.Width - .5f;
            dragPointNew.Y = e.Y / (float)this.Height - .5f;
            dragPointNew.Z = (float)Math.Sqrt(1 - dragPointNew.X * dragPointNew.X - dragPointNew.Y * dragPointNew.Y);

            dragPointPrev.Normalize();
            dragPointNew.Normalize();
            RotAngle = Math.Acos(Vector3.Dot(dragPointPrev, dragPointNew));
            RotAxis = Vector3.Cross(dragPointPrev, dragPointNew);
            RotAxis.Normalize();

            base.OnMouseUp(e);
        }
        */
    }

}

