﻿using System;
using System.Xml;

/* This file contains the:
 * SystemControl Class
 *      - SystemInfo - handles and controls to DAQ devices
 *      - SubjectData - Probe/Data/Demographics info for this scan
 *      - scantypes/stimtyoes/durtypes - holds stimulus/run information (from XML file)
 *      - handles to control widgets on the main window
 *      Methods:
 *          -ReadXMLStims - Reads stim/run information from XML file.  This is often the 
 *                          later half of a probe.sdg file, but could be a seperate file TBD
*/

namespace BrainScanIR
{

	public class SystemControlClass
	{
		// Method to hold all the objects for the control software
		public Csystem SystemInfo;    // class to hold info on instrument
		public Csubject SubjectData;  // Class to hold subject and data info
		public static Csystem SystemControler;



		// handles to widgets so I can toggle the visibility from function calls
		//		public ToggleButton[] laserhandles;
		//		public SpinButton[] laserspinhandles;
		//		public VScale[] detectorhandles;
		//		public Button allonbuttonhandle;
		//		public Button startbuttonhandle;
		//		public ComboBox whichdisphandle;

		public bool laseralltogglestate;   // remembers state of all on/off since there are multiple referneces of this button

		public string[] scantypes;   // These hold the types of scans and stim-types associated with
		public string[][] stimtypes; // each type of scan.   
		public double[][] durtypes;  // this holds the default duration (in seconds) for stim-types

		// Constructor
		public SystemControlClass()
		{

			laseralltogglestate = false;
			SystemInfo = new Csystem();     // The constructor for this reads the XML file and sets the instrument specs
			SubjectData = new Csubject();   // Initialize the subject class
		}


		public void ReadXMLStims(string file)
		{
			// This function reads the default stim information from an XML file
			// This is called after the probe is loaded via the register subject menu
			XmlDocument doc = new XmlDocument();
			XmlDocument doc2 = new XmlDocument();
			XmlDocument doc3 = new XmlDocument();

			doc.Load(file); // opens the XML stream
			XmlNodeList elemList, elemListsub, elemListsubsub;

			// Scan types and stimulus information
			elemList = doc.GetElementsByTagName("scan");

			// Add 4 default types (setup, resting, --, --) to allow real-time editing
			scantypes = new string[elemList.Count + 4];
			stimtypes = new string[elemList.Count + 4][];
			durtypes = new double[elemList.Count + 4][];

			// First entry will be the "setup" 
			string[] stims = new string[4];
			double[] dur = new double[4];
			stims[0] = "Mark #1"; stims[1] = "Mark #2"; stims[2] = "Mark #3"; stims[3] = "Mark #4";
			dur[0] = 1; dur[1] = 1; dur[2] = 1; dur[3] = 1;
			scantypes[0] = "Setup";
			stimtypes[0] = stims;
			durtypes[0] = dur;

			// Now load all the scan types from the XML file
			for (int i = 0; i < elemList.Count; i++)
			{
				doc2 = new XmlDocument();
				doc2.LoadXml("<root>" + elemList[i].InnerXml + "</root>");
				elemListsub = doc2.GetElementsByTagName("name");
				scantypes[i + 1] = elemListsub[0].InnerXml;

				elemListsub = doc2.GetElementsByTagName("condition");
				stims = new string[elemListsub.Count];
				dur = new double[elemListsub.Count];
				for (int j = 0; j < elemListsub.Count; j++)
				{
					doc3 = new XmlDocument();
					doc3.LoadXml("<root>" + elemListsub[j].InnerXml + "</root>");
					elemListsubsub = doc3.GetElementsByTagName("name");
					stims[j] = elemListsubsub[0].InnerXml;
					elemListsubsub = doc3.GetElementsByTagName("name");
					stims[j] = elemListsubsub[0].InnerXml;
					elemListsubsub = doc3.GetElementsByTagName("duration");
					dur[j] = Convert.ToDouble(elemListsubsub[0].InnerXml);

					//TODO add trigger types from XML file for stim detection off aux channels

				}
				stimtypes[i + 1] = stims;
				durtypes[i + 1] = dur;
			}


			// Add "Resting" condition
			stims = new string[4];
			dur = new double[4];
			stims[0] = "Mark #1"; stims[1] = "Mark #2"; stims[2] = "Mark #3"; stims[3] = "Mark #4";
			dur[0] = 1; dur[1] = 1; dur[2] = 1; dur[3] = 1;
			scantypes[elemList.Count + 1] = "Resting";
			stimtypes[elemList.Count + 1] = stims;
			durtypes[elemList.Count + 1] = dur;


			// Add blank (editable) condition #1
			stims = new string[4];
			dur = new double[4];
			stims[0] = "Mark #1"; stims[1] = "Mark #2"; stims[2] = "Mark #3"; stims[3] = "Mark #4";
			dur[0] = 1; dur[1] = 1; dur[2] = 1; dur[3] = 1;
			scantypes[elemList.Count + 2] = " ----  ";
			stimtypes[elemList.Count + 2] = stims;
			durtypes[elemList.Count + 2] = dur;


			// Add blank (editable) condition #2
			stims = new string[4];
			dur = new double[4];
			stims[0] = "Mark #1"; stims[1] = "Mark #2"; stims[2] = "Mark #3"; stims[3] = "Mark #4";
			dur[0] = 1; dur[1] = 1; dur[2] = 1; dur[3] = 1;
			scantypes[elemList.Count + 3] = " ----  ";
			stimtypes[elemList.Count + 3] = stims;
			durtypes[elemList.Count + 3] = dur;

			return; // End of ReadXMLStims
		}

	} // End of SystemControlClass definition


}
