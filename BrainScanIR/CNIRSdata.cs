﻿using System;
using System.Collections;
using Gdk;
using Gtk;
using System.Collections.Generic;
using MathNet.Numerics;

// Class to hold data 


namespace BrainScanIR
{

	public class stim{
		public List<double> onset;
		public List<double> duration;
		public List<double> amplitude;

		public stim(){
			onset = new List<double>();
			duration = new List<double>();
			amplitude = new List<double>();
		}

	}
	public class StimEvents{
		public int numconditions;
		public string[] conditions;
		public static stim[] events;

		public StimEvents(){
			numconditions = 0;
			conditions = new string[0];
			events = new stim[0];
		}
			
		public stim GetEvents (int i){
			return events [i];
		}

		public stim GetEvents (string name){
			int ind=-1;
			for (int i = 0; i < numconditions; i++) {
				if (conditions [i].Equals (name)) {
					ind=i;
				}
			}
			if(ind==-1)
				return new stim();
			else
			return events [ind];
		}

		public void SetStimEvents(string name, stim newevent){
			bool found = false;
			for(int i=0; i<numconditions; i++){
				if(conditions[i].Equals(name)){
					for (int j = 0; j < newevent.amplitude.Count; j++) {
						events [i].onset.Add (newevent.onset [j]);
						events [i].duration.Add (newevent.duration [j]);
						events [i].amplitude.Add (newevent.amplitude [j]);
					}
					found = true;
				}
			}
			if (!found) {
				stim[] events2= new stim[numconditions+1];
				string[] conditions2 = new string[numconditions + 1];
				for(int i=0; i<numconditions; i++){
					events2[i]=events[i];
					conditions2[i]=conditions[i];
				}
				events2[numconditions]=newevent;
				conditions2 [numconditions] = name;
				numconditions++;
				events=events2;
				conditions = conditions2;
			}


			TreeIter[] temp = new TreeIter[MainClass.AppWindow.handles.StimTreeIter.Length + 1];
			for (int i = 0; i < MainClass.AppWindow.handles.StimTreeIter.Length; i++) {
				temp [i] = MainClass.AppWindow.handles.StimTreeIter [i];
			}
				
			temp[MainClass.AppWindow.handles.StimTreeIter.Length]=MainClass.AppWindow.handles.StimStore.AppendValues(name,String.Format("{0}",newevent.onset[0]),
						String.Format("{0}",newevent.duration[0]),String.Format("{0}",newevent.amplitude[0]));
			MainClass.AppWindow.handles.StimTreeIter = temp;
    

		}
		public void EndStimEvent(string name, double endt){
			for(int i=0; i<numconditions; i++){
				if(conditions[i].Equals(name)){
					int cnt =events[i].duration.Count-1;
					events[i].duration[cnt]=(endt/1000-events[i].onset[cnt]);
				}
			}
		}

		public void ChangeStimEvents (string oldname,stim oldev,string newname, stim newev){
			for (int i = 0; i < numconditions; i++) {
				if (conditions [i].Equals (oldname)) {
					double indx = events [i].onset.FindIndex(item=>item==oldev.onset [0]);
					events [i].onset.RemoveAt ((int)indx);
					events [i].duration.RemoveAt ((int)indx);
					events [i].amplitude.RemoveAt ((int)indx);

				}
			}
			for (int i = 0; i < numconditions; i++) {
				if (conditions [i].Equals (newname)) {
					events [i].onset.Add(newev.onset[0]);
					events [i].duration.Add(newev.duration[0]);
					events [i].amplitude.Add(newev.amplitude[0]);

				}
			}
		}

		public MathNet.Numerics.LinearAlgebra.Matrix<double> GetDesignMtx(double[] time){
			// This returns the binary non-convolved version of the design matrix 
			var X = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense(time.Length,numconditions+1,0);

			for (int i = 0; i < time.Length; i++) {
				for (int j = 0; j < numconditions; j++) {
					for (int k = 0; k < events [j].amplitude.Count; k++) {
						if (time [i] >= events [j].onset [k]*1000 & time [i] < (events [j].onset [k] + events [j].duration [k])*1000)
						X [i, j] = 1;
					}
				}
				X [i, numconditions] = 1; // Column of 1's for DC regressor
			}
			return X;
		}


	}

	public class CNIRSdata
	{
		public double[] time;
		public double[,] data;
		public double[,] aux;
		public int samples;
		public Cprobe SDGRef;  // The data keeps a reference to the probe object
		public int nummeas;  // This is the total number (#wavelengths x # src-dets)
		public int numaux;
		public string type;
		public StimEvents StimInfo;
		public Color[] stimcolor;

		int buffersize = MainClass.BUFFER_SIZE;


	//-----------------------------------------------------------------------
		public CNIRSdata (Cprobe SDG)
		{
			// Constructor- intial empty data fields
			nummeas = SDG.nummeas*SDG.numlambda;
			numaux = 4;
			// Initialize data with an empty buffer
			time = new double[buffersize];
			data = new double[nummeas, buffersize];
			aux = new double[numaux, buffersize];
			samples = -1;
			SDGRef = SDG;
			type = "raw";

			StimInfo = new StimEvents();

			stimcolor = new Color[12];
			// for now
			stimcolor[0]=new Color (255, 200, 200);
			stimcolor[1]=new Color (200, 255, 200);
			stimcolor[2]=new Color (200, 200, 255);
			stimcolor[3]=new Color (200, 255, 255);
			stimcolor[4]=new Color (255, 200, 255);
			stimcolor[5]=new Color (255, 255, 200);
			stimcolor[6]=new Color (255, 200, 200);
			stimcolor[7]=new Color (200, 255, 200);
			stimcolor[8]=new Color (200, 200, 255);
			stimcolor[9]=new Color (200, 255, 255);
			stimcolor[10]=new Color (255, 200, 255);
			stimcolor[11]=new Color (255, 255, 200);

			return;
		}

		//-----------------------------------------------------------------------
		public void adddata(double[] d,double t){
			// function to add a new data point

			// TODO make sure that I am not exceeding the data buffer and if so write to file and start over again


			for (int i = 0; i < this.SDGRef.nummeas * this.SDGRef.numlambda; i++) {
				this.data [i, samples+1] = d [this.SDGRef.measlist[i,5]];
			}
			for (int i = 0; i < this.numaux; i++)
			{
				this.aux[i, samples + 1] = d[this.SDGRef.nummeas * this.SDGRef.numlambda+i];
			}
			this.time [samples+1] = t;
			samples += 1;  // Update the samples last to prevent read of new data until the end

			return;
		}



		//-----------------------------------------------------------------------
		public void drawdata(Gdk.Drawable da,int whichdata){
			int width, height;
			da.GetSize (out width, out height);

			if((int)this.samples<1)
				return;

			double maxY, minY;
			minY = 99999; maxY = -99999;

            int startIdx = 0;
            if (MainClass.AppWindow.handles.windowdata_checkbox.Active) {
				int tim;
				try
				{
					tim= Convert.ToInt32((string)MainClass.AppWindow.handles.windatatime.Text);
				}
				catch
				{
					tim = (int)(this.time[this.samples]/1000);
				}

				double startt = this.time [this.samples] - tim*1000;
				startIdx = Array.FindIndex(this.time, item => item >= startt);
			}

			for (int i = 0; i < this.SDGRef.nummeas; i++) {
				for (int j = startIdx; j < this.samples; j++) {
					if (this.SDGRef.measlistAct [i]) {
						double d = this.data [i+this.SDGRef.nummeas * whichdata, j];
						if (maxY < d)
							maxY = d;
						if (minY > d)
							minY = d;
					}
				}
			}
				
			double rangeY = maxY - minY;
			double rangeX = this.time[(int)this.samples]- this.time [startIdx];
			int xoffset = 20;
			int yoffset = 1; 

			height = height - 24;
			width = width - 24;

			Gdk.GC gc = new Gdk.GC (da);

			gc.RgbBgColor = new Gdk.Color (0, 0, 0);
			gc.RgbFgColor = new Gdk.Color (0,0,0);
			Rectangle rarea = new Rectangle();
			rarea.X = xoffset-1;
			rarea.Y = yoffset-1;
			rarea.Height = height+2;
			rarea.Width = width+2;
			da.DrawRectangle (gc, true, rarea);

			gc.RgbBgColor = new Color (0, 0, 0);
			gc.RgbFgColor = new Color (255, 255, 255);
			rarea = new Rectangle();
			rarea.X = xoffset;
			rarea.Y = yoffset;
			rarea.Height = height;
			rarea.Width = width;
			da.DrawRectangle (gc, true, rarea);


			gc.SetLineAttributes(2, LineStyle.Solid, CapStyle.Projecting, JoinStyle.Round);

            if (MainClass.AppWindow.handles.showstim.Active) {
				for (int j = 0; j < this.StimInfo.numconditions; j++) {
					gc.RgbFgColor = stimcolor [j];
					Rectangle area = new Rectangle ();
					stim ev = this.StimInfo.GetEvents (j);
					
					for (int k = 0; k < ev.onset.Count; k++) {
						if (ev.amplitude [k] > 0 & ev.onset[k]+ev.duration[k]>=this.time [startIdx]/1000) {
							area.Width = (int)(ev.duration [k] * 1000 / rangeX * width);
							area.Height = height;
							area.X = (int)(xoffset + Math.Max((ev.onset [k] * 1000 - this.time [startIdx]),0) / rangeX * width);
							area.Y = yoffset;	
							da.DrawRectangle (gc, true, area);
						}
						//da.DrawLine(gc,(int)x+xoffset,yoffset,(int)x+xoffset,(int)height+yoffset);
					}	
				}



			}


			gc.SetLineAttributes(1, LineStyle.Solid, CapStyle.Projecting, JoinStyle.Round);
			for (int i = 0; i < this.SDGRef.nummeas; i++) {
				int ii = i + this.SDGRef.nummeas * whichdata;
				if (this.SDGRef.measlistAct [i]) {
					gc.RgbFgColor = this.SDGRef.colormap [i];
					for (int j = startIdx+1; j < this.samples; j++) {

						double y2 = (this.data[ii,j]-minY)/rangeY*height;
						double y1 = (this.data[ii,j-1]-minY)/rangeY*height;
				
						double x2 = (this.time[j]-this.time[startIdx])/rangeX*width;
						double x1 = (this.time[j-1]-this.time[startIdx])/rangeX*width;

						da.DrawLine(gc,(int)x1+xoffset,(int)(height-y1+yoffset),(int)x2+xoffset,(int)(height-y2+yoffset));
					}
				}
			}

            if (MainClass.AppWindow.handles.showstim.Active & MainClass.AppWindow.handles.showlegend.Active) {
				// add legend to window
				int w=0;
				int h=0;

				int maxw = 0;

				for (int j = 0; j < this.StimInfo.numconditions; j++) {
					Gtk.Label lab = new Gtk.Label ();
					gc.RgbFgColor = stimcolor [j];
					lab.Text = this.StimInfo.conditions [j];
					da.DrawLayout (gc, xoffset+10, yoffset+j*10+10, lab.Layout);
					lab.Layout.GetPixelSize (out w, out h);
					if (w > maxw)
						maxw = w;
				}
				gc.RgbBgColor = new Gdk.Color (0, 0, 0);
				gc.RgbFgColor = new Gdk.Color (0,0,0);
				rarea = new Rectangle();
				rarea.X = xoffset+4;
				rarea.Y = yoffset+4;
				rarea.Height = this.StimInfo.numconditions*10+12;
				rarea.Width = maxw+12;
				da.DrawRectangle (gc, true, rarea);

				gc.RgbBgColor = new Gdk.Color (0, 0, 0);
				gc.RgbFgColor = new Gdk.Color (255, 255, 255);
				rarea = new Rectangle();
				rarea.X = xoffset+5;
				rarea.Y = yoffset+5;
				rarea.Height = this.StimInfo.numconditions*10+10;
				rarea.Width = maxw+10;
				da.DrawRectangle (gc, true, rarea);
				for (int j = 0; j < this.StimInfo.numconditions; j++) {
					Gtk.Label lab = new Gtk.Label ();
					gc.RgbFgColor = stimcolor [j];
					lab.Text = this.StimInfo.conditions [j];
					da.DrawLayout (gc, xoffset+10, yoffset+j*10+10, lab.Layout);
				}
			}

		gc.RgbFgColor = new Gdk.Color (0, 0,0);
		int numxlabels = 10;
		int numylabels = 5;
		
		// Add Xtick marks to the graph
			double tstart, tend, dt;
			tstart = this.time [startIdx];
			tend = this.time [this.samples - 1];
			dt = Math.Round ((tend - tstart ) / (1+numxlabels));

			if (dt < 1)
				dt = 1;
			
			for (double i = 0; i < rangeX; i+=dt) {
				double x = i/rangeX*width;
				Gtk.Label lab = new Gtk.Label ();
				lab.Text = String.Format ("{0}",Math.Round((tstart+i)/100)/10);
				da.DrawLayout (gc, (int)x+xoffset, (int)height+2, lab.Layout);
			}

			double dy;
			dy = rangeY / (1+numylabels);

			if (dy ==0)
				dy = 1;
			
			for (double i = 0; i < rangeY; i+=dy) {
				double y = height-i/rangeY*height;
				Gtk.Label lab = new Gtk.Label ();
				lab.Text = String.Format ("{0}",Math.Round((i+minY)*10)/10);
				da.DrawLayout (gc, 0, (int)y+yoffset, lab.Layout);
			}

		}
	}
}

