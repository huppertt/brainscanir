﻿
namespace BrainScanIR
{
    public partial class Splash : Gtk.Window
    {
        
        public Splash() :
                base(Gtk.WindowType.Toplevel)
        {
            this.Build();

			this.SetPosition(Gtk.WindowPosition.Center);
            int x, y;
            this.GetPosition(out x,out y);
            this.Move(x-this.WidthRequest/2,y-this.HeightRequest/2);
            var buffer = System.IO.File.ReadAllBytes(@"surf/splash.jpg");
			var pixbuf = new Gdk.Pixbuf(buffer);

			int w, h;
			h = pixbuf.Height;
			w = pixbuf.Width;
            this.image1.SetSizeRequest(w, h);
            this.image1.Pixbuf = pixbuf;
            //this.WidthRequest = w;
            this.ShowAll();
        }

        public void dispmsg(string msg){
			uint contextID;
			contextID = this.statusbar1.GetContextId(msg);
			contextID = this.statusbar1.Push(contextID, msg);
            this.QueueDraw();
            this.ShowAll();
            System.Windows.Forms.Application.DoEvents();
        }

    }
}
