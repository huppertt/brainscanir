﻿using System;
namespace BrainScanIR
{
    public partial class CSystemSettings : Gtk.Window
    {
        public CSystemSettings() :
                base(Gtk.WindowType.Toplevel)
        {
            this.Build();
            if(MainClass.SYSTEM==2){
                this.combobox_samplerate.Visible = true;
                this.entry_samplerate.Visible = false;
                //TODO set wireless sample rate
            }else{
                this.combobox_samplerate.Visible = false;
				this.entry_samplerate.Visible = true;
                this.entry_samplerate.Text = MainClass.SystemControler.SystemInfo.samplerate.ToString();
            }
            string instrument;
            if (MainClass.SYSTEM == 0)
                instrument = "Simulator";
            else if (MainClass.SYSTEM == 1)
                instrument = "Cw6";
            else if (MainClass.SYSTEM == 2)
                instrument = "Wireless";
            else
                instrument = "unknown";
            


            this.textview1.Buffer.Text = "System Type: " + instrument +"/n";
            this.textview1.Buffer.Text += "Number of Lasers " + MainClass.SystemControler.SystemInfo.maxsources.ToString() + "/n";
            for (int i = 0; i < MainClass.SystemControler.SystemInfo.numwavelengths; i++){
                this.textview1.Buffer.Text += "/t" + MainClass.SystemControler.SystemInfo.wavelengths[i].ToString() + "nm/n";
            }
            this.textview1.Buffer.Text += "Number of Detectors " + MainClass.SystemControler.SystemInfo.maxdetectors.ToString() + "/n";




		}

        protected void seteditsamplerate(object sender, EventArgs e)
        {
            int rate = Convert.ToInt32(this.entry_samplerate.Text);
            MainClass.SystemControler.SystemInfo.SetSampleRate(rate);
        }

        protected void setwirelesssamplerate(object sender, EventArgs e)
        {
            int rate = Convert.ToInt32(this.combobox_samplerate.ActiveText);
			MainClass.SystemControler.SystemInfo.SetSampleRate(rate);
        }
    }
}
