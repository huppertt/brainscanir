﻿
using System;
using System.Collections;
using System.Threading;
using System.Diagnostics;
using System.IO.Ports;
            
namespace BrainScanIR
{

	public class CTechEnWireless
	{
		public int[] gain { get; set; }
		public bool[] lason { get; set; }
        public int[] laserpower { get; set; }
		public static bool isrunning;
		public static int samplesavaliable;
		public static int samplerate;
		static Queue[] dataqueue;
		static int maxmeas;
		private int maxdet;
		private int maxsrc;
		static int maxaux;
		public static Thread newthread;
		private static FTD2XX_NET.FTDI ftdi;
		public bool usefilter;
		public bool useDC;
		bool useserial;
		static SerialPort wirelessserial;

		public bool getisrunning ()
		{
			return isrunning;
		}

		public int getsamplesavaliable ()
		{
			return samplesavaliable;
		}



		public CTechEnWireless ()
		{
			maxdet = 6;
			maxsrc = 8;
			maxaux = 4;
			maxmeas = 16;
			samplerate = 75;
			dataqueue = new Queue[maxmeas+ maxaux];
			useserial = true;

			gain = new int[maxdet];
			lason = new bool[maxsrc];
			laserpower = new int[maxsrc];

			for (int i = 0; i < maxdet; i++) {
				gain [i] = 0;
			}
			for (int i = 0; i < maxsrc; i++) {
				lason [i] = false;
				laserpower [i] = 1;
			}
			isrunning = false;
			samplesavaliable = 0;

			for (int i = 0; i < maxmeas+ maxaux; i++) {
				dataqueue [i] = new Queue ();
			}

			if (useserial) {
				
				string[] ports = SerialPort.GetPortNames();

				int found = 1;
				for (int i = 0; i < ports.Length; i++)
				{
					if (ports[i].Contains("/dev/tty.usbserial"))
						found = i;

				}
				// TODO fix this
				try
				{
					wirelessserial = new SerialPort(ports[found], 230400, Parity.None, 8);  //TODO fix this.
					wirelessserial.StopBits = StopBits.One;
					wirelessserial.Handshake = Handshake.None;
					wirelessserial.Open();
				}
				catch
				{
					Gtk.MessageDialog msg = new Gtk.MessageDialog(null, Gtk.DialogFlags.Modal, Gtk.MessageType.Error, Gtk.ButtonsType.Ok, "Failed to load WIRELESS system");
					msg.Run();
					Process.GetCurrentProcess().CloseMainWindow();
				}

			} else {
				ftdi = new FTD2XX_NET.FTDI ();
				initialize ();
			}

			alloff ();
			setdarkcorr (false);
			setfilter (false);
			SendSerial("stp");

			for (int i = 0; i < maxdet; i++) {
				setgain (i, gain [i]);
			}
			for (int i = 0; i < maxsrc; i++) {
				setlaser (i, lason [i]);
				setlaserpower (i, laserpower [i]);
			}

		}

		~CTechEnWireless(){
			alloff ();
			setdarkcorr (false);
			setfilter (false);
			SendSerial("stp");
			if(useserial){
				wirelessserial.Close();
				wirelessserial.Dispose ();
			}
		}

		public static void adddata ()
		{
			// TODO
			int wait;
			wait = (500 / samplerate);
			Debug.WriteLine ("Running thread");
			char[] line = new char[121];
			while (isrunning) {
				int cnt = wirelessserial.BytesToRead;
				while (cnt > 122) {
					int ch=0;
					while(ch!=36){
						ch=wirelessserial.ReadChar();
					}
					wirelessserial.Read (line, 0, 121);

					for (int i = 0; i < maxmeas+maxaux; i++) {
						double value = 0;
						value += 16*16*16*char2val (line [10+i * 5]);
						value += 16*16*char2val (line [10 + i * 5+1]);
						value += 16*char2val (line [10 + i * 5+2]);
						value += char2val (line [10 + i * 5+3]);
						dataqueue [i].Enqueue (value);
					}
					samplesavaliable += 1;

					cnt = wirelessserial.BytesToRead;
				}
				System.Threading.Thread.Sleep (wait);
			}
				

		}

		static double char2val(char a){
			switch (a.ToString()) {
			case "0":
				return 0;
			case "1":
				return 1;
			case "2":
				return 2;
			case "3":
				return 3;
			case "4":
				return 4;
			case "5":
				return 5;
			case "6":
				return 6;
			case "7":
				return 7;
			case "8":
				return 8;
			case "9":
				return 9;
			case "A":
				return 10;
			case "B":
				return 11;
			case "C":
				return 12;
			case "D":
				return 13;
			case "E":
				return 14;
			case "F":
				return 15;
			default:
				return 0;
			}
				
		}


		public bool initialize ()
		{
			for (int i = 0; i < maxmeas; i++) {
				dataqueue [i].Clear ();
			}
			isrunning = false;
			samplesavaliable = 0;

			if (!useserial) {
				// TODO
				FTD2XX_NET.FTDI.FT_STATUS ftstatus;


				UInt32 devcount = 0;
				ftstatus = ftdi.GetNumberOfDevices (ref devcount);
				string msg = String.Format ("Found {0} FTDI devices", devcount);
				//PhotonViewer.AppWindow.paneldb.debugmsg (msg);

				FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE[] devicelist;
				devicelist = new FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE[devcount];
				// Instantiate the array elements as FT_DEVICE_INFO_NODE
				for (UInt32 i = 0; i < devcount; i++) {
					devicelist [i] = new FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE ();
				}

				ftstatus = ftdi.GetDeviceList (devicelist);
				for (UInt32 i = 0; i < devcount; i++) {
					msg = String.Format ("Device {0}: Serial Number {1}", i, devicelist [i].SerialNumber);
					//		PhotonViewer.AppWindow.paneldb.debugmsg (msg);
					msg = String.Format ("COM {0}", devicelist [i].LocId);
					//		PhotonViewer.AppWindow.paneldb.debugmsg (msg);
				}

				ftstatus = ftdi.OpenByIndex ((uint)0);

				if (ftstatus == FTD2XX_NET.FTDI.FT_STATUS.FT_OK) {
					msg = "Connected to Wireless";
				//	PhotonViewer.AppWindow.paneldb.debugmsg (msg);

					uint contextID;
                    contextID = MainClass.AppWindow.handles.statusbardevice.GetContextId (msg);
					MainClass.AppWindow.handles.statusbardevice.Push (contextID, msg);

				}

				ftdi.ResetDevice ();

				ftdi.SetDataCharacteristics (FTD2XX_NET.FTDI.FT_DATA_BITS.FT_BITS_8,
					FTD2XX_NET.FTDI.FT_STOP_BITS.FT_STOP_BITS_1,
					FTD2XX_NET.FTDI.FT_PARITY.FT_PARITY_NONE);
				ftdi.SetBaudRate ((UInt32)230400);
				ftdi.SetFlowControl (FTD2XX_NET.FTDI.FT_FLOW_CONTROL.FT_FLOW_NONE, 1, 1);

				return (ftstatus == FTD2XX_NET.FTDI.FT_STATUS.FT_OK);
			} else
				return true;
			
		}

		public bool setfilter(bool state){
			bool ok;
			if(state)
				ok=SendSerial("ft1");
			else
				ok=SendSerial("ft0");
			usefilter = state;
			return ok;
		}


		public bool setdarkcorr(bool state){

			bool ok;
			if(state)
				ok=SendSerial("DC1");
			else
				ok=SendSerial("DC0");
			useDC = state;
			return ok;

		}

		public bool setgain (int indx, int g)
		{
			gain [indx] = g;
			// TODO- send signal to Cw6
			string msg;
			switch (indx) {
			case 0:
				msg = "d1";
				break;
			case 1:
				msg = "d2";
				break;
			case 2:
				msg = "d3";
				break;
			case 3:
				msg = "d4";
				break;
			case 4:
				msg = "d5";
				break;
			case 5:
				msg = "d6";
				break;
			default:
				return false;
			}

			msg += String.Format ("{0}", g);
			bool ok=SendSerial (msg);
			return ok;
		}


		public int getgain (int indx)
		{
			return gain [indx];
		}

		public bool setlaser (int indx, bool state)
		{
			lason [indx] = state;
			// TODO- send signal to Cw6
			string msg;
			switch (indx) {
			case 0:
				msg = "s16";
				break;
			case 1:
				msg = "s18";
				break;
			case 2:
				msg = "s26";
				break;
			case 3:
				msg = "s28";
				break;
			case 4:
				msg = "s36";
				break;
			case 5:
				msg = "s38";
				break;
			case 6:
				msg = "s46";
				break;
			case 7:
				msg = "s48";
				break;
			default:
				return false;
			}

			if (state)
				msg += "1";
			else
				msg += "0";

			bool ok=SendSerial (msg);

			return ok;
		}


		public bool setlaserpower (int indx, int power)
		{
			laserpower [indx] = power;
			// TODO- send signal to Cw6
			string msg;
			switch (indx) {
			case 0:
				msg = "l16";
				break;
			case 1:
				msg = "l18";
				break;
			case 2:
				msg = "l26";
				break;
			case 3:
				msg = "l28";
				break;
			case 4:
				msg = "l36";
				break;
			case 5:
				msg = "l38";
				break;
			case 6:
				msg = "l46";
				break;
			case 7:
				msg = "l48";
				break;
			default:
				return false;
			}

			msg += String.Format ("{0}", power);
			bool ok=SendSerial (msg);

			return ok;
		}

		public bool getlaser (int indx)
		{
			return lason [indx];
		}

		public double[] getdata ()
		{
			double[] thisdata = new double[maxmeas+maxaux];
			for (int i = 0; i < maxmeas+ maxaux; i++) {
				thisdata [i] = (double)dataqueue [i].Dequeue ();
			}
			samplesavaliable -= 1;
			return thisdata;
		}

		public bool start ()
		{
			//initialize ();

			// Send start command

			Debug.WriteLine ("Started function");
			isrunning = true;
			newthread = new Thread (adddata);
			newthread.Start ();

			if (useserial) {
				wirelessserial.DiscardInBuffer ();
				wirelessserial.DiscardOutBuffer ();
			} else {
				ftdi.Purge (FTD2XX_NET.FTDI.FT_PURGE.FT_PURGE_RX);
				ftdi.Purge (FTD2XX_NET.FTDI.FT_PURGE.FT_PURGE_TX);
			}

			bool ok=SendSerial ("run");

			if (useserial) {
				int ch=0;
				while(ch!=36){
					ch=wirelessserial.ReadChar();
				}
				char[] line=new char[84];
				wirelessserial.Read (line, 0, 84);

			}
				
			return ok;
		}

		public bool stop ()
		{

			// Send stop command
			bool ok=SendSerial("stp");
			isrunning = false;
			newthread.Abort ();
			return ok;
		}

		public bool alloff ()
		{
			for (int i = 0; i < maxsrc; i++) {
				lason [i] = false;
			}

			bool ok = SendSerial ("s160");
			ok = SendSerial ("s180") & ok;
			ok = SendSerial ("s260") & ok;
			ok = SendSerial ("s280") & ok;
			ok = SendSerial ("s360") & ok;
			ok = SendSerial ("s380") & ok;
			ok = SendSerial ("s460") & ok;
			ok = SendSerial ("s480") & ok;
			return ok;
		}

		public bool setrate (int rate)
		{
			samplerate = rate;

			// TODO- fix this
			bool ok = SendSerial("f75");  // Sets sample rate to 75 Hz
		
			return ok;
		}

		public bool SendSerial(string msg){
			msg += "\r\n";
			uint BytesWritten=0;
			int BuffSize;
			BuffSize = msg.Length;

			if (useserial) {
				wirelessserial.Write (msg);
				return true;
			} else {
				FTD2XX_NET.FTDI.FT_STATUS status;
				status = ftdi.Write (msg, BuffSize, ref BytesWritten);
				return (status == FTD2XX_NET.FTDI.FT_STATUS.FT_OK);
			}
		}
		
		public int getlaserpower(int indx)
		{
			return laserpower[indx];
		}

        public void autoadjust_gain()
		{
			//TODO
			Random rnd = new Random();

			// in the simulator just make it random
			for (int i = 0; i < maxdet; i++)
			{
				gain[i] = rnd.Next(9);
			}
			return;
		}
		public void autoadjust_lasers()
		{
			//TODO
			Random rnd = new Random();

			// in the simulator just make it random
			for (int i = 0; i < maxsrc; i++)
			{
				laserpower[i] = rnd.Next(9);
			}
			return;
		}

		
	}


}