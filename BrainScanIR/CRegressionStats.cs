﻿//using System;
//using MathNet.Numerics;
//using System.Linq;
////using System.Collections.Generic;
////using System.Collections;
//using Gtk;
//using Gdk;
//using Microsoft.Xna;

namespace BrainScanIR
{

	public class tstat{
		public double[] beta;
		public double[] t;
		public double[] p;
		public double[] q;
		public double dof;

		public tstat(){
			beta = new double[0];
			t = new double[0];
			p = new double[0];
			q= new double[0];
			dof = 0;
		}

	}

	public partial class CRegressionStats : Gtk.Window
	{
		public static string[] conditions { get; set;}
		public static MathNet.Numerics.LinearAlgebra.Matrix<double>[] CovBeta { get; set;}
		public static MathNet.Numerics.LinearAlgebra.Matrix<double> Beta { get; set;}
		public static int dof { get; set;}
		public static Cprobe probe { get; set;}
		public static double[] contrast { get; set;}
		public static tstat tval { get; set;}

		TreeIter[] contTreeIter;
	//	double[,] colormappoly;
	//	double[] colormapmu;

		ListStore [] selectedcontrast; 



		public CRegressionStats (MathNet.Numerics.LinearAlgebra.Matrix<double> lBeta,
			MathNet.Numerics.LinearAlgebra.Matrix<double>[] lCovBeta, 
			string[] lcond, int ldof, Cprobe lprobe):
			base(Gtk.WindowType.Toplevel)
		{
			Build ();
			conditions = lcond;
			CovBeta = lCovBeta;
			Beta = lBeta;
			dof = ldof;
			probe = lprobe;
			for (int i = 0; i < probe.measlistAct.Length; i++) {
				probe.measlistAct [i] = true;
			}

			contrast = new double[Beta.RowCount];
			contrast [0] = 1;
			for (int i = 1; i < Beta.RowCount; i++) {
				contrast [i] = 0;
			}

			// This is a 8th order polyfit interp of the colormap
	//		colormappoly= new double[3,8]{{0.0209,-0.0326,-0.1252,0.1958,0.1449,-0.5549,0.2395,0.9707},
	//		{0.0025,-0.0111,0.0155,0.1320,-0.0649,-0.5848,-0.0573,0.9538},
	//		{-0.0010,-0.0594,0.0269, 0.3173,-0.0096,-0.6655 ,-0.2513,0.9442}};

	//		colormapmu=new double[3]{0.5860,0.5860,0.5860};

			this.drawingarea3.ExposeEvent+=drawprobe;

			selectedcontrast = new ListStore [contrast.Length];
			for (int i = 0; i < contrast.Length; i++) {
				selectedcontrast [i] = new ListStore (typeof(string));
				selectedcontrast [i].AppendValues("-1");
				selectedcontrast [i].AppendValues("--");
				selectedcontrast [i].AppendValues("+1");

			}

			treeviewcontrast.HeadersVisible=true;
			treeviewcontrast.EnableGridLines = Gtk.TreeViewGridLines.Both;

			CellRendererText colt = new CellRendererText ();
			TreeViewColumn col = new TreeViewColumn ();
			col.Title = "Condition";
			col.PackStart (colt, true);
			col.AddAttribute (colt, "text", 0);
			treeviewcontrast.AppendColumn (col);


			CellRendererCombo colc = new CellRendererCombo ();
			colc.TextColumn=0;
			colc.Model = new Gtk.ListStore(typeof(string));
			colc.HasEntry = true;
			colc.Editable = true;
			colc.Edited += ChangedContrast;
			colc.Model = selectedcontrast [0];

			TreeViewColumn col2 = new TreeViewColumn ();
			col2.Title = "Weight";
			col2.PackStart (colc, false);
			col2.AddAttribute(colc, "text", 1);
			treeviewcontrast.AppendColumn (col2);


			ListStore store = new ListStore (typeof(string),typeof(string)); 
			treeviewcontrast.Model = store;
			contTreeIter = new TreeIter[conditions.Length];
			for (int i = 0; i < conditions.Length; i++) {
				contTreeIter [i] = new TreeIter ();
				contTreeIter[i]=store.AppendValues (conditions[i],selectedcontrast[i]);
				store.SetValue (contTreeIter[i],1,"--");
			}
			store.SetValue (contTreeIter[0],1,"+1");

			tval = ttest ();

			ShowAll ();
		}

			
		protected void drawprobe(object sender, EventArgs e){

			double thres = Convert.ToDouble(this.entryThresh.Text);
			double[] p;
			double[] p2;
			double[] t;
			if (this.checkbuttonFDR.Active)
				p = tval.q;
			else
				p=tval.p;

			t=new double[probe.measlistAct.Length];
			p2=new double[probe.measlistAct.Length];

			if(comboboxWhichDisp.Active==0){
				for(int i=0; i<probe.measlistAct.Length; i++){
					t[i] = tval.t[i];  // or beta only ones I care about
					p2[i]=p[i];
				}}
			else if(comboboxWhichDisp.Active==1){
				for(int i=0; i<probe.measlistAct.Length; i++){
					t[i] = tval.t[i+probe.measlistAct.Length];  // or beta only ones I care about
					p2[i]=p[i+probe.measlistAct.Length];
				}}
			else if(comboboxWhichDisp.Active==2){
				for(int i=0; i<probe.measlistAct.Length; i++){
					t[i] = tval.beta[i];  // or beta only ones I care about
					p2[i]=p[i];
				}}
			else if(comboboxWhichDisp.Active==3){
				for(int i=0; i<probe.measlistAct.Length; i++){
					t[i] = tval.beta[i+probe.measlistAct.Length];  // or beta only ones I care about
					p2[i]=p[i+probe.measlistAct.Length];
				}
			}
			
			for (int i = 0; i < probe.measlistAct.Length; i++) {
				probe.measlistAct [i] = (p[i] < thres);
			}

			probe.colormap = calcolormap(t);

			probe.drawsdg (drawingarea3.GdkWindow);
			drawingarea3.QueueDraw();

			// Draw the colorbar
			Gdk.GC gc = new Gdk.GC(drawingarea4.GdkWindow);

			gc.RgbBgColor = new Gdk.Color(0, 0, 0);
			gc.RgbFgColor = new Gdk.Color(0, 0, 0);
			Rectangle rarea = new Rectangle();
			rarea.Height = 300;
			rarea.Width = 30;
			drawingarea4.GdkWindow.DrawRectangle(gc, true, rarea);

			drawingarea4.QueueDraw ();
			ShowAll ();
		}


		public tstat ttest(){
			return ttest (contrast);
		}

		public tstat ttest(double[] c){

			var cc = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.Dense (c.Length,1, c);

			var b = cc.Transpose() * Beta;


			tstat tstatistic = new tstat ();

			tstatistic.beta=new double[Beta.ColumnCount];
			tstatistic.t=new double[Beta.ColumnCount];
			tstatistic.p=new double[Beta.ColumnCount];
			tstatistic.q=new double[Beta.ColumnCount];
			tstatistic.dof = dof;

			double[] psorted=new double[Beta.ColumnCount];
			for (int i = 0; i < b.ColumnCount; i++) {
				var s = cc.Transpose() * CovBeta[i] * cc;
				tstatistic.beta [i] = b[0, i];
				if (s [0, 0] <= 0)
					tstatistic.t [i] = 0;  //avoid the divide by zero issues
				else
					tstatistic.t [i] = tstatistic.beta [i] / Math.Sqrt (s [0, 0] );

				tstatistic.p[i] = 2*(MathNet.Numerics.Distributions.StudentT.CDF (0, 1, tstatistic.dof, -Math.Abs(tstatistic.t [i])));
				psorted [i] = tstatistic.p [i];
			}


			Array.Sort (psorted);
			for (int i = 0; i < psorted.Length; i++) {
				int j = Array.FindIndex (tstatistic.p, item=>item==psorted [i]);
				tstatistic.q [j] = psorted [i] *  psorted.Length / (i + 1);
			}
			for (int i = 0; i <psorted.Length - 1; i++) {
				if (tstatistic.q [i] < tstatistic.q [i + 1]) {
					tstatistic.q [i] = tstatistic.q [i+1];
				}
			}

			return tstatistic;
		
		}




		protected void ChangedThres (object sender, EventArgs e)
		{
			
			drawprobe (sender, e);
		}

		protected void ChangedContrast(object sender, EditedArgs e){
			TreeSelection selection = treeviewcontrast.Selection;	
			TreeIter iter;

			if (!selection.GetSelected (out iter)) {
				return;
			}
			Gtk.TreeModel store = treeviewcontrast.Model;

			store.SetValue (iter, 1, e.NewText);

			// Recompute the T-statistics
			contrast = new double[conditions.Length+1];
			for (int i = 0; i < conditions.Length; i++) {
				string cond = (string)store.GetValue (contTreeIter [i], 1);
				if (cond.Equals ("-1"))
					contrast [i] = -1;
				else if (cond.Equals ("+1"))
					contrast [i] = 1;
				else
					contrast [i] = 0;

			}
			contrast [conditions.Length] = 0; // DC term
			tval = ttest (contrast);
			drawprobe (sender, e);

		}

		public void update (MathNet.Numerics.LinearAlgebra.Matrix<double> lBeta,
			MathNet.Numerics.LinearAlgebra.Matrix<double>[] lCovBeta, 
			string[] lcond, int ldof){

			Beta = lBeta;
			CovBeta = lCovBeta;
			dof = dof;
			for (int i = 0; i < lcond.Length; i++) {
				conditions [i] = lcond [i];
			}
			tval = ttest();
			drawingarea3.QueueDraw ();
			ShowAll ();
		}

		protected void ReconstructImage(object sender, EventArgs e)
		{
			FWDmodel fwd = new FWDmodel();
			fwd.Jacobian(AppWindow.SystemController.subjectData.SDGprobe,
			             AppWindow.headmodel[1],.0001,AppWindow.Smoother);
			var b2 = MathNet.Numerics.LinearAlgebra.Vector<double>.Build.DenseOfArray(tval.beta);
			var b = MathNet.Numerics.LinearAlgebra.Vector<double>.Build.DenseOfArray(tval.beta);

			// Need to convert back to dOD
			for (int j = 0; j < 2; j++)
			{
				for (int i = 0; i < probe.nummeas; i++)
				{
					int sIdx1 = AppWindow.SystemController.subjectData.data[0].SDGRef.measlist[i + probe.nummeas * j, 3];
					double ExtHbO = AppWindow.SystemController.systemInfo.laserarray[sIdx1].ExtHbO;
					double ExtHb = AppWindow.SystemController.systemInfo.laserarray[sIdx1].ExtHbR;
					double L = AppWindow.SystemController.subjectData.data[0].SDGRef.distances[i]/1000000;

					b[i + probe.nummeas * j] = L * ExtHbO * b2[i] + ExtHb * L * b2[i + probe.nummeas];

				}
			}
			var image = fwd.param2brain*fwd.iL * b;



			Color[] cmap = calcolormap(image.SubVector(0, AppWindow.headmodel[1].nvertices).ToArray());

			// TODO- HbO/HbR vs Tstat
			Microsoft.Xna.Framework.Color[] cdata = new Microsoft.Xna.Framework.Color[AppWindow.headmodel[1].nvertices];
			for (int i = 0; i < AppWindow.headmodel[1].nvertices; i++)
			{
				cdata[i] = new Microsoft.Xna.Framework.Color((float)cmap[i].Red, 
				                                             (float)cmap[i].Green, 
				                                             (float)cmap[i].Blue);
			}


			AppWindow.headmodel[1].setcolors(cdata);
			//DrawBrain win = new DrawBrain();
			//win.Show();



		}

		public Color[] calcolormap(double[] t)
		{
			double tmax = Math.Max(Math.Abs(t.Max()), Math.Abs(t.Min()));
			if (!checkbuttonautoscale.Active)
			{
				tmax = Convert.ToDouble(entryscale.Text);
			}

			if (tmax == 0)
				tmax = 1;

			// update the colormap to use
			Gdk.Color[] colormap = new Gdk.Color[t.Length];

			int ncmap = probe.colormapstats.Length;


			for (int i = 0; i < t.Length; i++)
			{
				int idx = (int)((ncmap - 1) / 2 * t[i] / tmax+(ncmap - 1) / 2);
				if (idx < 0)
					idx=0;
				if (idx > ncmap)
					idx = ncmap;
				
				colormap[i] = probe.colormapstats[idx];


				/*double R = 0;
				double G = 0;
				double B = 0;
				double x = t[i] / tmax;
				for (int j = 0; j < 8; j++)
				{
					R += Math.Pow(x / colormapmu[0], 7 - j) * colormappoly[0, j];
					G += Math.Pow(x / colormapmu[1], 7 - j) * colormappoly[1, j];
					B += Math.Pow(x / colormapmu[2], 7 - j) * colormappoly[2, j];
				}
				R = Math.Max(R, 0);
				G = Math.Max(G, 0);
				B = Math.Max(B, 0);
				R = Math.Min(R, 1);
				G = Math.Min(G, 1);
				B = Math.Min(B, 1);

				colormap[i] = new Gdk.Color(Convert.ToByte((int)(R * 255)),
					Convert.ToByte((int)(G * 255)), Convert.ToByte((int)(B * 255)));
				*/

			}
			return colormap;
		}

	}

}

