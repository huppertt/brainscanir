﻿using System;
using System.Xml;

namespace BrainScanIR
{
	public class lasers{
		public int index { get; set; }
		public int lambda { get; set; }
		public double ExtHbO { get; set; }
		public double ExtHbR { get; set; }
	}


	public class Csystem
	{
		public string systemtype; // Type of instrument
		public int maxsources;
		public int maxdetectors;
		public bool laser_adjustable;
		public int maxdatarate;
		public int maxdetectgain;
		public int maxlaserpwr;
		public lasers[] laserarray;
		public object daq;

		public string datadir;
		public string probedir;
		public int samplerate;

		public int numwavelengths;
		public double[] wavelengths;
        public bool loadheadmodel;

        // Constructor
        public Csystem()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@"BrainScanIR_Config.xml");
            XmlNodeList elemList;
            XmlNodeList elemListsub;

            elemList = doc.GetElementsByTagName("systemtype");
            this.systemtype = (string)elemList[0].InnerXml;
            this.systemtype = this.systemtype.Trim();
            this.systemtype = this.systemtype.ToUpper();

            if (this.systemtype.Equals("SIMULATOR"))
            {
                BrainScanIR.MainClass.SYSTEM = 0;
            }
			else if (this.systemtype.Equals("CW6"))
			{
				BrainScanIR.MainClass.SYSTEM = 1;
			}
            else if (this.systemtype.Equals("WIRELESS")){
                BrainScanIR.MainClass.SYSTEM = 2;
            }else{
				Gtk.MessageDialog msgb = new Gtk.MessageDialog(null, Gtk.DialogFlags.Modal, Gtk.MessageType.Error, Gtk.ButtonsType.Ok, "Unkown Instrument");
				msgb.Run();
				msgb.Destroy();

				BrainScanIR.MainClass.SYSTEM = -1;
			}

			elemList = doc.GetElementsByTagName("loadheadmodel");
            this.loadheadmodel = (Convert.ToInt32(elemList[0].InnerXml)==1);

			elemList = doc.GetElementsByTagName("maxsources");
			this.maxsources = Convert.ToInt32(elemList [0].InnerXml);

			elemList = doc.GetElementsByTagName("maxdetectors");
			this.maxdetectors = Convert.ToInt32(elemList [0].InnerXml);

			elemList = doc.GetElementsByTagName("laser_adjustable");
			this.laser_adjustable = ((Convert.ToInt32(elemList [0].InnerXml))==1);

			elemList = doc.GetElementsByTagName("maxdatarate");
			this.maxdatarate = Convert.ToInt32(elemList [0].InnerXml);

			this.samplerate = this.maxdatarate;


			elemList = doc.GetElementsByTagName("maxdetectgain");
			this.maxdetectgain = Convert.ToInt32(elemList [0].InnerXml);

			elemList = doc.GetElementsByTagName("maxlaserpwr");
			this.maxlaserpwr = Convert.ToInt32(elemList [0].InnerXml);

			// Wavelength and extinction coeff
			elemList = doc.GetElementsByTagName("wavelength");
			XmlDocument doc2 = new XmlDocument();
			doc2.LoadXml ("<root>" + elemList [0].InnerXml + "</root>");
			elemList = doc2.GetElementsByTagName("lambda");

			double[] wavelengthsExHbO = new double[elemList.Count];
			double[] wavelengthsExHbR = new double[elemList.Count];
			numwavelengths = elemList.Count;
			wavelengths = new double[elemList.Count];
			for (int i = 0; i < elemList.Count; i++) {
				doc2 = new XmlDocument();
				doc2.LoadXml ("<root>" + elemList [i].InnerXml + "</root>");
				elemListsub = doc2.GetElementsByTagName("index");
				int wavelengthsIdx=Convert.ToInt32(elemListsub [0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("color");
				wavelengths[wavelengthsIdx]=Convert.ToDouble(elemListsub [0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("extHbo");
				wavelengthsExHbO[wavelengthsIdx]=Convert.ToDouble(elemListsub [0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("extHbr");
				wavelengthsExHbR[wavelengthsIdx]=Convert.ToDouble(elemListsub [0].InnerXml);
			}

			laserarray = new lasers [this.maxsources];

			elemList = doc.GetElementsByTagName("lasers");
			doc2 = new XmlDocument();
			doc2.LoadXml ("<root>" + elemList [0].InnerXml + "</root>");
			elemList = doc2.GetElementsByTagName("diode");
            for (int i=0; i < Math.Min(this.maxsources,elemList.Count); i++)
			{   
				doc2 = new XmlDocument();
				doc2.LoadXml ("<root>" + elemList [i].InnerXml + "</root>");
				elemListsub = doc2.GetElementsByTagName("index");
				lasers lsr = new lasers ();
				lsr.index= Convert.ToInt32(elemListsub [0].InnerXml);
				elemListsub = doc2.GetElementsByTagName("lambda");
				lsr.lambda= (int)wavelengths[Convert.ToInt16(elemListsub [0].InnerXml)];
				lsr.ExtHbO=wavelengthsExHbO[Convert.ToInt16(elemListsub [0].InnerXml)];
				lsr.ExtHbR=wavelengthsExHbR[Convert.ToInt16(elemListsub [0].InnerXml)];
				laserarray [i] = lsr;
			}

			elemList = doc.GetElementsByTagName("datadir");
			this.datadir = (string)elemList [0].InnerXml;

			elemList = doc.GetElementsByTagName("probedir");
			this.probedir = (string)elemList [0].InnerXml;


			if (BrainScanIR.MainClass.SYSTEM==0) {
				daq = new CSimulator ();
			} else if(BrainScanIR.MainClass.SYSTEM==1) {
				daq = new CTechEnCw6 ();
			} else if(BrainScanIR.MainClass.SYSTEM==2){
				daq = new CTechEnWireless ();
			}


			this.SetSampleRate (this.samplerate);

		}

		public void SetLaser(int index, bool flag){

			if (BrainScanIR.MainClass.SYSTEM==0) {
				((CSimulator)daq).setlaser(index,flag);
			} else if(BrainScanIR.MainClass.SYSTEM==1) {
				((CTechEnCw6)daq).setlaser(index,flag);
			} else if(BrainScanIR.MainClass.SYSTEM==2){
				((CTechEnWireless)daq).setlaser(index,flag);
			}
			return;
		}

		public void SetLaser(int index, bool flag,bool applynow)
		{

			if (BrainScanIR.MainClass.SYSTEM == 0)
			{
				((CSimulator)daq).lason[index]=flag;
			}
			else if (BrainScanIR.MainClass.SYSTEM == 1)
			{
				((CTechEnCw6)daq).lason[index]=flag;
			}
			else if (BrainScanIR.MainClass.SYSTEM == 2)
			{
				((CTechEnWireless)daq).lason[index]=flag;
			}
			return;
		}

		public void SetLaserInten(int index,int value){
			if (BrainScanIR.MainClass.SYSTEM == 0) {
			//	((CSimulator)daq).setlaserpower (index, value);
			} else if (BrainScanIR.MainClass.SYSTEM == 1) {
			//	((CTechEnCw6)daq).setlaserpower (index, value);
			} else if (BrainScanIR.MainClass.SYSTEM == 2) {
				((CTechEnWireless)daq).setlaserpower (index, value);
			}
			return;
		}

		public void SetDetector(int index,int value){
			if (BrainScanIR.MainClass.SYSTEM == 0)
			{
				((CSimulator)daq).setgain(index, value);
			}
			else if (BrainScanIR.MainClass.SYSTEM == 1)
			{
				((CTechEnCw6)daq).setgain(index, value);
			}
			else if (BrainScanIR.MainClass.SYSTEM == 2)
			{
				((CTechEnWireless)daq).setgain(index, value);
			}
			return;
		}

		public void SetSampleRate(int rate){
			samplerate = rate;

			if (MainClass.SYSTEM==0) {
				((CSimulator)daq).setrate (rate);
			} else if(MainClass.SYSTEM==1) {
				((CTechEnCw6)daq).setrate (rate);
			} else if(MainClass.SYSTEM==2){
				((CTechEnWireless)daq).setrate (rate);
			}

			return;
		}
		public bool initialize(){
			if (MainClass.SYSTEM==0) {
                int nummeas = MainClass.SystemControler.SubjectData.SDGprobe.nummeas;
                int numlam = MainClass.SystemControler.SubjectData.SDGprobe.numlambda;
				((CSimulator)daq).initialize (nummeas*numlam);
			} else if(MainClass.SYSTEM==1) {
				((CTechEnCw6)daq).initialize ();
			} else if(MainClass.SYSTEM==2){
				((CTechEnWireless)daq).initialize ();
			}

			return true;
		}

		public Cprobe initialize(Cprobe probe)
		{
			if (MainClass.SYSTEM == 0)
			{
				int nummeas = probe.nummeas;
				int numlam = probe.numlambda;
				((CSimulator)daq).initialize(nummeas * numlam);
			}
			else if (MainClass.SYSTEM == 1)
			{
				probe = ((CTechEnCw6)daq).initialize(probe);
			}
			else if (MainClass.SYSTEM == 2)
			{
				((CTechEnWireless)daq).initialize();
			}

			return probe;
		}


		public bool start(){
			bool flag = false;
			if (MainClass.SYSTEM==0) {
				flag = ((CSimulator)daq).getisrunning ();
				if (!flag) {
					((CSimulator)daq).start ();
					flag = true;
				} else {
					((CSimulator)daq).stop ();	
					flag = false;
				}
			} else if(MainClass.SYSTEM==1) {
				flag = ((CTechEnCw6)daq).getisrunning ();
				if (!flag) {
					((CTechEnCw6)daq).start ();
					flag = true;
				} else {
					((CTechEnCw6)daq).stop ();	
					flag = false;
				}
			} else if(MainClass.SYSTEM==2){
					flag = ((CTechEnWireless)daq).getisrunning ();
				if (!flag) {
					((CTechEnWireless)daq).start ();
					flag = true;
				} else {
					((CTechEnWireless)daq).stop ();	
					flag = false;
				}
			} 



		return flag;
		}

		public float getbufferfill(){
			float fract = 0;
			if (BrainScanIR.MainClass.SYSTEM==0) {
				fract = ((CSimulator)daq).getsamplesavaliable () / BrainScanIR.MainClass.BUFFER_SIZE;
			} else if(BrainScanIR.MainClass.SYSTEM==1) {
				fract = ((CTechEnCw6)daq).getsamplesavaliable () / BrainScanIR.MainClass.BUFFER_SIZE;
			} else if(BrainScanIR.MainClass.SYSTEM==2) {
							fract = ((CTechEnWireless)daq).getsamplesavaliable () / BrainScanIR.MainClass.BUFFER_SIZE;
			}
			return fract;
		}

		public CNIRSdata getnewdata(CNIRSdata data){

			if (BrainScanIR.MainClass.SYSTEM == 0)
			{
				int sampleav = ((CSimulator)daq).getsamplesavaliable();
				double[] d = new double[data.nummeas];
				double t;

				for (int i = 0; i < sampleav - 1; i++)
				{
					d = ((CSimulator)daq).getdata();
					t = (data.samples + 1) * 1000 / samplerate;
					data.adddata(d, t);
				}
			}
			else if (BrainScanIR.MainClass.SYSTEM == 1)
			{
				int sampleav = ((CTechEnCw6)daq).getsamplesavaliable();
				double[] d = new double[data.nummeas];
				double t;

				for (int i = 0; i < sampleav - 1; i++)
				{
					d = ((CTechEnCw6)daq).getdata();
					t = (data.samples + 1) * 1000 / samplerate;
					data.adddata(d, t);
				}
			}
			else if (BrainScanIR.MainClass.SYSTEM == 2)
			{
				int sampleav = ((CTechEnWireless)daq).getsamplesavaliable();
				double[] d = new double[data.nummeas + data.numaux];
				double t;

				for (int i = 0; i < sampleav - 1; i++)
				{
					d = ((CTechEnWireless)daq).getdata();
					t = (data.samples + 1) * 1000 / samplerate;
					data.adddata (d, t);
				}
			}
			return data;
		}

        public void autoadjlasers()
        {
            int[] laserspower = new int[maxsources];
            if (BrainScanIR.MainClass.SYSTEM == 0)
            {
                ((CSimulator)daq).autoadjust_lasers();
                for (int i = 0; i < maxsources; i++)
                {
                    laserspower[i] = ((CSimulator)daq).getlaserpower(i);
                }
            }
            else if (BrainScanIR.MainClass.SYSTEM == 1)
            {
                ((CTechEnCw6)daq).autoadjust_lasers();
				for (int i = 0; i < maxsources; i++)
                {
                    laserspower[i] = ((CTechEnCw6)daq).getlaserpower(i);
				}
            }
            else if (BrainScanIR.MainClass.SYSTEM == 2)
			{
               ((CTechEnWireless)daq).autoadjust_lasers();
				for (int i = 0; i < maxsources; i++)
                {
					laserspower[i] = ((CTechEnWireless)daq).getlaserpower(i);
				}
            }
            for (int i = 0; i < maxsources; i++)
            {
                MainClass.AppWindow.handles.laserspinners[i].Value = (double)laserspower[i];
            }


            return; 
        }


		public void autoadjgains()
		{
            int[] gains = new int[maxdetectors];
			if (BrainScanIR.MainClass.SYSTEM == 0)
			{
                ((CSimulator)daq).autoadjust_gain();
                for (int i = 0; i < maxdetectors; i++)
				{
                    gains[i] = ((CSimulator)daq).getgain(i);
				}
			}
			else if (BrainScanIR.MainClass.SYSTEM == 1)
			{
				((CTechEnCw6)daq).autoadjust_gain();
                for (int i = 0; i < maxdetectors; i++)
				{
                    gains[i] = ((CTechEnCw6)daq).getgain(i);
				}
			}
			else if (BrainScanIR.MainClass.SYSTEM == 2)
			{
				((CTechEnWireless)daq).autoadjust_gain();
                for (int i = 0; i < maxdetectors; i++)
				{
                    gains[i] = ((CTechEnWireless)daq).getgain(i);
				}
			}
            for (int i = 0; i < maxdetectors; i++)
			{
                MainClass.AppWindow.handles.detscales[i].Value = (double)gains[i];
			}


			return;
		}

	}


}

