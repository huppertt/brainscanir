﻿﻿using System;
using System.IO;
using Gtk;

namespace BrainScanIR
{
    public partial class RegisterSubjectWindow : Gtk.Window
    {
		Cprobe tmpprobe;
        MainWindow apref;

        public RegisterSubjectWindow(MainWindow apref2): base(Gtk.WindowType.Toplevel){

            this.Build();
            this.Title = "Register Subject";
            this.TransientFor = apref2;
            this.SetPosition(WindowPosition.CenterOnParent);

            tmpprobe = new Cprobe();
            apref = apref2;

            this.previewSDG.ExposeEvent += sdgdraw;
            this.investigatorlist.Changed += new EventHandler(OnInvestigatorlistChanged);
            this.studylist.Changed += new EventHandler(OnStudylistChanged);
            this.button71.Clicked += new EventHandler(AcceptReg);
            this.button69.Clicked += new EventHandler(CancelReg);

            //DeleteEvent += delegate { this.Destroy(); };

			// Populate the investigator/study/subjid folders
            string rootfolder = MainClass.SystemControler.SystemInfo.datadir;

			string[] investigators = Directory.GetDirectories(rootfolder);
			Gtk.ListStore ClearList = new Gtk.ListStore(typeof(string));
            this.investigatorlist.Model = ClearList;
			foreach (string s in investigators)
			{
				string[] s2 = s.Split(System.IO.Path.DirectorySeparatorChar);
                this.investigatorlist.AppendText(s2[s2.Length - 1]);
			}
            this.investigatorlist.Active = 0;
            ShowAll();

		}
		
        protected void sdgdraw(object sender, EventArgs e)
		{
			if (tmpprobe.isregistered)
                tmpprobe.drawsdg1020(this.previewSDG.GdkWindow);
			else
                tmpprobe.drawsdg(this.previewSDG.GdkWindow);

			return;
		}

		protected void OnInvestigatorlistChanged(object sender, EventArgs e)
		{
            string rootfolder = MainClass.SystemControler.SystemInfo.datadir;
			string[] investigators = Directory.GetDirectories(rootfolder);
			int i = investigatorlist.Active;

			if (i > -1 & i < investigators.Length)
			{
				string[] studies = Directory.GetDirectories(investigators[i]);

				Gtk.ListStore ClearList = new Gtk.ListStore(typeof(string));
                //this.studylist.Active = -1;
                this.studylist.Model = ClearList;
				if (studies.Length > 0)
				{
					//this.investigatorlist.Clear ();
					foreach (string s in studies)
					{
						string[] s2 = s.Split(System.IO.Path.DirectorySeparatorChar);
                        this.studylist.AppendText(s2[s2.Length - 1]);
					}
                    this.studylist.Active = 0;
				}
			}
			ShowAll();

		}

		protected void OnStudylistChanged(object sender, EventArgs e)
		{
            string rootfolder = MainClass.SystemControler.SystemInfo.datadir;
			string[] investigators = Directory.GetDirectories(rootfolder);
			int i = investigatorlist.Active;


			if (i > -1 & i < investigators.Length)
			{
				string[] studies = Directory.GetDirectories(investigators[i]);
                int j = this.studylist.Active;

				if (j > -1 & j < studies.Length)
				{
					string[] subjids = Directory.GetDirectories(studies[j]);

					Gtk.ListStore ClearList = new Gtk.ListStore(typeof(string));
                    //this.comboboxentry_subject.Active = -1;
                    this.comboboxentry_subject.Model = ClearList;

					if (subjids.Length > 0)
					{
						//this.investigatorlist.Clear ();
						foreach (string s in subjids)
						{
							string[] s2 = s.Split(System.IO.Path.DirectorySeparatorChar);
                            this.comboboxentry_subject.AppendText(s2[s2.Length - 1]);
						}
					}
                    this.comboboxentry_subject.AppendText("New Subject");
                    this.comboboxentry_subject.Active = subjids.Length;

					string probefile = System.IO.Path.Combine(studies[j], @"Probe.sdg");
					if (File.Exists(probefile))
					{
						tmpprobe.loadxmlprobe(probefile);
					}
					else
					{
						tmpprobe = new Cprobe();
					}

				}

			}

			this.previewSDG.QueueDraw();
            ShowAll();

		}

		protected void AcceptReg(object o, EventArgs args)
		{

            MainClass.SystemControler.SubjectData = new Csubject();


            tmpprobe = MainClass.SystemControler.SystemInfo.initialize(tmpprobe);
            MainClass.SystemControler.SubjectData.SDGprobe = tmpprobe;

            apref.ActivateGUI(tmpprobe,true);

            string investigator = this.investigatorlist.ActiveText;
            string study = this.studylist.ActiveText;
            string subjID = this.comboboxentry_subject.ActiveText;
            string rootfolder = MainClass.SystemControler.SystemInfo.datadir;

			string datet = subjID + "-" + String.Format("{0:s}", DateTime.Now);

            MainClass.SystemControler.SubjectData.outfolder = System.IO.Path.Combine(rootfolder, investigator);
            MainClass.SystemControler.SubjectData.outfolder = System.IO.Path.Combine(MainClass.SystemControler.SubjectData.outfolder, study);
            MainClass.SystemControler.SubjectData.outfolder = System.IO.Path.Combine(MainClass.SystemControler.SubjectData.outfolder, datet);

			System.IO.Directory.CreateDirectory(MainClass.SystemControler.SubjectData.outfolder);
			MainClass.SystemControler.SubjectData.fileroot = subjID + "-" + String.Format("{0:MM-DD-YYYY}", DateTime.Now) + "-scan";
			MainClass.SystemControler.SubjectData.scanIdx = 0;

            // Set the stimulus information
            MainClass.SystemControler.ReadXMLStims(tmpprobe.filename);

			ListStore lst = new ListStore(typeof(string));
			for (int i = 0; i < MainClass.SystemControler.scantypes.Length; i++)
			{
				lst.AppendValues(MainClass.SystemControler.scantypes[i]);
			}
            apref.handles.scantypes.Model = lst;
			apref.handles.scantypes.Active = 0;

			ListStore lst2 = new ListStore(typeof(string));
            for (int i = 0; i < MainClass.SystemControler.stimtypes[0].Length; i++)
			{
				lst2.AppendValues(MainClass.SystemControler.stimtypes[0][i]);
			}
			apref.handles.stimtypes.Model = lst2;
			apref.handles.stimtypes.Active = 0;

           
            apref.handles.SDGwin.QueueDraw();
			this.Destroy();
		}

		protected void CancelReg(object o, EventArgs args)
		{
			this.Destroy();
		}

		protected void RegSubjectDestroy(object o, Gtk.DestroyEventArgs args)
        {
            this.Destroy();
        }
    }
}
