﻿using System;
using Gtk;

namespace BrainScanIR
{
    class MainClass
    {

		// Since C# doesn't allow #DEFINE constants, I'll use the MainClass to hold these
		public static int BUFFER_SIZE = 20 * 60 * 20;  // Buffer size for the data storage array (20min @20Hz)
		public static bool DEBUG = true;  // Flag to add output messages to consol
		public static int SYSTEM = 0; // 0=simulator,1=Cw6,2=wireless
		public static int UPDATETIME = 500; // How often to update refreshes to the display window 

        public static SystemControlClass SystemControler;
        public static MainWindow AppWindow;

        public static Splash splash;
        public static void Main(string[] args)
        {
            Application.Init();

			splash = new Splash();
			splash.Show();
			splash.Opacity = .7;
			splash.Decorated = false;
			splash.dispmsg("Loading GUI");


			GLib.Timeout.Add(1000, delegate
			{
				splash.dispmsg("Initializing system");
                SystemControler = new SystemControlClass();
                splash.dispmsg("Preparing GUI");
				AppWindow = new MainWindow();

				// Layout for the laser panel
				int numlambda = MainClass.SystemControler.SystemInfo.numwavelengths;
				int numlasers = MainClass.SystemControler.SystemInfo.maxsources;
				int numdetectors = MainClass.SystemControler.SystemInfo.maxdetectors;

				// Create all the layouts based on the config file info
				splash.dispmsg("Creating Layouts: Lasers");
                AppWindow.SetLaserLayout(numlasers, numlambda);
                splash.dispmsg("Creating Layouts: Detectors");
                AppWindow.SetDetectorLayout(numdetectors);
                splash.dispmsg("Creating Layouts: Events");
                AppWindow.SetDataFileLayout();
				AppWindow.SetStimTableLayout();

                if (MainClass.SystemControler.SystemInfo.loadheadmodel)
                {
                    splash.dispmsg("Loading 3D models");
                    AppWindow.LoadHeadModel();
					AppWindow.handles.headrefgmenu.Sensitive = true;
                }else{
                    //AppWindow.handles.headrefgmenu.Sensitive = false;
                }

				AppWindow.ShowAll();

				splash.Hide();
                splash.Destroy();
				return false;
			});

			Application.Run();

        }

    }
}
