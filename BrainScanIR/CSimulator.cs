﻿using System;
using System.Collections;
using System.Threading;
using System.Diagnostics;


namespace BrainScanIR
{
	

	public class CSimulator
	{
		public static int[] gain { get; set; }
		public bool[] lason { get; set; }
        public static int[] laserpower { get; set; }
		public static bool isrunning;
		public static int samplesavaliable;
		public static int samplerate;
		static Queue[] dataqueue;
		static int maxmeas;
		private int maxdet;
		private int maxsrc;
		static int maxaux;
		public static Thread newthread;

		public bool getisrunning(){
			return isrunning;
		}

		public int getsamplesavaliable(){

			samplesavaliable = dataqueue [0].Count;

			return samplesavaliable;
		}

		public static void adddata(){
			
			int wait;
			wait = (500 / samplerate);
			Debug.WriteLine ("Running thread");
		
            while (isrunning) {
				Random rnd = new Random ();

                try
                {
                    for (int i = 0; i < (maxmeas + maxaux); i++)
                    {
                        dataqueue[i].Enqueue(rnd.NextDouble() * 100 + i * 10 + 50 * gain[0]);
                    }
                    samplesavaliable += 1;

                    Thread.Sleep(wait);
                }catch{
                    Console.WriteLine("Error queueing data");
                }
			}

		}
			
		public CSimulator ()
		{
			maxdet = 32;
			maxsrc = 32;
			maxaux = 4;
			maxmeas = 1024;
			samplerate = 20;
			dataqueue = new Queue[maxmeas + maxaux];

			gain = new int[maxdet];
			lason = new bool[maxsrc];
            laserpower = new int[maxsrc];

			for (int i = 0; i < maxdet; i++) {
				gain [i] = 0;
			}
			for (int i = 0; i < maxsrc; i++) {
				lason[i] = false;
                laserpower[i] = 0;
			}
			isrunning = false;
			samplesavaliable = 0;

			for (int i = 0; i < maxmeas+maxaux; i++) {
				dataqueue[i] = new Queue();
			}


		}

		public bool initialize(int maxml){
			maxmeas = maxml;
			dataqueue = new Queue[maxmeas+ maxaux];
			for (int i = 0; i < maxmeas+ maxaux; i++) {
				dataqueue [i]=new Queue();
			}
			isrunning = false;
			samplesavaliable = 0;


			return false;
		}


		public bool setgain(int indx,int g){
			gain [indx] = g;
	

			return true;
		}


		public int getlaserpower(int indx){
			return laserpower[indx];
		}

		public bool setlaserpowern(int indx, int g)
		{
			laserpower[indx] = g;
        	return true;
		}


		public int getgain(int indx)
		{
			return gain[indx];
		}

		public bool setlaser(int indx,bool state){
            lason[indx] = state;
			return true;
		}


		public bool getlaser(int indx){
			return lason[indx];
		}

		public double[] getdata(){
			double[] thisdata = new double[maxmeas+ maxaux];
			for (int i = 0; i < maxmeas+ maxaux; i++) {
				thisdata [i] = (double)dataqueue [i].Dequeue ();
			}
			samplesavaliable -=1;
			return thisdata;
		}

		public bool start(){
			
			// Send start command
			Debug.WriteLine ("Started function");
			isrunning=true;
			newthread = new Thread (adddata);
			newthread.Start ();



			return true;
		}
		public bool stop(){
		
			// Send stop command
			isrunning=false;
			newthread.Abort ();
			return true;
		}

		public bool alloff(int indx){
			for (int i = 0; i < maxsrc; i++) {
				lason[i] = false;
			}
			return true;
			// TODO
		}

		public bool setrate(int rate){
			samplerate = rate;
		
			// TODO- send signal to Cw6

			return true;
		}

        public void autoadjust_gain(){
			Random rnd = new Random();

			// in the simulator just make it random
			for (int i = 0; i < maxdet; i++)
			{
                gain[i] = rnd.Next(9);
			}
            return;
        }
        public void autoadjust_lasers(){
			Random rnd = new Random();

			// in the simulator just make it random
			for (int i = 0; i < maxsrc;i++){
                laserpower[i] = rnd.Next(9);
            }
            return;
        }




	}


}