﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;

/*
 * Define UDP message 
 * 
 */


namespace BrainScanIR
{
    // This class handles communications to Eprime (or other) via a UDP/IP socket
    public class CcomSocket
    {
        public IPAddress[] address;
        public int port;
        static UdpClient udpServer;
        static Thread newthread;
        public static bool isrunning;

        public struct socketevent{
            public string name;
            public float onset;
            public float type;
            public string response;
            public string cresponse;
            public float reactiontime;
            public string comment;
            public byte[] msgdata;
        }

        public CcomSocket()
        {
            string hostname = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(hostname);
            address = ipEntry.AddressList;

            port = 11000;
            isrunning = false;
            udpServer = new UdpClient(port);

			

        }

        public void start()
        {
            isrunning = true;
            newthread = new Thread(listen);
            newthread.Priority = ThreadPriority.AboveNormal;
            newthread.Start();
        }
		public void stop()
		{
            isrunning = false;
            newthread.Abort();
		}


        void listen()
        {
            while (isrunning)
            {
                var groupEP = new IPEndPoint(IPAddress.Any, port);
                var data = udpServer.Receive(ref groupEP);
                MainClass.AppWindow.handles.texteventlistener.Buffer.Text = MainClass.AppWindow.handles.texteventlistener.Buffer.Text + " New Event";


                // todo make this more general
                socketevent e = new socketevent();
                e.msgdata = data;
                if (!data[0].Equals(37))
                {
                    //malformed message
                    e.name = "unknown";
                    e.comment = "";
                    e.cresponse = "";
                    e.response = "";
                    e.onset = 0;
                    e.reactiontime = 0;
                }
                else
                {
                    int cnt = 1;
                    e.name = "";
                    while (!data[cnt].Equals(35))
                    {
                        e.name = e.name + Convert.ToChar(data[cnt]);
                        cnt++;
                    }
                    cnt++;
                    e.comment = "";
                    while (!data[cnt].Equals(35))
                    {
                        e.comment = e.comment + Convert.ToChar(data[cnt]);
                        cnt++;
                    }
                    cnt++;
                    e.response = "";
                    while (!data[cnt].Equals(35))
                    {
                        e.response = e.response + Convert.ToChar(data[cnt]);
                        cnt++;
                    }
                    cnt++;
                    e.cresponse = "";
                    while (!data[cnt].Equals(35))
                    {
                        e.cresponse = e.cresponse + Convert.ToChar(data[cnt]);
                        cnt++;
                    }
                    cnt++;
                    string val;
                    val = "";
                    while (!data[cnt].Equals(35))
                    {
                        val = val + Convert.ToChar(data[cnt]);
                        cnt++;
                    }
                    e.onset = Convert.ToSingle(val);
                    cnt++;
                    val = "";
                    while (!data[cnt].Equals(35))
                    {
                        val = val + Convert.ToChar(data[cnt]);
                        cnt++;
                    }
                    e.type = Convert.ToSingle(val);
                    cnt++;
                    val = "";
                    while (!data[cnt].Equals(35))
                    {
                        val = val + Convert.ToChar(data[cnt]);
                        cnt++;
                    }
                    e.reactiontime = Convert.ToSingle(val);
                    cnt++;
                }

				int i = MainClass.SystemControler.SubjectData.data[0].samples;
				double time = MainClass.SystemControler.SubjectData.data[0].time[i];

				stim ev = new stim();
				ev.onset.Add(time / 1000);
				
                ev.amplitude.Add(1);
                 if (e.type.Equals(0))
                {
                    ev.duration.Add(1);
                    MainClass.SystemControler.SubjectData.data[0].StimInfo.SetStimEvents(e.name, ev);
                }
                else if(e.type.Equals(1)) {
                    ev.duration.Add(999);
                    MainClass.SystemControler.SubjectData.data[0].StimInfo.SetStimEvents(e.name, ev);
				}else if(e.type.Equals(-1)){
                    MainClass.SystemControler.SubjectData.data[0].StimInfo.EndStimEvent(e.name, time);
                }
                    
            }
		}


    }
}

