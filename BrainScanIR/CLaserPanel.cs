﻿using System;
using Gdk;

namespace BrainScanIR
{
    [System.ComponentModel.ToolboxItem(true)]
    public partial class CLaserPanel : Gtk.Bin
    {
        public Gtk.Label[] laserlabels = new Gtk.Label[4];
        public Gtk.ToggleButton[] laserbutton = new Gtk.ToggleButton[4];
        public Gtk.SpinButton[] laserspinner = new Gtk.SpinButton[4];

        public bool linklasers; // flag to control if all lasers act together or independent
        public int[] laserindices;
        bool[] laserstates;
        public bool[] laserinuse;

        public CLaserPanel()
        {
            this.Build();

            linklasers = true;
            laserindices = new int[4];
            laserstates = new bool[4];
            laserinuse = new bool[4];

            laserlabels[0] = this.Label1;
            laserlabels[1] = this.label2;
            laserlabels[2] = this.label3;
            laserlabels[3] = this.label4;

            laserbutton[0] = this.button1;
            laserbutton[1] = this.button2;
            laserbutton[2] = this.button3;
            laserbutton[3] = this.button4;

            laserspinner[0] = this.spinbutton1;
            laserspinner[1] = this.spinbutton2;
            laserspinner[2] = this.spinbutton3;
            laserspinner[3] = this.spinbutton4;

            Color color = new Color(169, 169, 169);
            for (int i = 0; i < 4; i++){
                laserspinner[i].Sensitive = false;
                laserbutton[i].Sensitive = false;
                laserlabels[i].Sensitive = false;
                laserindices[i] = 0;
                laserstates[i] = false;
                laserbutton[i].ModifyBg(laserbutton[i].State, color);
                laserbutton[i].ModifyFg(laserbutton[i].State, color);
                laserinuse[i] = true;
            }

		}

        protected void clicklaser1(object sender, EventArgs e)
        {
                setlaser(0, !laserstates[0]);
        }

        protected void clicklaser2(object sender, EventArgs e)
        {
            setlaser(1, !laserstates[1]);
        }

        protected void clicklaser3(object sender, EventArgs e)
        {
            setlaser(2, !laserstates[2]);
        }

        protected void clicklaser4(object sender, EventArgs e)
        {
            setlaser(3, !laserstates[3]);
        }


        protected void spinlaserchanged1(object sender, EventArgs e)
        {
            if (laserinuse[0] & MainClass.SystemControler.SystemInfo.laser_adjustable)
                MainClass.SystemControler.SystemInfo.SetLaserInten(this.laserindices[0],(int)laserspinner[0].Value);
        }

        protected void spinlaserchanged2(object sender, EventArgs e)
        {
            if (laserinuse[1] & MainClass.SystemControler.SystemInfo.laser_adjustable)
            MainClass.SystemControler.SystemInfo.SetLaserInten(this.laserindices[1], (int)laserspinner[1].Value);
        }

        protected void spinlaserchanged3(object sender, EventArgs e)
        {
            if (laserinuse[2] & MainClass.SystemControler.SystemInfo.laser_adjustable)
            MainClass.SystemControler.SystemInfo.SetLaserInten(this.laserindices[2], (int)laserspinner[2].Value);
        }

        protected void spinlaserchanged4(object sender, EventArgs e)
        {
            if (laserinuse[3] & MainClass.SystemControler.SystemInfo.laser_adjustable)
            MainClass.SystemControler.SystemInfo.SetLaserInten(this.laserindices[3], (int)laserspinner[3].Value);
        }


        public void setlaser(int idx, bool flag){

            Color color;
            if (flag)
            {
                color = new Color(255, 0, 0);
            } else{
                color = new Color(169, 169, 169);
            }
                
            if (linklasers)
			{
				for (int i = 0; i < 4; i++)
				{
                    if (laserinuse[i])
                    {
                        laserstates[i] = flag;
                        MainClass.SystemControler.SystemInfo.SetLaser(laserindices[i], laserstates[i]);
                        laserbutton[i].ModifyBg(Gtk.StateType.Active, color);
                        laserbutton[i].ModifyBg(Gtk.StateType.Normal, color);
                    }
				}
			}
			else
			{
                if (laserinuse[idx])
                {
                    laserstates[idx] = flag;
                    MainClass.SystemControler.SystemInfo.SetLaser(laserindices[idx], laserstates[idx]);
                    laserbutton[idx].ModifyBg(Gtk.StateType.Active, color);
                    laserbutton[idx].ModifyBg(Gtk.StateType.Normal, color);
                }
			}
            if (MainClass.AppWindow != null)
            {
                bool areanyon = true;
                for (int i = 0; i < MainClass.AppWindow.handles.laserpanels.Length; i++)
                {
                    for (int j = 0; j < MainClass.AppWindow.handles.laserpanels[i].laserstates.Length; j++)
                    {
                        if (MainClass.AppWindow.handles.laserpanels[i].laserstates[j])
                            areanyon = (areanyon & !MainClass.AppWindow.handles.laserpanels[i].laserstates[j]);
                    }
                }
                areanyon = !areanyon;
                if (areanyon)
                {
                    MainClass.AppWindow.handles.imagelaserssafety.Sensitive = true;
                    MainClass.AppWindow.handles.button_LasersOnOff.State = Gtk.StateType.Active;
                    MainClass.AppWindow.handles.button_LasersOnOff2.State = Gtk.StateType.Active;

					Gdk.Color ccolor = new Gdk.Color(255, 0, 0);
					MainClass.AppWindow.handles.button_LasersOnOff.ModifyFg(Gtk.StateType.Active, ccolor);
					MainClass.AppWindow.handles.button_LasersOnOff.ModifyBg(Gtk.StateType.Active, ccolor);

					MainClass.AppWindow.handles.button_LasersOnOff2.ModifyFg(Gtk.StateType.Active, ccolor);
					MainClass.AppWindow.handles.button_LasersOnOff2.ModifyBg(Gtk.StateType.Active, ccolor);
                    MainClass.AppWindow.handles.button_LasersOnOff.Label = "All Off";
                    MainClass.AppWindow.handles.button_LasersOnOff2.Label = "All Off";

				}
                else
                {
                    MainClass.AppWindow.handles.imagelaserssafety.Sensitive = false;
                    MainClass.AppWindow.handles.button_LasersOnOff.State = Gtk.StateType.Normal;
                    MainClass.AppWindow.handles.button_LasersOnOff2.State=Gtk.StateType.Normal;
					MainClass.AppWindow.handles.button_LasersOnOff.Label = "All On";
					MainClass.AppWindow.handles.button_LasersOnOff2.Label = "All On";
            
                    Gdk.Color ccolor = Gtk.Button.DefaultStyle.BaseColors[0];
					MainClass.AppWindow.handles.button_LasersOnOff2.ModifyFg(Gtk.StateType.Active, ccolor);
					MainClass.AppWindow.handles.button_LasersOnOff2.ModifyBg(Gtk.StateType.Active, ccolor);



				}
            }

        }

    }
}
