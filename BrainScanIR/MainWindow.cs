﻿using System;
using Gtk;
using System.Threading;
using System.Diagnostics;

/* This file contains the:
  * AppWindow Class
 *      - This is the main GUI interface
 *      - headmodel - GIFTI format of head/brain model from Colin27 atlas
 *      - c1020 - 10-20 labels for Colin27 atlas
 * */

namespace BrainScanIR
{
    public class guihandles
    {
        public CheckButton windowdata_checkbox;
        public Entry windatatime;
        public StatsWindow statswinview;
        public Statusbar statusbardevice;

        public DrawingArea SDGwin;
        public ComboBox combobox_wh;
        public TreeIter[] FileTreeIter;

        public CRealTimePanel rtpanel;

        // detectors
        public CDetectorsTab[] detpanels;
        public VScale[] detscales;
        public ColorButton[] detimages;
        public Label[] detlabels;

        //events
        public ListStore StimStore;
        public ComboBoxEntry scantypes;
        public ComboBoxEntry stimtypes;
        public TextView texteventlistener;
        public TreeIter[] StimTreeIter;
        public CheckButton showlegend;
        public CheckButton showstim;

        // sources
        public Button[] laserbuttons;
        public CLaserPanel[] laserpanels;
        public Alignment[] laserframes;
        public Label[] laserlabels;
        public Label[] laserlabels2;
        public SpinButton[] laserspinners;

        public ToggleButton button_LasersOnOff;
        public Button button_LasersOnOff2;
        public Button buttonlaser_autoadjust;
        public Image imagelaserssafety;
        public CheckButton checkbuttonlinksrcs;

        public CheckButton checkbutton_useeventlistener;
        public Expander expander1;
        public Button buttonautoadjustgain;
        public Button ShowDataQuality;
        public Expander expander_controls;
        public ToggleButton togglebutton_markstim;
        public Button button_markstim;
        public Button startstop_button;
        public Gtk.Action headrefgmenu;

    }

    public partial class MainWindow : Gtk.Window
    {
        public guihandles handles;
        public static Thread maindisplaythread;  // Timing thread to handle drawing during running aquistion
        public CcomSocket EventSocket;

        public MainWindow() : base(Gtk.WindowType.Toplevel)
        {
            Build();
            this.Hide();
            if(MainClass.SystemControler.SystemInfo.loadheadmodel)
                this.KeepBelow = true;  // this hack is needed for the OpenGL rendering


            this.Title = "BrainScanIR";
            handles = new guihandles();
            setGUIhandles();

            string msg = "Application loaded";
            dispstatus(msg);

            // Add the event handlers to control clicking on the SDG window
            this.drawingareaSDG.ExposeEvent += sdgdraw;
            this.drawingarea_main.ExposeEvent += datadraw;
            this.drawingareaSDG.AddEvents((int)Gdk.EventMask.ButtonPressMask);
            this.drawingareaSDG.AddEvents((int)Gdk.EventMask.ButtonReleaseMask);
            this.drawingareaSDG.ButtonReleaseEvent += ClickSDG;

			EventSocket = new CcomSocket();
			this.entry_eventlistener.Text = EventSocket.address[0].ToString() + ":" + EventSocket.port.ToString();

			// Show the the application initialized
			if (MainClass.SYSTEM == 0)
			{
				msg = "Connected to: SIMULATOR";
			}
			else if (BrainScanIR.MainClass.SYSTEM == 1)
			{
				msg = "Connected to: CW6";
			}
			else if (BrainScanIR.MainClass.SYSTEM == 2)
			{
				msg = "Connected to: WIRELESS";
			}
			dispstatus(msg);

		}


        protected void OnDeleteEvent(object sender, DeleteEventArgs a)
        {
            Application.Quit();
            a.RetVal = true;
        }

        protected void AboutCallback(object sender, EventArgs e)
        {
            var aboutwin = new AboutDialog();
            aboutwin.ProgramName = "Brain Scan IR";
            aboutwin.Version = "0.0.1";
            aboutwin.Comments = "fNIRS Data Aquistion GUI";
            aboutwin.Copyright = "(c) 2017 Theodore Huppert";
            aboutwin.Run();
            aboutwin.Destroy();

        }

        protected void RegisterSubject(object sender, EventArgs e)
        {
            RegisterSubjectWindow win = new RegisterSubjectWindow(this);
            win.Show();
            Application.Run();
            this.drawingareaSDG.Show();
        }


        protected void sdgdraw(object sender, EventArgs e)
        {
            // This is evoked on exposure of the SDG window to draw the probe
            // drawing is handled by the CProbe class and is reused by several GUIs in the code

            MainClass.SystemControler.SubjectData.SDGprobe.drawsdg(this.drawingareaSDG.GdkWindow);
            this.drawingareaSDG.QueueDraw();
            return;
        }
        //-----------------------------------------------------------------------
        public void ClickSDG(object o, ButtonReleaseEventArgs args)
        {
            // This is evoked when someone clicks on the SDG window to update the active measurement list of the probe.
            // The actual update is handled by the CProbe class allowing it to be reused

            double x = args.Event.X;
            double y = args.Event.Y;

            int width, height;
            this.drawingareaSDG.GdkWindow.GetSize(out width, out height);

            bool reset = true;
            if (args.Event.Button == 3)
            {
                reset = false;  // right clicked
            }


            MainClass.SystemControler.SubjectData.SDGprobe.updateML((int)x, (int)y, reset, width, height);  // update the active measurement list
            MainClass.SystemControler.SubjectData.data[0].SDGRef = MainClass.SystemControler.SubjectData.SDGprobe;  // restore the probe reference with the updated version

            // update the probe and the data on the the next cycle
            this.drawingareaSDG.QueueDraw();
            this.drawingarea_main.QueueDraw();
        }

        //-----------------------------------------------------------------------
        protected void ClickedStartDAQ(object sender, EventArgs e)
        {
            // This handles both the start and stop aquistion

            bool flag = MainClass.SystemControler.SystemInfo.start();  // this is the call to the instrument 

            // TODO- add the icon to the button.  Currently disapears after first push
            if (flag)
            {
                // If instrument said it is now running
                MainClass.SystemControler.SubjectData.newscan();

                handles.rtpanel.InitializeRealtimeProcess();

                startstop_button.Label = "Stop Aquistion";
                maindisplaythread = new Thread(updatedata);

                // TODO - adjust the priority of the thread

                if (handles.checkbutton_useeventlistener.Active)
                    EventSocket.start();

                maindisplaythread.Start(); // start the timing thread to update the display

                string msg = "Running";
                dispstatus(msg);

            }
            else
            {
                startstop_button.Label = "Start Aquistion";
                EventSocket.stop();
                maindisplaythread.Abort();


                // Auto save the data and tell the user about it
                string msg = "Aquistion stopped";
                dispstatus(msg);

                // save data
                MainClass.SystemControler.SubjectData.scanIdx += 1;
                string filename = System.IO.Path.Combine(MainClass.SystemControler.SubjectData.outfolder,
                                      MainClass.SystemControler.SubjectData.fileroot +
                            String.Format("{0}", MainClass.SystemControler.SubjectData.scanIdx) + ".nirs");
                MainClass.SystemControler.SubjectData.save2matlab(filename);

                msg = "Saved data as: " + filename;
                dispstatus(msg);

                // Add the info to the table of saved data
                string name = "Scan-" +
                    String.Format("{0}", MainClass.SystemControler.SubjectData.scanIdx) + ".nirs";


                ToggleButton tgl = new ToggleButton();
                tgl.Active = true;

                ComboBox cbox = new ComboBox();

                handles.StimTreeIter = new TreeIter[handles.FileTreeIter.Length];
                handles.FileTreeIter.CopyTo(handles.StimTreeIter, 0);
                TreeIter[] temp = new TreeIter[handles.FileTreeIter.Length + 1];
                for (int i = 0; i < handles.StimTreeIter.Length; i++)
                {
                    temp[i] = handles.FileTreeIter[i];
                }

                ListStore store = (ListStore)this.treeviewfiles.Model;
                temp[handles.FileTreeIter.Length] = store.AppendValues(name, tgl, cbox);

                handles.FileTreeIter = temp;


            }
            this.startstop_button.QueueDraw();
            this.ShowAll();
        }
        //-----------------------------------------------------------------------
        public void dispstatus(string msg)
        {
            // Function to handle updates to the status bar in the main GUI
            uint contextID;
            contextID = this.statusbar1.GetContextId(msg);
            contextID = this.statusbar1.Push(contextID, msg);

        }

        //-----------------------------------------------------------------------
        protected void datadraw(object sender, EventArgs e)
        {
            int whichdata, whichdata2;

            whichdata = (int)Math.Floor((double)(this.combobox_whichdisplay.Active / MainClass.SystemControler.SystemInfo.numwavelengths));
            whichdata2 = (int)this.combobox_whichdisplay.Active % (int)MainClass.SystemControler.SystemInfo.numwavelengths;


            // This is evoked on exposure of the main data window to update the drawing
            MainClass.SystemControler.SubjectData.data[whichdata].drawdata(this.drawingarea_main.GdkWindow, whichdata2);
            return;
        }

        //-----------------------------------------------------------------------
        protected void ChangeWhichData(object sender, EventArgs e)
        {
            // User changed which data (690nm, HbO, etc) to display
            this.drawingarea_main.QueueDraw();
        }
        //-----------------------------------------------------------------------
        protected void updatedata()
        {
            // This loop is evoked with the maindisplaythread is started and updates the data display

            float cnt = 0;
            while (maindisplaythread.IsAlive)
            {
                Thread.Sleep(MainClass.UPDATETIME);  // update rate (default 500ms)

                // Get data from the instrument
                MainClass.SystemControler.SubjectData.data[0] = MainClass.SystemControler.SystemInfo.getnewdata(MainClass.SystemControler.SubjectData.data[0]);

                MainClass.SystemControler.SubjectData.data = handles.rtpanel.UpdateRealtimeProcess(MainClass.SystemControler.SubjectData.data);
                this.drawingarea_main.QueueDraw();

                // TODO - replace this with a tic/toc from the system so I can compare to the timing report from the instrument thread
                // TODO - add feedback about buffer overload and loss data in DEBUG mode
                cnt += 1;
                string msg = string.Format("Running: Time Elapsed: {0:0.0}", cnt * MainClass.UPDATETIME / 1000);
                dispstatus(msg);

                // spin the progress bar... 
                // TODO- make this not look so stupid
                this.progressbar2.Pulse();

            }
        }






        protected void ShowControls_Callback(object sender, EventArgs e)
        {

            if (!this.expander_controls.Expanded)
            {
                this.notebook_controls.Visible = false;
                this.notebook_controls.HeightRequest = 1;
                this.drawingarea_main.HeightRequest = this.drawingarea_main.HeightRequest + 600;
            }
            else
            {
                this.notebook_controls.Visible = true;
                this.notebook_controls.HeightRequest = 320  ;
                this.drawingarea_main.HeightRequest = this.drawingarea_main.HeightRequest - 600;
            }
            this.drawingarea_main.QueueDraw();
            this.ShowAll();
        }

        protected void ChangeShowTimeCourse(object sender, EventArgs e)
        {
            this.ShowTimeCourseAction.Value = 1;
            this.ShowStatisticalModelAction.Value = 0;
            this.ShowImageReconstructionAction.Value = 0;
            this.notebook4.Page = 0;
        }


        protected void ChangeShowGLM(object sender, EventArgs e)
        {
            this.ShowTimeCourseAction.Value = 0;
            this.ShowStatisticalModelAction.Value = 1;
            this.ShowImageReconstructionAction.Value = 0;
            this.notebook4.Page = 1;
        }

        protected void ChangeShowImageRecon(object sender, EventArgs e)
        {
            this.ShowTimeCourseAction.Value = 0;
            this.ShowStatisticalModelAction.Value = 0;
            this.ShowImageReconstructionAction.Value = 1;
            this.notebook4.Page = 2;
        }

        protected void ClickAllOnOff(object sender, EventArgs e)
        {
            Gdk.Color color = new Gdk.Color(255, 0, 0);
            this.button_LasersOnOff.ModifyFg(StateType.Active, color);
            this.button_LasersOnOff.ModifyBg(StateType.Active, color);

            handles.button_LasersOnOff2.ModifyFg(StateType.Active, color);
			handles.button_LasersOnOff2.ModifyBg(StateType.Active, color);
           
            if (this.button_LasersOnOff.Active)
            {
                this.button_LasersOnOff.Label = "All Off";
				handles.button_LasersOnOff2.Label = "All Off";
                handles.button_LasersOnOff2.ModifyFg(StateType.Normal, color);
				handles.button_LasersOnOff2.ModifyBg(StateType.Normal, color);
			}
            else
            {

                Gdk.Color color2 = Gtk.Button.DefaultStyle.BaseColors[0];
                handles.button_LasersOnOff2.ModifyFg(StateType.Normal, color2);
				handles.button_LasersOnOff2.ModifyBg(StateType.Normal, color2);
                this.button_LasersOnOff.Label = "All On";
                handles.button_LasersOnOff2.Label = "All On";
            }
            for (int i = 0; i < handles.laserpanels.Length; i++)
            {
                handles.laserpanels[i].linklasers = false;  // turn this off to prevent nested lopping
                for (int j = 0; j < 4; j++)
                {
                    handles.laserpanels[i].setlaser(j, this.button_LasersOnOff.Active);
                }
                handles.laserpanels[i].linklasers = this.checkbuttonlinksrcs.Active;
            }

        }

        protected void linksourcescallback(object sender, EventArgs e)
        {
            for (int i = 0; i < handles.laserpanels.Length; i++)
            {
                handles.laserpanels[i].linklasers = this.checkbuttonlinksrcs.Active;
            }
        }

        protected void CheckComSocketCallback(object sender, EventArgs e)
        {
            if (handles.checkbutton_useeventlistener.Active)
                EventSocket.start();
            else
                EventSocket.stop();

        }

        protected void systemsettingscallback(object sender, EventArgs e)
        {
            CSystemSettings win = new CSystemSettings();
            win.Title = "System Settings";
			win.Show();
			Application.Run();

        }

        protected void LaunchViewRegistration(object sender, EventArgs e)
        {
            HeadRegistration win = new HeadRegistration(MainClass.SystemControler.SubjectData.SDGprobe);
			win.Show();
			Application.Run();
        }

        protected void LasersOnOffButton(object sender, EventArgs e)
        {
            this.button_LasersOnOff.Active = !this.button_LasersOnOff.Active;
            this.button_LasersOnOff.Toggle();
        }

        protected void autoadjustlasers(object sender, EventArgs e)
        {
            MainClass.SystemControler.SystemInfo.autoadjlasers();
        }

        protected void adjustdetgains(object sender, EventArgs e)
        {
			MainClass.SystemControler.SystemInfo.autoadjgains();
        }

        protected void ShowDataQualityCallBack(object sender, EventArgs e)
        {
			//collect 5s of data
			bool flag = MainClass.SystemControler.SystemInfo.start();  // this is the call to the instrument 
            if (flag)
            {
                string label = this.ShowDataQuality.Label;
                this.ShowDataQuality.Label = "Please Wait";
                this.ShowDataQuality.ShowNow();
                Thread.Sleep(1000);
                this.ShowDataQuality.Label = label;
                this.ShowDataQuality.ShowNow();
                MainClass.SystemControler.SystemInfo.start();  //send again to stop
                CNIRSdata data = new CNIRSdata(MainClass.SystemControler.SubjectData.SDGprobe);
                data=MainClass.SystemControler.SystemInfo.getnewdata(data);

                //TODO-  send to window for display
                CShowDataQuality win = new CShowDataQuality(data);
                win.Show();
                Application.Run();
            }



		}

        protected void TestLaunchApp(object sender, EventArgs e)
        {
            //TODO obviously this is just for testing right now
            Process process = Process.Start("/Users/admin/Projects/BrainScanIR/Mac/bin/Debug/BrainViewIR.Mac.app/Contents/MacOS/BrainViewIR.Mac");


        }
    }
}
