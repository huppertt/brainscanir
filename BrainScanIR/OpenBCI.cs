﻿using System;
using System.Collections;
using System.Threading;
using System.Diagnostics;
using System.IO.Ports;

namespace BrainScanIR
{
    public class OpenBCI
    {
		public int[] gain { get; set; }
		public static bool isrunning;
		public static int samplesavaliable;
		public static int samplerate;
        static Queue[] dataqueue;
		private static int maxdet;
		public static Thread newthread;
	//	private static FTD2XX_NET.FTDI ftdi;
		static SerialPort openbciserial;


        // This is the system control for recording from the OpenBCI system
        public OpenBCI()
        {
			samplerate = 200;
            maxdet = 8;

			dataqueue = new Queue[maxdet];
			gain = new int[maxdet];
            for (int i = 0; i < maxdet; i++)
			{
				gain[i] = 0;
			}
			
			isrunning = false;
			samplesavaliable = 0;

			for (int i = 0; i < maxdet; i++)
			{
				dataqueue[i] = new Queue();
			}
			string[] ports = SerialPort.GetPortNames();

			int found = 1;
			for (int i = 0; i < ports.Length; i++)
			{
				if (ports[i].Contains("/dev/tty.usbserial"))
					found = i;

			}
			// TODO fix this
			try
			{
				openbciserial = new SerialPort(ports[found], 230400, Parity.None, 8);  //TODO fix this.
				openbciserial.StopBits = StopBits.One;
				openbciserial.Handshake = Handshake.None;
				openbciserial.Open();
			}
			catch
			{
				Gtk.MessageDialog msg = new Gtk.MessageDialog(null, Gtk.DialogFlags.Modal, Gtk.MessageType.Error, Gtk.ButtonsType.Ok, "Failed to load OPENBCI system");
				msg.Run();
				Process.GetCurrentProcess().CloseMainWindow();
			}

		}


		~OpenBCI()
		{
			    openbciserial.Close();
				openbciserial.Dispose();
		}

		public bool getisrunning()
		{
			return isrunning;
		}

		public int getsamplesavaliable()
		{
			return samplesavaliable;
		}
		public int getgain(int indx)
		{
			return gain[indx];
		}

		public bool setgain(int indx, int g)
		{
			gain[indx] = g;
			// TODO- send signal to Cw6
			string msg;

			msg = "d1";

			msg += String.Format("{0}", g);
			bool ok = SendSerial(msg);
			return ok;
		}


		public bool setrate(int rate)
		{
			samplerate = rate;

			// TODO- fix this
			bool ok = SendSerial("f75");  // Sets sample rate to 75 Hz

			return ok;
		}


		public double[] getdata()
		{
            double[] thisdata = new double[maxdet];
            for (int i = 0; i <maxdet; i++)
			{
				thisdata[i] = (double)dataqueue[i].Dequeue();
			}
			samplesavaliable -= 1;
			return thisdata;
		}

		public bool start()
		{
			//initialize ();

			// Send start command

			Debug.WriteLine("Started function");
			isrunning = true;
			newthread = new Thread(adddata);
			newthread.Start();

			openbciserial.DiscardInBuffer();
			openbciserial.DiscardOutBuffer();
			

			bool ok = SendSerial("run");

			int ch = 0;
				while (ch != 36)
				{
					ch = openbciserial.ReadChar();
				}
				char[] line = new char[84];
				openbciserial.Read(line, 0, 84);


			return ok;
		}


		public static void adddata()
		{
			// TODO
			int wait;
			wait = (500 / samplerate);
			Debug.WriteLine("Running thread");
			char[] line = new char[121];
			while (isrunning)
			{
                int cnt = openbciserial.BytesToRead;
				while (cnt > 122)
				{
					int ch = 0;
					while (ch != 36)
					{
						ch = openbciserial.ReadChar();
					}
					openbciserial.Read(line, 0, 121);

					for (int i = 0; i < maxdet; i++)
					{
						double value = 0;
						value += 16 * 16 * 16 * char2val(line[10 + i * 5]);
						value += 16 * 16 * char2val(line[10 + i * 5 + 1]);
						value += 16 * char2val(line[10 + i * 5 + 2]);
						value += char2val(line[10 + i * 5 + 3]);
						dataqueue[i].Enqueue(value);
					}
					samplesavaliable += 1;

					cnt = openbciserial.BytesToRead;
				}
				System.Threading.Thread.Sleep(wait);
			}


		}

		static double char2val(char a)
		{
			switch (a.ToString())
			{
				case "0":
					return 0;
				case "1":
					return 1;
				case "2":
					return 2;
				case "3":
					return 3;
				case "4":
					return 4;
				case "5":
					return 5;
				case "6":
					return 6;
				case "7":
					return 7;
				case "8":
					return 8;
				case "9":
					return 9;
				case "A":
					return 10;
				case "B":
					return 11;
				case "C":
					return 12;
				case "D":
					return 13;
				case "E":
					return 14;
				case "F":
					return 15;
				default:
					return 0;
			}

		}


		public bool stop()
		{

			// Send stop command
			bool ok = SendSerial("stp");
			isrunning = false;
			newthread.Abort();
			return ok;
		}


		public bool SendSerial(string msg)
		{
			msg += "\r\n";
			openbciserial.Write(msg);
			return true;
			
		}
	}
}
