﻿using System;
using Gtk;
using System.Threading;


namespace BrainScanIR
{


    public partial class MainWindow : Gtk.Window
    {

		protected void ChangedStimOnset(object sender, EditedArgs e)
		{
			TreeSelection selection = treeviewstim.Selection;
			TreeIter iter;

			if (!selection.GetSelected(out iter))
			{
				return;
			}
			Gtk.TreeModel store = treeviewstim.Model;

			stim oldev = new stim();
			oldev.onset.Add(Convert.ToDouble(store.GetValue(iter, 1)));
			oldev.duration.Add(Convert.ToDouble(store.GetValue(iter, 2)));
			oldev.amplitude.Add(Convert.ToDouble(store.GetValue(iter, 3)));
			string oldname = (string)store.GetValue(iter, 0);

			store.SetValue(iter, 1, e.NewText);

			stim newev = new stim();
			newev.onset.Add(Convert.ToDouble(store.GetValue(iter, 1)));
			newev.duration.Add(Convert.ToDouble(store.GetValue(iter, 2)));
			newev.amplitude.Add(Convert.ToDouble(store.GetValue(iter, 3)));
			string newname = (string)store.GetValue(iter, 0);
			MainClass.SystemControler.SubjectData.data[0].StimInfo.ChangeStimEvents(oldname, oldev, newname, newev);
			this.drawingarea_main.QueueDraw();
		}
		protected void ChangedStimDuration(object sender, EditedArgs e)
		{
			TreeSelection selection = treeviewstim.Selection;
			TreeIter iter;

			if (!selection.GetSelected(out iter))
			{
				return;
			}
			Gtk.TreeModel store = treeviewstim.Model;

			stim oldev = new stim();
			oldev.onset.Add(Convert.ToDouble(store.GetValue(iter, 1)));
			oldev.duration.Add(Convert.ToDouble(store.GetValue(iter, 2)));
			oldev.amplitude.Add(Convert.ToDouble(store.GetValue(iter, 3)));
			string oldname = (string)store.GetValue(iter, 0);

			store.SetValue(iter, 2, e.NewText);

			stim newev = new stim();
			newev.onset.Add(Convert.ToDouble(store.GetValue(iter, 1)));
			newev.duration.Add(Convert.ToDouble(store.GetValue(iter, 2)));
			newev.amplitude.Add(Convert.ToDouble(store.GetValue(iter, 3)));
			string newname = (string)store.GetValue(iter, 0);
			MainClass.SystemControler.SubjectData.data[0].StimInfo.ChangeStimEvents(oldname, oldev, newname, newev);
			this.drawingarea_main.QueueDraw();
		}

		protected void ChangedStimAmp(object sender, EditedArgs e)
		{
			TreeSelection selection = treeviewstim.Selection;
			TreeIter iter;

			if (!selection.GetSelected(out iter))
			{
				return;
			}
			Gtk.TreeModel store = treeviewstim.Model;

			stim oldev = new stim();
			oldev.onset.Add(Convert.ToDouble(store.GetValue(iter, 1)));
			oldev.duration.Add(Convert.ToDouble(store.GetValue(iter, 2)));
			oldev.amplitude.Add(Convert.ToDouble(store.GetValue(iter, 3)));
			string oldname = (string)store.GetValue(iter, 0);

			store.SetValue(iter, 3, e.NewText);

			stim newev = new stim();
			newev.onset.Add(Convert.ToDouble(store.GetValue(iter, 1)));
			newev.duration.Add(Convert.ToDouble(store.GetValue(iter, 2)));
			newev.amplitude.Add(Convert.ToDouble(store.GetValue(iter, 3)));
			string newname = (string)store.GetValue(iter, 0);
			MainClass.SystemControler.SubjectData.data[0].StimInfo.ChangeStimEvents(oldname, oldev, newname, newev);
			this.drawingarea_main.QueueDraw();
		}

		protected void ClickMarkStim(object sender, EventArgs e)
		{
            int i = MainClass.SystemControler.SubjectData.data[0].samples;
			if (i < 0)
				return;

            if (maindisplaythread.IsAlive)
            {
                double time = MainClass.SystemControler.SubjectData.data[0].time[i];

                string cond = this.combobox_whichstim.ActiveText;
                double d = MainClass.SystemControler.durtypes[this.comboboxscantypes.Active][this.combobox_whichstim.Active];
                stim ev = new stim();
                ev.onset.Add(time / 1000);
                ev.duration.Add(d);
                ev.amplitude.Add(1);

                MainClass.SystemControler.SubjectData.data[0].StimInfo.SetStimEvents(cond, ev);

                ev = MainClass.SystemControler.SubjectData.data[0].StimInfo.GetEvents(cond);
                this.labelStimMarks.Text = String.Format("Marks: {0}", ev.amplitude.Count);
            }



		}

		protected void ToggleStimClicked(object sender, EventArgs e)
		{
			int i = MainClass.SystemControler.SubjectData.data[0].samples;
			if (i < 0)
				return;
            
            if (maindisplaythread.IsAlive)
			{
                double time = MainClass.SystemControler.SubjectData.data[0].time[i];

                string cond = this.combobox_whichstim.ActiveText;

				stim ev = new stim();
				ev.onset.Add(time / 1000);
				ev.duration.Add(999);
				ev.amplitude.Add(1);

				if (this.togglebutton_markstim.Active)
					MainClass.SystemControler.SubjectData.data[0].StimInfo.EndStimEvent(cond, time);
				else
					MainClass.SystemControler.SubjectData.data[0].StimInfo.SetStimEvents(cond, ev);

			}
		}

		protected void ShowLegend(object sender, EventArgs e)
		{
            drawingarea_main.QueueDraw();
		}

		protected void ShowStim(object sender, EventArgs e)
		{
			drawingarea_main.QueueDraw();
		}

		protected void changesscantype(object sender, EventArgs e)
		{
            int cnt = comboboxscantypes.Active;
			ListStore lst2 = new ListStore(typeof(string));
            for (int i = 0; i < MainClass.SystemControler.stimtypes[cnt].Length; i++)
			{
				lst2.AppendValues(MainClass.SystemControler.stimtypes[cnt][i]);
			}
            combobox_whichstim.Model = lst2;
			combobox_whichstim.Active = 0;


		}

    }
}